import { Component, OnInit } from '@angular/core';
import { Estudiante } from 'src/app/interfaces/estudiante';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Estudiantec } from 'src/app/interfaces/estudiantec';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Roles } from 'src/app/interfaces/roles';
import { Rol } from 'src/app/interfaces/rol';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-admodalumno',
  templateUrl: './admodalumno.component.html',
  styleUrls: ['./admodalumno.component.css']
})
export class AdmodalumnoComponent implements OnInit {
  constructor(private procesoService: ProcesosService, private activatedRoute: ActivatedRoute, private router: Router,
              private formBuilder: FormBuilder) {
    this.buildForm();
  }
  get CampoTipDoc() {
    return this.form.get('TipDocName');
  }
  get CampoUs() {
    return this.form.get('UserName');
  }
  get CampoCor() {
    return this.form.get('UserCorreo');
  }
  get CampoDNI() {
    return this.form.get('UserDNI');
  }
  get CampoCel() {
    return this.form.get('UserCel');
  }
  get CampoDir() {
    return this.form.get('UserDir');
  }
  websiteList: string[] = ['Operador', 'Docente', 'Estudiante'];
  selectedUser?: string;
  isLoading = false;
  nombreRol: string[] = [];
  idTodos: number[] = [];
  nombreRolFijo: string[] = [];
  roles = [];
  roles1 = [];
  rolesL: Rol[];
  rolcitos: Roles = {
    Estudiante: null,
    Operador: null,
    Docente: null,
  };
  rolError = false;
  localRol = null;
  cargando = false;
  estudiante: Estudiantec = {
    id: null,
    Nombre: null,
    idDNITipo: null,
    DNI: null,
    Correo: null,
    Operador: 0,
    Estudiante: 0,
    Docente: 0,
    Direccion: null,
    Telefono: null,
    ListaRoles: [],
  };
  form: FormGroup;
  rolaux: number[] = [];
  rolcito: string;


  onSearch(value: string): void {
    this.isLoading = true;
  }
  ngOnInit(): void {
    this.getRolLista();
    const id = this.activatedRoute.snapshot.params.id;
    this.getDetalles(id);


  }
  private buildForm() {
    this.form = this.formBuilder.group({
      UserName: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(40), Validators.pattern(/^[a-zA-Z ]+$/)]],
      UserCorreo: ['', [Validators.required, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+([a-z0-9](?:[a-z0-9-]*[a-z0-9])?){2}/)]],
      UserDNI: ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{7}/), Validators.maxLength(8), Validators.minLength(8)]],
      UserCel: ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{8}/), Validators.maxLength(9), Validators.minLength(9)]],
      UserDir: ['', [Validators.required, Validators.maxLength(50), Validators.pattern(/^[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?(( |\-)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?)* (((#|[nN][oO]\.?) ?)?\d{1,4}(( ?[a-zA-Z0-9\-]+)+)?)/)]],
    });
  }
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.cargando = true;
      this.actualizar();
      const value = this.form.value;
      console.log('valor: ' + value);
      console.log('nombre rol: ' + this.nombreRol);
    } else {
      this.form.markAllAsTouched();
      this.cargando = false;
    }
  }
  onCreate(uname: string) {
    if (uname != null) {
      const indice = this.nombreRol.indexOf(uname); // obtenemos el indice
      if ( uname === 'Secretaria de Unidad' || uname === 'Secretaria de Programa'){
        const indexope = this.estudiante.ListaRoles.indexOf(2);
        this.estudiante.ListaRoles.splice(indexope, 1);
      }
      if (uname === 'Estudiante') {
        this.rolcitos.Estudiante = true;
        this.estudiante.Estudiante = 1;
      }
      if (uname === 'Operador') {
        this.rolcitos.Operador = true;
        this.estudiante.Operador = 1;
      }
      if (uname === 'Docente') {
        this.rolcitos.Docente = true;
        this.estudiante.Docente = 1;
      }
      const indiceFijo = this.nombreRolFijo.indexOf(uname);
      console.log('este: ', uname);
      this.localRol = uname;
      this.roles.push({
        name: uname
      });
      console.log(indiceFijo);
      this.estudiante.ListaRoles.push(this.idTodos[indiceFijo]);
      console.log(this.estudiante.ListaRoles);
      delete this.nombreRol[indice];
      this.nombreRol = this.nombreRol.filter((i) => i !== '');
    }
  }
  checks(uname: string) {
    if (uname != null) {
      const indice = this.nombreRol.indexOf(uname); // obtenemos el indice
      if (uname === 'Estudiante') {
        this.rolcitos.Estudiante = true;
        this.estudiante.Estudiante = 1;
      }
      if (uname === 'Operador') {
        this.rolcitos.Operador = true;
        this.estudiante.Operador = 1;
      }
      if (uname === 'Docente') {
        this.rolcitos.Docente = true;
        this.estudiante.Docente = 1;
      }
      const indiceFijo = this.nombreRolFijo.indexOf(uname);
      console.log('este: ', uname);
      console.log(this.idTodos[indiceFijo]);
      this.roles.push({
        name: uname
      });
      this.localRol = uname;
      this.estudiante.ListaRoles.push(this.idTodos[indiceFijo]);
      console.log(this.estudiante.ListaRoles);
      delete this.nombreRol[indice];
      this.nombreRol = this.nombreRol.filter((i) => i !== '');
    }
  }
  delete(uname1: string) {
    this.nombreRol.push(uname1);
    console.log('asd');
    console.log(uname1);
    if ( uname1 === 'Secretaria de Unidad' || uname1 === 'Secretaria de Programa'){
      console.log('efe');
      if (this.estudiante.Operador === 1){
        const num = 2;
        const indexope = this.estudiante.ListaRoles.push(num);
        this.estudiante.ListaRoles.push(num)
        console.log('operador', indexope);
        
      }
    }
    if (uname1 === 'Estudiante') {
      this.rolcitos.Estudiante = false;
      this.estudiante.Estudiante = 0;
    }
    if (uname1 === 'Operador') {
      this.rolcitos.Operador = false;
      this.estudiante.Operador = 0;
    }
    if (uname1 === 'Docente') {
      this.rolcitos.Docente = false;
      this.estudiante.Docente = 0;
    }
    let ind;
    for (let index = 0; index < this.roles.length; index++) {
      if (uname1 === this.roles[index].name) {
        ind = index;
      }
    }
    console.log(ind);
    delete this.roles[ind];
    this.estudiante.ListaRoles.splice(ind, 1);
    console.log(this.estudiante.ListaRoles);
    this.roles = this.roles.filter((i) => i !== '');
  }
  uncheck(uname1: string) {
    this.nombreRol.push(uname1);
    if (uname1 === 'Estudiante') {
      this.rolcitos.Estudiante = false;
      this.estudiante.Estudiante = 0;
    }
    if (uname1 === 'Operador') {
      this.rolcitos.Operador = false;
      this.estudiante.Operador = 0;
    }
    if (uname1 === 'Docente') {
      this.rolcitos.Docente = false;
      this.estudiante.Docente = 0;
    }
    let ind;
    for (let index = 0; index < this.roles.length; index++) {
      if (uname1 === this.roles[index].name) {
        ind = index;
      }
    }
    console.log(ind);
    /*console.log(ind); */
    delete this.roles[ind];
    this.estudiante.ListaRoles.splice(ind, 1);
    console.log(this.estudiante.ListaRoles);
    this.roles = this.roles.filter((i) => i !== '');
  }
  onCheckboxChange(e) {
    if (e.target.checked) {
      // tslint:disable-next-line: no-shadowed-variable
      const index = this.websiteList.indexOf(e.target.value);
      // tslint:disable-next-line: no-shadowed-variable
      const nombre = this.websiteList[index];
      console.log(nombre);
      this.checks(nombre);
    } else if (e.target.checked === false) {
      const index = this.websiteList.indexOf(e.target.value);
      const nombre = this.websiteList[index];
      console.log(nombre);
      this.delete(nombre);
    }
  }
  getDetalles(id: number) {
    this.procesoService.getAlumno(id).subscribe(
      (data: Estudiantec) => {
        this.estudiante = data;
        console.log(this.estudiante);
        console.log(this.estudiante.ListaRoles.length);
        console.log('ROLLLLLLLLLLLLL: ' + this.nombreRolFijo);
        console.log(this.estudiante.ListaRoles);
        const tam = this.estudiante.ListaRoles.length;
        for (let i = 0; i < tam; i++) {
          this.rolaux[i] = this.estudiante.ListaRoles[i];
        }
        this.estudiante.ListaRoles.splice(0, tam);
        console.log(this.rolaux);
        console.log(this.estudiante.ListaRoles);
        this.cargar();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }

  cargar() {
    const tam = this.rolaux.length;
    console.log('tamaño del rol aux', tam);
    for (let i = 0; i < tam; i++) {
      const ind = this.rolaux[i];
      console.log(ind);
      const id = this.idTodos.indexOf(ind);
      console.log('el id', id);
      console.log('fijo', this.nombreRolFijo[id]);
      this.rolcito = this.nombreRolFijo[id];
      this.checks(this.rolcito);
      console.log(ind);
      console.log(this.rolcito);
    }
  }

  getRolLista() {
    this.procesoService.rolLista().subscribe(
      (data: Rol[]) => {
        this.rolesL = data;
        console.log(this.rolesL);
        for (let i = 0; i < this.rolesL.length; i++) {
          this.nombreRol[i] = this.rolesL[i].NombreRol;
          this.nombreRolFijo[i] = this.rolesL[i].NombreRol;
          this.idTodos[i] = this.rolesL[i].IdRol;
        }
        console.log(this.nombreRol);
        console.log(this.nombreRolFijo);
      }
    );
  }

  /*   private buildForm(){
      this.form = this.formBuilder.group({
        UserName : ['', [Validators.required, Validators.minLength(10), Validators.pattern(/^[a-zA-Z ]+$/)]],
        UserCorreo : ['', [Validators.required, Validators.email]],
        UserDNI : ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{7}/)]],
        UserCel : ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{8}/)]],
        // tslint:disable-next-line: max-line-length
        UserDir : ['', [Validators.required, Validators.pattern(/^[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?(( |\-)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?)*
                                                               (((#|[nN][oO]\.?) ?)?\d{1,4}(( ?[a-zA-Z0-9\-]+)+)?)/)]],
  });

    }

save(event: Event){
      event.preventDefault();
      if (this.form.valid){
        this.registrar();
        const value = this.form.value;
        console.log(value);
      } else{
        this.form.markAllAsTouched();
      }
    }

get CampoUs(){
      return this.form.get('UserName');
    }
get CampoCor(){
      return this.form.get('UserCorreo');
    }
get CampoDNI(){
      return this.form.get('UserDNI');
    }
get CampoCel(){
      return this.form.get('UserCel');
    }
get CampoDir(){
      return this.form.get('UserDir');
    }
 */
  actualizar() {
    this.estudiante.idDNITipo = 1;
    console.log(this.estudiante);
    console.log('hizo clic en grabar');
    const id = this.activatedRoute.snapshot.params.id;
    console.log('hizo clic en grabar');
    this.procesoService.update(id, this.estudiante).subscribe((data) => {
      console.log(data);
      alert('Usuario Modificado');
      this.cargando = false;
      this.router.navigate(['/adlistalumnos']);
    }, (err: HttpErrorResponse) => {
      this.cargando = false;
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
}
