import { Actividad } from './../../interfaces/actividad';
import { Router } from '@angular/router';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Proceso } from './../../interfaces/proceso';
import { Component, OnInit } from '@angular/core';
import { ProcesoActividad } from 'src/app/interfaces/procesoactividad';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-adlistaprocesos',
  templateUrl: './adlistaprocesos.component.html',
  styleUrls: ['./adlistaprocesos.component.css']
})
export class AdlistaprocesosComponent implements OnInit {
  constructor(private procesoService: ProcesosService, private router:Router, private formBuilder: FormBuilder) {
    this.buildForm();
  }
  procesos: Proceso[];
  proceso: Proceso={
    IdProceso: null,
    NombreProceso: null,
  }

  actividad: Actividad={
    IdProcesoActividad: null,
    IdProceso: null,
    Nombre: null,
  }
  actividades: Actividad[];
  form: FormGroup;
  result: string = '';
  carga: boolean = false;
  ProcDet = false;

  ngOnInit(): void {
    this.getProcesoLista();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm(){
    this.form = this.formBuilder.group({
      ProcName : ['', [Validators.required, Validators.minLength(5), Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ ]*$/)]],
     });
  }
  get CampoProc(){
    return this.form.get('ProcName');
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any): void{
    var selectFile = event.target.files[0];
    if (selectFile.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
      this.result = 'Nombre: ' + selectFile.name;
      this.result += '<br>Tamaño: ' + selectFile.size;
      this.result += '<br>Tipo: ' + selectFile.type;
      this.carga = false;
    }
    else{
      this.result = 'Seleccione un archivo valido!';
      this.carga = true;
    }

  }
   /**
   * @func getProcesoLista
   * @description Funcion que obtener el listado de procesos registrados
   */
  getProcesoLista(){
    this.procesoService.procesoLista().subscribe(
      (data: Proceso[]) => {
        this.procesos = data;
        console.log(this.procesos);
      }
      );
  }
   /**
   * @func enviar
   * @description Funcion que permite actualizar la lista de actividades
   */
  enviar(id: number ){
    console.log('Envia ', id);
    this.ProcDet = true;
    this.procesoService.actividadLista(id).subscribe(
      (data: Actividad[]) =>{
        this.actividades= data ;
        console.log(this.actividades);
      /* (data: any)=>{
        const datos = data;
        console.log(datos); */
      }
      );
     console.log("el id es:",id);
  }
  /**
   * @func crearProceso
   * @description Funcion que permite crear un nuevo proceso
   */
  crearProceso(){
    console.log('hizo clic en grabar');
     console.log(this.proceso);
     this.procesoService.procesoCrear(this.proceso).subscribe((data)=>{
     console.log(data);
    alert('Proceso Guardada');
    this.getProcesoLista();
    },(error)=>{
        console.log(error);
        alert('Ocurrio un error');
     });
  }
  /**
   * @func crearActividad
   * @description Funcion que permite crear una nueva actividad segun el proceso
   */
  crearActividad(){
    console.log('hizo clic en grabar');
     console.log(this.actividad);
     this.procesoService.actividadCrear(this.proceso).subscribe((data)=>{
     console.log(data);
    alert('Actividad Guardada');
    this.getProcesoLista();
    },(error)=>{
        console.log(error);
        alert('Ocurrio un error');
     });
  }
}
