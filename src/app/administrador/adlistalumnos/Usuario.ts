
export interface Usuario{
  id: number;
  name: string;
  idDNTipo: string;
  DNI: number;
  Correo: string;
  clave: string;
  operador: boolean;
  estudiante: boolean;
  docente: boolean;
}
