import { Component, OnInit } from '@angular/core';
import { Roles } from '../../interfaces/roles';
import { Estudiante } from 'src/app/interfaces/estudiante';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { UserserviceService } from 'src/app/servicios/userservice.service';
import { Router } from '@angular/router';
import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';
import { DetallesAlumno, Estudiantec } from 'src/app/interfaces/estudiantec';
import { ReactiveFormsModule } from '@angular/forms';
import { FormControl} from '@angular/forms';
interface Usuar {
  name: string;
}
/*============= INTERFACE PARA MENSAJES*/
interface Mensajes {
  Mensaje: string;
}
/*=============*/
/*===================== */
interface ColumnItem {
  name: string;
  sortOrder?: NzTableSortOrder;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
}
/*===================== */
@Component({
  selector: 'app-adlistalumnos',
  templateUrl: './adlistalumnos.component.html',
  styleUrls: ['./adlistalumnos.component.css']
})
export class AdlistalumnosComponent implements OnInit {
  constructor(private procesoService: ProcesosService, private userService: UserserviceService, private router: Router,
              private modalService: NgbModal) {
                this.filtrarNombre();
                this.filtrarDNI();
  }
  archivoxd: any; // variable para descargar archivos
  /*==========Datos Condicionales===========*/
  BtRegOp = false;
  BtCarOp = false;
  TabOp = false;
  filtro = false;
  fp1 = true;
  fp2 = false;
  FileUp = false;
  FileValShow = false;
  FileUp1 = false;
  FileValShow1 = false;
  tam: Number;
  isSpinning = false;
  filtroDNI = new FormControl('',[]);
  filtroNombre = new FormControl('',[]);
  /*==========/Datos Condicionales===========*/
  /*==========DATATABLES===========*/
  listOfColumns: ColumnItem[] = [
    {
      name: 'DNI',
      sortOrder: null,
      sortFn: (a: Estudiante, b: Estudiante) => a.DNI - b.DNI
    }
  ];
  listOfColumns1: ColumnItem[] = [
    {
      name: 'Nombre/s&Apellidos',
      sortOrder: null,
      sortFn: (a: Estudiante, b: Estudiante) => a.Nombre.localeCompare(b.Nombre),
    },
    {
      name: 'Correo Electronico',
      sortOrder: null,
      sortFn: (a: Estudiante, b: Estudiante) => a.Correo.localeCompare(b.Correo),
    }
  ];
  /*==========/DATATABLES===========*/
  someJSONdata: JSON;
  resultDoc = '';
  resultEst = '';
  carga = false;
  /** CONDICIONALES */
  cargando = false;
  subio = true;
  exito = false;
  closed = false;
  /** */
  filterPost = '';
  filterPost1 = '';
  estado1 = false;
  estado2 = false;
  estado3 = false;
  /**
   * Obtenemos los roles del usuario logueado
   * est rol del estudiante
   * doc rol de docente
   * ope rol de operador
   */
  est = localStorage.getItem('rolesE');
  doc = localStorage.getItem('rolesD');
  ope = localStorage.getItem('rolesO');
  /**
   * roles Objeto que guarda los roles del usuario
   */
  roles: Roles = {
    Estudiante: false,
    Docente: false,
    Operador: false,
  };

  estudiantes: Estudiante[];
  detallesAlumno: DetallesAlumno;
  estudiantes1: Estudiante[];
  /**
   * Variables para archivos
   * fileToUpload file para subir data de estudiantes
   * fileToUploadDoc file para subir data de docentes
   */
  fileToUpload: File = null;
  fileToUploadDoc: File = null;
  /**
   * Variables para mensajes resultantes en la carga de archivo
   * mensaje Respuesta a la carga de archivos (estudiante)
   * cortandoMensaje Formato para mostrar el mensaje en una tabla (estudiante)
   */
  mensaje: string;
  cortandoMensaje: string[] = [];
  /**
   * Variables para mensajes resultantes en la carga de archivo
   * mensaje2 Respuesta a la carga de archivos (docente)
   * listadeMensaje Formato para mostrar el mensaje en una tabla (docente)
   */
  mensaje2: string;
  listadeMensaje: string[] = [];
  /**
   * Variables para busqueda de datos
   */
  buscando: string;
  tipodebusqueda: string;
  DNI = 'DNI';
  Nombre = 'Nombre';

  switchValue = false;


  estudiante: Estudiantec = {
    id: null,
    Nombre: null,
    idDNITipo: null,
    DNI: null,
    Correo: null,
    Operador: 0,
    Estudiante: 0,
    Docente: 0,
    Direccion: null,
    Telefono: null,
    ListaRoles: [],
  };
  ngOnInit(): void {
    this.rolEst();
    this.rolOpe();
    this.rolDoc();
    this.getEstudiantes();
  }
  /**
   * @func onChange
   * @description Funcion que lista la data de los usuarios filtados por "estudiante"
   * @param isChecked parametro que indica si se debe realizar el filtro o no
   */
  onChange(isChecked: boolean) {
    if (isChecked) {
      this.estado1 = true;
      this.estudiantes = Object.values(this.estudiantes).filter((i) => (i.Estudiante === 1));
    } else {
      this.estado1 = false;
      this.estudiantes = this.estudiantes1;
      if (this.estado2 === true) {
        this.onChange1(true);
      }
      if (this.estado3 === true) {
        this.onChange2(true);
      }
    }

  }
  /**
   * @func onChange
   * @description Funcion que lista la data de los usuarios filtados por "docente"
   * @param isChecked parametro que indica si se debe realizar el filtro o no
   */
  onChange1(isChecked: boolean) {
    console.log('hola');
    if (isChecked) {
      this.estado2 = true;
      this.estudiantes = Object.values(this.estudiantes).filter((i) => (i.Docente === 1));
    } else {
      this.estado2 = false;
      this.estudiantes = this.estudiantes1;
      if (this.estado1 === true) {
        this.onChange(true);
      }
      if (this.estado3 === true) {
        this.onChange2(true);
      }
    }
  }
  /**
   * @func onChange
   * @description Funcion que lista la data de los usuarios filtados por "operador"
   * @param isChecked parametro que indica si se debe realizar el filtro o no
   */
  onChange2(isChecked: boolean) {
    console.log('hola');
    if (isChecked) {
      this.estado3 = true;
      this.estudiantes = Object.values(this.estudiantes).filter((i) => (i.Operador === 1));
    } else {
      this.estado3 = false;
      this.estudiantes = this.estudiantes1;
      if (this.estado2 === true) {
        this.onChange1(true);
      }
      if (this.estado1 === true) {
        this.onChange(true);
      }
    }
  }
  /**
   * @func rolEst
   * @description Funcion que almacena el rol estudiante de un usuario
   */
  rolEst() {
    if (this.est === '1') {
      this.roles.Estudiante = true;
    } else {
      this.roles.Estudiante = false;
    }
  }
  /**
   * @func rolOpe
   * @description Funcion que almacena el rol operador de un usuario
   */
  rolOpe() {
    if (this.ope === '1') {
      this.roles.Operador = true;
      this.BtCarOp = true; // EXCEL
      this.BtRegOp = true; // REGISTRO
      this.TabOp = true; // TABLA
      this.filtro = true;
    } else {
      this.roles.Operador = false;
    }
  }
  /**
   * @func rolDoc
   * @description Funcion que almacena el rol docente de un usuario
   */
  rolDoc() {
    if (this.doc === '1') {
      this.roles.Docente = true;
    } else {
      this.roles.Docente = false;
    }
  }
  /**
   * @func getEstudiantes
   * @description Funcion que obtiene el listado con todos los usuarios registrados en la base de datos.
   */
  getEstudiantes() {
    this.isSpinning = true;
    this.procesoService.getAllEstudiantes().subscribe(
      (data: Estudiante[]) => {
        this.estudiantes = data;
        this.tam = this.estudiantes.length;
        console.log('tamaño:' ,this.tam);
        this.estudiantes1 = data;
        this.isSpinning = false;
        console.log(this.estudiantes);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });

  }
  /**
   * @func buscar
   * @description Funcion que busca un dato en la base de datos.
   * @param paramentro que indica el valor a buscar
   */
  buscar(tipo: string) {
    console.log(this.buscando);
    console.log(tipo);
    const valor = tipo + '=' + this.buscando;
    this.procesoService.buscar(valor).subscribe(
      (data: Estudiante[]) => {
        this.estudiantes = data;
        this.estudiantes1 = data;
        console.log(this.estudiantes);
      }

    );

  }

  /**
   * @func eliminar
   * @description Funcion que elimina un estudiante en el sistema.
   * @param id El id del estudiante a eliminar.
   * @returns Mensaje indicando si se elimino correctamente o no.
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminar(id)
        .subscribe((data) => {
          alert();
          console.log(data);
          this.getEstudiantes();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func handleFileInput
   * @description Funcion que seleccionado un archivo temporalmente en la pagina para el estudiante.
   * @param files Archivo a seleccionado.
   * @returns Mensaje indicando si el archivo es correcto o no.
   */
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.resultEst = 'Nombre: ' + this.fileToUpload.name;
      this.resultEst += '<br>Tamaño: ' + this.fileToUpload.size;
      this.resultEst += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp1 = true;
      this.FileValShow1 = false;
    }
    else {
      this.FileValShow1 = true;
      this.carga = true;
      this.FileUp1 = false;
    }
  }
  /**
   * @func handleFileInputDoc
   * @description Funcion que seleccionado un archivo temporalmente en la pagina para el docente.
   * @param files Archivo a seleccionado.
   * @returns Mensaje indicando si el archivo es correcto o no.
   */
  handleFileInputDoc(files: FileList) {
    this.fileToUploadDoc = files.item(0);
    if (this.fileToUploadDoc.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.resultDoc = 'Nombre: ' + this.fileToUploadDoc.name;
      this.resultDoc += '<br>Tamaño: ' + this.fileToUploadDoc.size;
      this.resultDoc += '<br>Tipo: ' + this.fileToUploadDoc.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func CargarEst
   * @description Funcion que carga el archivo seleccionado en el sistema (estudiante).
   * @returns Mensaje de carga correcta o no.
   */
  cargarEst() {
    if(this.FileUp1 === true && this.resultEst !== ''){
    this.procesoService.postFileEstudiante(this.fileToUpload).subscribe((data: Mensajes) => {
      const mensajeInicial = data.Mensaje.split('-', 1);
      this.mensaje = mensajeInicial[0];
      this.cortandoMensaje = data.Mensaje.split('-');
      this.cortandoMensaje.shift();
      this.subio = false;
      this.cargando = true;
      this.exito = false;
    },
      (err: HttpErrorResponse) => {
        alert(err.error.message);
      },
      () => {
        this.procesoService.getAllEstudiantes().subscribe(
          (data: Estudiante[]) => {
            this.estudiantes = data;
            console.log(data);
            this.subio = true;
            this.cargando = false;
            this.exito = true;
          });
      });
    }
    else{
     this.FileValShow1 = true;
    }
  }
  /**
   * @func CargarDoc
   * @description Funcion que carga el archivo seleccionado en el sistema (docente).
   * @returns Mensaje de carga correcta o no.
   */
  cargarDoc() {
    if(this.FileUp === true){
    this.procesoService.postFileDocente(this.fileToUploadDoc).subscribe(
      (data: Mensajes) => {
        const mensajeInicial = data.Mensaje.split('-', 1);
        this.mensaje2 = mensajeInicial[0];
        this.listadeMensaje = data.Mensaje.split('-');
        console.log(this.listadeMensaje);
        console.log('q te paso', this.mensaje2);
        this.listadeMensaje.shift();
        this.subio = false;
        this.cargando = true;
        this.exito = false;
      },
      (err: HttpErrorResponse) => {
        alert(err.error.message);
      },
      () => {
        this.procesoService.getAllEstudiantes().subscribe(
          (data: Estudiante[]) => {
            this.estudiantes = data;
            console.log(data);
            this.subio = true;
            this.cargando = false;
            this.exito = true;
          });
      });
    }
    else{
     this.FileValShow = true;
    }
  }
  /**
   * @func getDocente
   * @description Funcion que obtiene la plantilla de docente.
   * @returns PLantilla.
   */
  getDocente() {
    this.procesoService.descargarDocente().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'plantillaDocente';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
      });
  }
  /**
   * @func getEstudiante
   * @description Funcion que obtiene la plantilla de estudiante.
   * @returns PLantilla.
   */
  getEstudiante() {
    this.procesoService.descargarEstudiante().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'plantillaEstudiante';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func getEstudiante
   * @description Funcion que muestra la data de un usuario (solo puede observar la data aquel usuario de tipo administrador).
   */
  getDetalles(id: number) {
    this.procesoService.getAlumno(id).subscribe(
      (data: Estudiantec) => {
        this.estudiante = data;
        console.log(data);
        console.log(this.estudiante.ListaRoles);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func getDetallesAlumnos
   * @description Funcion que muestra la data de un usuario (servicio que utiliza contiene permisos especiales segun el usuario logueado).
   */
  getDetallesAlumnos(id: number) {
    this.procesoService.getDetallesAlumno(id).subscribe(
      (data: DetallesAlumno) => {
        this.detallesAlumno = data;
        console.log(this.detallesAlumno.Detalles);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func reset
   * @description Funcion que vuelve las variables de las alertas en vacias
   */
  Reset() {
    this.exito = false;
    this.resultDoc = '';
    this.resultEst = '';
  }
  /*==========DATATABLES===========*/
  /* this.dtOptions = {
    paging: false,
    info: false
  } */
  trackByName(_: number, item: ColumnItem): string {
    return item.name;
  }
  /**
   * @func sortByAge
   * @description Funcion que filtra la data segun la comparacion de un campo (DNI)
   */
  sortByAge(): void {
    this.listOfColumns.forEach(item => {
      if (item.name === 'DNI') {
        item.sortOrder = 'descend';
      } else {
        item.sortOrder = null;
      }
    });
  }
  /**
   * @func reset
   * @description Funcion que vuelve las variables de los filtros en vacias
   */
  resetFilters(): void {
    this.listOfColumns.forEach(item => {
      if (item.name === 'Name') {
        item.listOfFilter = [
          { text: 'Joe', value: 'Joe' },
          { text: 'Jim', value: 'Jim' }
        ];
      } else if (item.name === 'Address') {
        item.listOfFilter = [
          { text: 'London', value: 'London' },
          { text: 'Sidney', value: 'Sidney' }
        ];
      }
    });
  }
  /**
   * @func reset
   * @description Funcion que vuelve las variables de las filtros  y las visualizaciones en vacias
   */
  resetSortAndFilters(): void {
    this.listOfColumns.forEach(item => {
      item.sortOrder = null;
    });
    this.resetFilters();
  }
  /*==========DATATABLES===========*/
  searchDNI() {
    this.fp1 = true;
    this.fp2 = false;
  }
  searchNombre() {
    this.fp1 = false;
    this.fp2 = true;
  }


  /* ---------MODAL---------- */
  openXl(content) {
    this.modalService.open(content, { size: 'xl' });

  }
  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }
  openMd(content) {
    this.modalService.open(content, { size: 'md' });
  }
  /* ---------MODAL---------- */

  /* DALTONISMO NO TOCAR */
  clickSwitch(): void {

    if (this.switchValue === true) {
      document.getElementById('btnReg').style.backgroundColor = 'red';
    }else{
      document.getElementById('btnReg').style.backgroundColor = '#1874dd';
    }
}
  /* DATONISMO NO TOCAR */
  getDNI(event: Event){
    event.preventDefault();
    console.log(this.filtroDNI.value);
  }
  /**
   * @func filtrarDNI
   * @description Funcion que filtra la data segun la comparacion de un campo (DNI)
   */
  filtrarDNI(){
    this.filtroDNI.valueChanges
    .subscribe(value =>{
      this.estudiantes = this.estudiantes1;
      console.log(value);
      if(value == ''){
        this.estudiantes = this.estudiantes1;
      }
      else{
        this.estudiantes = Object.values(this.estudiantes).filter((i) =>
        (i.DNI.toString().includes(value.toString())));
      }
    })
  }
  /**
   * @func filtrarNombre
   * @description Funcion que filtra la data segun la comparacion de un campo (Nombre)
   */
  filtrarNombre(){
    this.filtroNombre.valueChanges
    .subscribe(value =>{
      this.estudiantes = this.estudiantes1;
      console.log(value);
      if(value == ''){
        this.estudiantes = this.estudiantes1;
      }
      else{
        this.estudiantes = Object.values(this.estudiantes).filter((i) =>
        (i.Nombre.toLowerCase().includes(value.toLowerCase())));
      }
    })
  }
}

