import { Router } from '@angular/router';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Component, OnInit, Output} from '@angular/core';
import { Rol } from 'src/app/interfaces/rol';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-adroles',
  templateUrl: './adroles.component.html',
  styleUrls: ['./adroles.component.css']
})
export class AdrolesComponent implements OnInit {
  @Output() nombreRol: string;
  constructor(private procesoService: ProcesosService, private router:Router) { }
  roles: Rol[];
  role: Rol = {
    IdRol: null,
    NombreRol: null,
  }

  result: string = '';
  carga: boolean = false;
  ngOnInit(): void {
    this.getRolLista();
  }
  /**
   * @func getRolLista
   * @description Funcion que muestra la lista de roles registrados en la base de datos
   */
  getRolLista(){
    this.procesoService.rolLista().subscribe(
      (data: Rol[]) => {
        this.roles = data;
        console.log(this.roles);
      }
      );
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any): void{
    var selectFile = event.target.files[0];
    if (selectFile.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
      this.result = 'Nombre: ' + selectFile.name;
      this.result += '<br>Tamaño: ' + selectFile.size;
      this.result += '<br>Tipo: ' + selectFile.type;
      this.carga = false;
    }
    else{
      this.result = 'Seleccione un archivo valido!';
      this.carga = true;
    }

  }
  /**
   * @func eliminar
   * @description Funcion que elimina un rol.
   * @param id parametro que indica el id del rol para su eliminacion
   */
  eliminar(id: number){
    if(confirm('Seguro que deseas eliminar')){
      this.procesoService.rolEliminar(id)
      .subscribe((data)=> {
        alert('Eliminado con exito');
        console.log(data);
        this.getRolLista();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
    }
  }
  /**
   * @func crearRol
   * @description Funcion que permite crear un rol deacuerdo a los datos proporcionados
   */
  crearRol(){
    console.log('hizo clic en grabar');
     console.log(this.role);
     this.procesoService.rolCrear(this.role).subscribe((data)=>{
     console.log(data);
    alert('Rol Guardada');
    this.getRolLista();
  }, (err: HttpErrorResponse) => {
    alert(err.error.message);
    if (err.error.message === 'Por favor, inicia sesion para continuar.') {
      this.router.navigate(['/login']);
    }
  });
}
}
