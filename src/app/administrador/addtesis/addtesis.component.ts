import { Component, OnInit } from '@angular/core';
import { Estudiante } from '../../interfaces/estudiante';
import { UserserviceService } from '../../servicios/userservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-addtesis',
  templateUrl: './addtesis.component.html',
  styleUrls: ['./addtesis.component.css']
})
export class AddtesisComponent implements OnInit {

  estudiante: Estudiante = {
    id: null,
    Nombre: null,
    idDNITipo: null,
    DNI: null,
    Correo: null,
    Operador: 0,
    Estudiante: 1,
    Docente: 0,
    Direccion: null,
    Telefono: null,
  }
  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserserviceService, private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) {
    this.buildForm();
  }
  form: FormGroup;
  ngOnInit(): void {
  }
  // FORMULARIO REACTIVO

  private buildForm() {
    this.form = this.formBuilder.group({
      UserName: ['', [Validators.required, Validators.minLength(10), Validators.pattern(/^[a-zA-Z ]+$/)]],
      UserCorreo: ['', [Validators.required, Validators.email]],
      UserDNI: ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{7}/)]],
      UserCel: ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{8}/)]],
      UserDir: ['', [Validators.required, Validators.pattern(/^[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?(( |\-)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?)* (((#|[nN][oO]\.?) ?)?\d{1,4}(( ?[a-zA-Z0-9\-]+)+)?)/)]],
    });
  }
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }

  get CampoUs() {
    return this.form.get('UserName');
  }
  get CampoCor() {
    return this.form.get('UserCorreo');
  }
  get CampoDNI() {
    return this.form.get('UserDNI');
  }
  get CampoCel() {
    return this.form.get('UserCel');
  }
  get CampoDir() {
    return this.form.get('UserDir');
  }
  registrar() {
    this.estudiante.idDNITipo = 1;
    console.log(this.estudiante);
    console.log('hizo clic en grabar');
    this.userService.register(this.estudiante).subscribe((data) => {
      console.log(data);
      alert('Registro Exitoso');
      this.router.navigate(['/login']);
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
    });
  }
}

