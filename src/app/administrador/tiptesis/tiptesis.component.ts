import { Router } from '@angular/router';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { TipoTesis } from './../../interfaces/tipotesis';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-tiptesis',
  templateUrl: './tiptesis.component.html',
  styleUrls: ['./tiptesis.component.css']
})
export class TiptesisComponent implements OnInit {

  /* DATOS CONDICIONALES */

  /* cargando = false;
  subio = true; */

  /* DATOS CONDICIONALES */

  constructor(private procesoService: ProcesosService, private router: Router, private formBuilder: FormBuilder) {
    this.buildForm();
  }
  mensaje: string;
  fileToUpload: File = null;
  archivoxd: any;
  tiposTesis: TipoTesis[];
  tipoTesis: TipoTesis = {
    IdTipoTesis: null,
    TipoTesis: null,
  };
  result = '';
  carga = false;
  form: FormGroup;
  FileUp = false;
  FileValShow = false;
  ngOnInit(): void {
    this.getTipoTesis();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      TipName: ['', [Validators.required,
      Validators.minLength(6),
      Validators.maxLength(40),
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]]
    });
  }
  get CampoTip() {
    return this.form.get('TipName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion de un tipo de tesis.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id del tipo de tesis a modificar.
   */
  edit(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func getTipoDocs
   * @description Funcion que obtiene el listado con todos los tipos de tesis registrados en la base de datos.
   */
  getTipoTesis() {
    this.procesoService.tesisLista().subscribe(
      (data: TipoTesis[]) => {
        this.tiposTesis = data;
        console.log(data);
      }
    );
  }
   /**
   * @func eliminar
   * @description Funcion que elimina un tipo de tesis.
   * @param id parametro que indica el id de un tipo de tesis para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarTipoTesis(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getTipoTesis();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra un tipo de tesis en la base de datos.
   */
  registrar() {
    console.log('hizo clic en grabar');
    console.log(this.tipoTesis);
    this.procesoService.crearTipoTesis(this.tipoTesis).subscribe((data) => {
      console.log(data);
      alert('Tipo de Tesis Guardado');
      this.getTipoTesis();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data de un tipo de tesis.
   * @param id parametro que indica el id de un tipo de tesis para obtener su data y visualizarla
   */
  ver(id: number) {
    this.procesoService.verTipoTesis(id).subscribe(
      (data: TipoTesis) => {
        this.tipoTesis = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func editar
   * @description Funcion que permite editar la data de un tipo de tesis.
   * @param id parametro que indica el id de un tipo de tesis para editar su data y actualizarla
   */
  editar(id: number) {
    this.procesoService.editarTipoTesis(id, this.tipoTesis).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        /*   this.screges = true;
          this.scregoc = false; */
        this.getTipoTesis();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getPlantillaTipoTesis
   * @description Funcion que permite obtener(descargar) el archivo plantilla de tipo de tesis
   */
  getPlantillaTipoTesis() {
    this.procesoService.descargarPlantillaTipoTesis().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Tipo Tesis';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });

  }
  /**
   * @func cargar
   * @description Funcion que permite el subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirTipoTesis(this.fileToUpload).subscribe(data => {
      alert('los datos fueron cargados');
      this.getTipoTesis();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  else{
   this.FileValShow = true;
  }
  }
}
