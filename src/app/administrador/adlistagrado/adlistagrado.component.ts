import { Escuela } from 'src/app/interfaces/escuela';
import { Grado } from './../../interfaces/grado';
import { Component, OnInit } from '@angular/core';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adlistagrado',
  templateUrl: './adlistagrado.component.html',
  styleUrls: ['./adlistagrado.component.css']
})
export class AdlistagradoComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
  }
  archivoxd: any; // variable para descargar archivos
  mensaje: string; //contiene la informacion de la alerta
  form: FormGroup;
  /**
   * Variables para seleccionar Escuela
   * escuelas Objeto que lista las escuelas
   * escuela Objeto de una escuela
   */
  seleccion: Escuela;
  escuelas: Escuela[];
  escuela: Escuela;
  /**
   * Variables para seleccionar Grados
   * grados Objeto que lista las grados
   * grado Objeto de un grado
   */
  grados: Grado[];
  grado: Grado = {
    IdGrado: null,
    IdEscuela: null,
    Grado: null,
    Escuela: null,
  };
  /**
   * Booleanos que manejan el resultado de la carga de archivos en el sistema
   */
  result = '';
  carga = false;
  /** CONDICIONALES */
  cargando = false;
  subio = true;
  exito = false;
  closed = false;
  FileUp = false;
  FileValShow = false;
  /** */
  filee: File; //manejador de archivos
  fileToUpload: File = null; //manejador de archivos

  ngOnInit(): void {
    this.getGrados();
    this.getEscuelas();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      graName: ['', [Validators.required,
      Validators.minLength(6),
      Validators.maxLength(90),
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]],
      facName: ['', [Validators.required]],
      escid: ['', [Validators.required]]
    });
  }
  get CampoGra() {
    return this.form.get('graName');
  }
  get CampoFac() {
    return this.form.get('facName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion de un grado.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id del grado a modificar.
   */
  edit(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getPlantillaGrado
   * @description Funcion que permite obtener(descargar) el archivo plantilla de grado.
   */
  getPlantillaGrado() {
    this.procesoService.descargarPlantillaGrado().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Grado';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite el subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirGrado(this.fileToUpload).subscribe(
      data => {
        console.log('Subiendo archivo');
        this.subio = false;
        this.cargando = true;
      },
      (err: HttpErrorResponse) => {
        alert(err.error.message);
      },
      () => {
        this.procesoService.listaGrado().subscribe(
          (data: Grado[]) => {
            this.grados = data;
            console.log(data);
          });
        this.subio = true;
        this.cargando = false;
        this.exito = true;
      });
    }
    else{
     this.FileValShow = true;
    }
  }
  /**
   * @func getGrados
   * @description Funcion que obtiene el listado con todos los grados registrados en la base de datos.
   */
  getGrados() {
    this.procesoService.listaGrado().subscribe(
      (data: Grado[]) => {
        this.grados = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func eliminar
   * @description Funcion que elimina un grado.
   * @param id parametro que indica el id de un grado para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarGrado(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getGrados();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra un grado en la base de datos.
   */
  registrar() {
    this.procesoService.crearGrado(this.grado).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        this.getGrados();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func getEscuelas
   * @description Funcion que obtiene el listado con todos las escuelas registrados en la base de datos.
   */
  getEscuelas() {
    this.procesoService.getAllEscuelas().subscribe(
      (data: Escuela[]) => {
        this.escuelas = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func onValueChanged
   * @description Funcion que maneja el evento de seleccion de escuela en un comboBox.
   * @param event parametro que indica el evento con el cual se selecciono la escuela
   */
  onValueChanged(event: Event) {
    this.grado.Escuela = this.seleccion.Escuela;
    this.grado.IdEscuela = this.seleccion.idEscuela;
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data de un grado.
   * @param id parametro que indica el id de un grado para obtener su data y visualizarla
   */
  ver(id: number) {
    this.procesoService.verGrado(id).subscribe(
      (data: Grado) => {
        this.grado = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func editar
   * @description Funcion que permite editar la data de un grado.
   * @param id parametro que indica el id de un grado para editar su data y actualizarla
   */
  editar(id: number) {
    console.log(this.grado.Grado);
    this.procesoService.editarGrado(id, this.grado).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        this.getGrados();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func reset
   * @description Funcion que vuelve las variables de las alertas en vacias
   */
  Reset() {
    this.exito = false;
  }
}
