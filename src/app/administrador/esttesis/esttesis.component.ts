import { ProcesosService } from 'src/app/servicios/procesos.service';
import { EstadoTesis } from './../../interfaces/estadotesis';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-esttesis',
  templateUrl: './esttesis.component.html',
  styleUrls: ['./esttesis.component.css']
})
export class EsttesisComponent implements OnInit {
  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
  }
  mensaje: string;
  fileToUpload: File = null;
  archivoxd: any;
  estadosTesis: EstadoTesis[];
  form: FormGroup;
  estadoTesis: EstadoTesis = {
    IdEstado: null,
    Estado: null,
    Modificable: '0',
  };
  result = '';
  carga = false;
  FileUp = false;
  FileValShow = false;
  ngOnInit(): void {
    this.getEstadoTesisLista();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      estName: ['', [Validators.required,
      Validators.minLength(5),
      Validators.maxLength(40),
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]]
    });
  }
  get CampoEst() {
    return this.form.get('estName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion de un estado de tesis.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id del estado de tesis a modificar.
   */
  edit(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
   /**
   * @func getTipoDocs
   * @description Funcion que obtiene el listado con todos los estados de tesis registrados en la base de datos.
   */
  getEstadoTesisLista() {
    this.procesoService.tesisEstadoLista().subscribe(
      (data: EstadoTesis[]) => {
        this.estadosTesis = data;
        console.log(this.estadoTesis);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
   /**
   * @func eliminar
   * @description Funcion que elimina un estado de tesis.
   * @param id parametro que indica el id de un estado de tesis para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarEstadoTesis(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getEstadoTesisLista();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra estado de tesis en la base de datos.
   */
  registrar() {
    console.log('hizo clic en grabar');
    console.log(this.estadoTesis);
    this.procesoService.crearEstadoTesis(this.estadoTesis).subscribe((data) => {
      console.log(data);
      alert('Estado de Tesis Guardado');
      this.getEstadoTesisLista();

    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data deestado de tesis.
   * @param id parametro que indica el id de estado de tesis para obtener su data y visualizarla
   */
  ver(id: number) {
    this.procesoService.verEstadoTesis(id).subscribe(
      (data: EstadoTesis) => {
        this.estadoTesis = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
   /**
   * @func editar
   * @description Funcion que permite editar la data de estado de tesis.
   * @param id parametro que indica el id de estado de tesis para editar su data y actualizarla
   */
  editar(id: number) {
    this.procesoService.editarEstadoTesis(id, this.estadoTesis).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        /*   this.screges = true;
          this.scregoc = false; */
        this.getEstadoTesisLista();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
/**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getPlantillaEstadoTesis
   * @description Funcion que permite obtener(descargar) el archivo plantilla de estado de tesis.
   */
  getPlantillaEstadoTesis() {
    this.procesoService.descargarPlantillaEstadoTesis().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla EstadoTesis';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite el subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirEstadoTesis(this.fileToUpload).subscribe(data => {
      alert('los datos fueron cargados');
      this.getEstadoTesisLista();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  else{
   this.FileValShow = true;
  }
  }
}
