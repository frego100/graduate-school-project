import { Component, OnInit } from '@angular/core';
import { Facultad } from 'src/app/interfaces/facultad';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adlistafacultad',
  templateUrl: './adlistafacultad.component.html',
  styleUrls: ['./adlistafacultad.component.css']
})
export class AdlistafacultadComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
  }
  form: FormGroup;
  /**
   * Variables para seleccionar Facultad
   * facultades Objeto que lista las facultades
   * facultad Objeto de una facultad
   */
  facultades: Facultad[];
  facultad: Facultad = {
    IdFacultad: null,
    Facultad: null,
  };
  mensaje: string; //contiene la informacion de la alerta
  fileToUpload: File = null; //manejador de archivos
  archivoxd: any; // variable para descargar archivos
  /* CONDICIONAL */
  scregfac = false;
  scregoc = true;
  FileUp = false;
  FileValShow = false;
  /* CONDICONAL */
  /**
   * Booleanos que manejan el resultado de la carga de archivos en el sistema
   */
  result = '';
  carga = false;

  ngOnInit(): void {
    this.getFacultades();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      facName: ['', [Validators.required,
        Validators.minLength(10),
        Validators.maxLength(90),
        Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]]
    });
  }
  get CampoFac() {
    return this.form.get('facName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func modal2
   * @description Funcion que con un evento realiza la modificacion.
   * @param event parametro que indica el evento para realizar o no la modificacion de la data.
   */
  modal2(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editarFacul(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any): void {
    const selectFile = event.target.files[0];
    if (selectFile.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + selectFile.name;
      this.result += '<br>Tamaño: ' + selectFile.size;
      this.result += '<br>Tipo: ' + selectFile.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getFacultades
   * @description Funcion que obtiene el listado con todos las facultades registrados en la base de datos.
   */
  getFacultades() {
    this.procesoService.listaFacultades().subscribe(
      (data: Facultad[]) => {
        this.facultades = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func eliminar
   * @description Funcion que elimina una facultad.
   * @param id parametro que indica el id de una facultad para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarFacultad(id)
        .subscribe((data) => {
          console.log(data);
          this.getFacultades();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra una facultad en la base de datos.
   */
  registrar() {
    console.log('hizo clic en grabar');
    console.log(this.facultad);
    this.procesoService.crearFacultad(this.facultad).subscribe((data: Mensajes) => {
      this.mensaje = data.Mensaje;
      this.scregfac = true;
      this.scregoc = false;
      this.getFacultades();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  /**
   * @func verFacul
   * @description Funcion que permite obtener la data de una facultad.
   * @param id parametro que indica el id de una facultad para obtener su data y visualizarla
   */
  verFacul(id: number) {
    this.procesoService.verFacultad(id).subscribe(
      (data: Facultad) => {
        this.facultad = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func editarFacul
   * @description Funcion que permite editar la data de una facultad.
   * @param id parametro que indica el id de una facultad para editar su data y actualizarla
   */
  editarFacul(id: number) {
    this.procesoService.editarFacultad(id, this.facultad).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        this.scregfac = true;
        this.scregoc = false;
        this.getFacultades();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func handleFileInput
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getPlantillaFacultad
   * @description Funcion que permite obtener(descargar) el archivo plantilla de escuelas.
   */
  getPlantillaFacultad() {
    this.procesoService.descargarPlantillaFacultad().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Facultad';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.postFileFacultad(this.fileToUpload).subscribe(data => {
      alert('los datos fueron cargados');
      this.getFacultades();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
    });
  }
  else{
   this.FileValShow = true;
  }
  }
  /**
   * @func restablecer
   * @description Funcion que vuelve las variables de los formularios en vacias
   */
  reestablecer() {
    this.scregfac = false;
    this.scregoc = true;
  }
}
