import { InformesCampos } from './../../interfaces/informes';
import { Facultad } from './../../interfaces/facultad';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
/* import print from 'print-js'; */
import * as printJS from 'print-js';
import { Informes } from 'src/app/interfaces/informes';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Escuela } from 'src/app/interfaces/escuela';
import { Grado } from 'src/app/interfaces/grado';
import { TipoTesis } from 'src/app/interfaces/tipotesis';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styleUrls: ['./informes.component.css']
})
export class InformesComponent implements OnInit {
  constructor(private procesoService: ProcesosService, private router: Router,
              private modalService: NgbModal, private formBuilder: FormBuilder) {
    this.buildForm();
  }
  someJSONdata: JSON;
  dni: number;
  datos: Informes[];
  datosSeleccionados: Informes[] = [];
  form: FormGroup;

  /* FROMATO CONDICINOAL */
  uno = false;
  dos = false;
  tres = false;
  stpct1 = false;
  stpct2 = false;
  stpct3 = false;
  stpct4 = false;
  stpct5 = false;
  stpct6 = false;
  stpct0 = false;
  filas = false;

  DNITB = true;
  nombreTB = true;
  GradoTB = true;
  escuelaTB = true;
  tituloTB = true;
  asesorTB = true;
  tipoTB = true;
  estadoTB = true;
  fechregTB = true;
  fechrevTB = true;
  fechacTB = true;
  fechasTB = true;
  /* FORMATO CONDICIONAL */
  escuela: Escuela;
  seleccionEscuela: Escuela = {
    idEscuela: null,
    IdFacultad: null,
    Facultad: null,
    Escuela: null,
    Directorio: null,
  };
  facultad: Facultad;
  seleccionFacultad: Facultad = {
    IdFacultad: null,
    Facultad: null,
  };
  tipotesis: TipoTesis;
  seleccionTipoTesis: TipoTesis = {
    IdTipoTesis: null,
    TipoTesis: null,
  };
  grado: Grado;
  grados: Grado[];
  escuelas: Escuela[];
  facultades: Facultad[];
  tipostesis: TipoTesis[];
  seleccionGrado: Grado = {
    IdGrado: null,
    IdEscuela: null,
    Grado: null,
    Escuela: null,
  };
  datosReporte: Informes = {
    id: null,
    DNI: null,
    Nombre: null,
    Escuela: null,
    Estado: null,
    Facultad: null,
    TipoTesis: null,
    FechaActualizacion: null,
    FechaFinAsesoria: null,
    FechaFinRevision: null,
    FechaGraduacion: null,
    FechaRegistro: null,
    Grado: null,
    TituloTesis: null,
    jurados: [],
    disabled: null,
  };
  mandarCampos: InformesCampos = {
    DNI: null,
    Nombre: null,
    idFacultad: null,
    idEscuela: null,
    IdGrado: null,
    IdTipoTesis: null,
    FechaDesde: null,
    FechaFin: null,
    FechaInicioActualizacion: null,
    FechaFinActualizacion: null,
    FechaInicioAsesoria: null,
    FechaFinAsesoria: null,
    FechaInicioGraduacion: null,
    FechaFinGraduacion: null,
    Asesor: null,
    Jurado: null,
  };

  marcas: boolean;
  /**
   * Variables para seleccion de checks
   */
  checked = false;
  loading = false;
  indeterminate = false;
  listOfCurrentPageData: Informes[] = [];
  setOfCheckedId = new Set<number>();
  /* STEPS */
  current = 0;
  ngOnInit(): void {
    this.getFacultades();
    this.getTipoTesis();
    this.marcas = false;

    this.uno = true;
    this.dos = false;
    this.tres = false;
  }
  private buildForm() {
    this.form = this.formBuilder.group({
      UserName: ['', ],
      DNIName: ['', ],
      facName: ['', ],
      EscName: ['', ],
      graName: ['', ],
      tipName: ['', ],
      asName: ['', ],
      jurName: ['', ],
      faDesName: ['', ],
      faHasName: ['', ],
      faAsDesName: ['', ],
      faAsHasName: ['', ],
      faGraDesName: ['', ],
      faGraHasDesName: ['', ],
      faReDesName: ['', ],
      faReHasName: ['', ],
    });
  }
  // Inicio de funciones usadas para selección de checks
  updateCheckedSet(id: number, checked: boolean): void {
    this.datosSeleccionados = [];
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
    // console.log(this.setOfCheckedId);
    // console.log(this.datos);
    for (const currentId of this.setOfCheckedId) {
      // console.log(currentId);
      // tslint:disable-next-line: prefer-for-of
      for (let j = 0; j < this.datos.length; j++) {
        // console.log("this.datos[j].id="+this.datos[j].id);
        if (currentId === this.datos[j].id) {
          // console.log(this.datos[j]);
          console.log('Datos Seleccionados:');
          this.datosSeleccionados.push(this.datos[j]);
          break;
        }
      }
    }
    console.log(this.datosSeleccionados);
  }

  onCurrentPageDataChange(listOfCurrentPageData: Informes[]): void {
    this.listOfCurrentPageData = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    const listOfEnabledData = this.listOfCurrentPageData.filter(({ disabled }) => !disabled);
    this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(checked: boolean): void {
    this.listOfCurrentPageData.filter(({ disabled }) => !disabled).forEach(({ id }) => this.updateCheckedSet(id, checked));
    this.refreshCheckedStatus();
  }

  sendRequest(): void {
    this.loading = true;
    const requestData = this.datos.filter(data => this.setOfCheckedId.has(data.id));
    console.log(requestData);
    setTimeout(() => {
      this.setOfCheckedId.clear();
      this.refreshCheckedStatus();
      this.loading = false;
    }, 1000);
  }
  // Fin de funciones usadas para selección de checks
  /* ---------MODAL---------- */
  openXl(content) {
    this.modalService.open(content, { size: 'xl' });
    /* this.calculo(); */
  }
  /* ---------MODAL---------- */
  /* ----------IMPRIMIR --------*/
  print() {
    printJS({
      printable: this.someJSONdata,
      type: 'html',
      header: 'Multiple Images',

    });
  }
  getEscuelas() {
    this.procesoService.getAllEscuelas().subscribe(
      (data: Escuela[]) => {
        this.escuelas = data;
      });
  }
  getFacultades() {
    this.procesoService.listaFacultades().subscribe(
      (data: Facultad[]) => {
        this.facultades = data;
        console.log(data);
      });
  }
  getTipoTesis() {
    this.procesoService.tesisLista().subscribe(
      (data: TipoTesis[]) => {
        this.tipostesis = data;
        console.log(data);
      });
  }
  onValueChangedFacultad(event: Event) {
    this.facultad = this.seleccionFacultad;
    this.mandarCampos.idFacultad = this.seleccionFacultad.IdFacultad;
    this.getEscuelaxFacultad(this.seleccionFacultad.IdFacultad);
  }
  onValueChangedEscuela(event: Event) {
    this.escuela = this.seleccionEscuela;
    this.mandarCampos.idEscuela = this.seleccionEscuela.idEscuela;
    this.getGradoxEscuela(this.seleccionEscuela.idEscuela);
  }
  onValueChangedGrado(event: Event) {

    this.mandarCampos.IdGrado = this.seleccionGrado.IdGrado;

  }
  onValueChangedTipoTesis(event: Event) {
    this.tipotesis = this.seleccionTipoTesis;
    this.mandarCampos.IdTipoTesis = this.seleccionTipoTesis.IdTipoTesis;

  }
   /**
   * @function getEscuelaxFacultad
   * @description Esta función obtiene las escuelas registrados por facultad
   */
  getEscuelaxFacultad(id: number) {
    this.procesoService.listarEscuelaxFacultad(id).subscribe(
      (data: Escuela[]) => {
        this.escuelas = data;
        console.log(this.escuelas);
      });
  }
  /**
   * @function getGradoxEscuela
   * @description Esta función obtiene los grados registrados por escuela
   */
  getGradoxEscuela(id: number) {
    this.procesoService.listarGradoxEscuela(id).subscribe(
      (data: Grado[]) => {
        this.grados = data;
        console.log(this.grados);
      }
    );
  }

  onvaluechangeFecha(event) {
  }
  /*RESPORTES*/
  /**
   * @function buscarReporte
   * @description Esta función busca el reporte generado mediante consultas al backend
   */
  buscarReporte() {
    console.log(this.mandarCampos.FechaDesde);
    console.log(this.datosReporte);
    this.procesoService.datosReporte(this.mandarCampos).
      subscribe(
        (data: Informes[]) => {
          this.datos = data;
          console.log(data);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
  }
  /* FIN REPORTES */
  /**
   * @function marcartodo
   * @description Esta función marca/desmarca todos los checks del listado de estudiantes registrados
   */
  marcartodo(isChecked: boolean) {
    if (isChecked) {
      this.marcas = true;
    } else {
      this.marcas = false;
    }
  }
  /**
   * @function next
   * @description Esta función controla el contenido de la cartilla  en el formulario paso a paso
   */
  next() {
    this.current += 1;
    this.changeContent();

  }
  /**
   * @function next0
   * @description Esta función controla el contenido de la cartilla  en el formulario paso a paso
   */
  next0(): void {
    this.current += 1;
    this.filas = true;
    this.stpct0 = true;
    this.stpct1 = false;
    this.stpct2 = false;
    this.stpct3 = false;
    this.stpct4 = false;
    this.stpct5 = false;
    this.stpct6 = false;
    this.changeContent();
  }
  /**
   * @function next1
   * @description Esta función controla el contenido de la cartilla 1 en el formulario paso a paso
   */
  next1(): void {
    this.current += 1;
    this.filas = false;
    this.stpct0 = false;
    this.stpct1 = true;
    this.stpct2 = false;
    this.stpct3 = false;
    this.stpct4 = false;
    this.stpct5 = false;
    this.stpct6 = false;
    this.changeContent();
  }
/**
   * @function next2
   * @description Esta función controla el contenido de la cartilla 2 en el formulario paso a paso
   */
  next2(): void {
    this.current += 1;
    this.filas = false;
    this.stpct0 = false;
    this.stpct2 = true;
    this.stpct1 = false;
    this.stpct3 = false;
    this.stpct4 = false;
    this.stpct5 = false;
    this.stpct6 = false;
    this.changeContent();
  }
  /**
   * @function next3
   * @description Esta función controla el contenido de la cartilla 3 en el formulario paso a paso
   */
  next3(): void {
    this.current += 1;
    this.stpct0 = false;
    this.filas = false;
    this.stpct3 = true;
    this.stpct2 = false;
    this.stpct1 = false;
    this.stpct4 = false;
    this.stpct5 = false;
    this.stpct6 = false;
    this.changeContent();
  }
  /**
   * @function next4
   * @description Esta función controla el contenido de la cartilla 4 en el formulario paso a paso
   */
  next4(): void {
    this.current += 1;
    this.stpct0 = false;
    this.filas = false;
    this.stpct4 = true;
    this.stpct3 = false;
    this.stpct2 = false;
    this.stpct1 = false;
    this.stpct5 = false;
    this.stpct6 = false;
    this.changeContent();
  }
  /**
   * @function next5
   * @description Esta función controla el contenido de la cartilla 5 en el formulario paso a paso
   */
  next5(): void {
    this.current += 1;
    this.stpct0 = false;
    this.filas = false;
    this.stpct4 = false;
    this.stpct6 = false;
    this.stpct5 = true;
    this.stpct3 = false;
    this.stpct2 = false;
    this.stpct1 = false;
    this.changeContent();
  }
  /**
   * @function next6
   * @description Esta función controla el contenido de la cartilla 6 en el formulario paso a paso
   */
  next6(): void {
    this.stpct0 = false;
    this.filas = false;
    this.current += 1;
    this.stpct6 = true;
    this.stpct4 = false;
    this.stpct5 = false;
    this.stpct3 = false;
    this.stpct2 = false;
    this.stpct1 = false;
    this.changeContent();
  }
   /**
   * @function changeContent
   * @description Esta función gestiona la interacción en el formulario paso a paso
   */
  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.uno = true;
        this.dos = false;
        this.tres = false;
        break;
      }
      case 1: {
        this.uno = false;
        this.dos = true;
        this.tres = false;
        break;
      }
      case 2: {
        this.uno = false;
        this.dos = false;
        this.tres = true;
        break;
      }
      default: {
      }
    }
  }

  done(): void {
    console.log('done');

  }
  pre(): void {
    this.current -= 1;
    this.changeContent();
  }


  /* STEPS */

  /* CAMBIO CHECKS */
  onChange(isChecked: boolean) {


  }

  /* CHECKS IMFORME */
  /**
   * @function Dnicheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de DNI en el informe dinamico
   */
  Dnicheck(isChecked: boolean) {
    if (isChecked) {
      this.DNITB = true;
    } else {
      this.DNITB = false;
    }
  }
  /**
   * @function Nombrecheck
   * @description Esta función se encarga de mostrar u ocultar la opcion nombre en el informe dinamico
   */
  Nombrecheck(isChecked: boolean) {
    if (isChecked) {
      this.nombreTB = true;
    } else {
      this.nombreTB = false;
    }
  }
  /**
   * @function Gradocheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de grado en el informe dinamico
   */
  Gradocheck(isChecked: boolean) {
    if (isChecked) {
      this.GradoTB = true;
    } else {
      this.GradoTB = false;
    }
  }
  /**
   * @function Escuelacheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de escuela en el informe dinamico
   */
  Escuelacheck(isChecked: boolean) {
    if (isChecked) {
      this.escuelaTB = true;
    } else {
      this.escuelaTB = false;
    }
  }
  /**
   * @function Titulocheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de titulo en el informe dinamico
   */
  Titulocheck(isChecked: boolean) {
    if (isChecked) {
      this.tituloTB = true;
    } else {
      this.tituloTB = false;
    }
  }
  /**
   * @function Asesorcheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de asesor en el informe dinamico
   */
  Asesorcheck(isChecked: boolean) {
    if (isChecked) {
      this.asesorTB = true;
    } else {
      this.asesorTB = false;
    }
  }
  /**
   * @function Tipocheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de tipo de tesis en el informe dinamico
   */
  Tipocheck(isChecked: boolean) {
    if (isChecked) {
      this.tipoTB = true;
    } else {
      this.tipoTB = false;
    }
  }
  /**
   * @function Estadocheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de estado en el informe dinamico
   */
  Estadocheck(isChecked: boolean) {
    if (isChecked) {
      this.estadoTB = true;
    } else {
      this.estadoTB = false;
    }
  }
  /**
   * @function FechaRegcheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de fecha de registro en el informe dinamico
   */
  FechaRegcheck(isChecked: boolean) {
    if (isChecked) {
      this.fechregTB = true;
    } else {
      this.fechregTB = false;
    }
  }
  /**
   * @function FechaRevcheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de fecha de fin de revisión en el informe dinamico
   */
  FechaRevcheck(isChecked: boolean) {
    if (isChecked) {
      this.fechrevTB = true;
    } else {
      this.fechrevTB = false;
    }
  }
  /**
   * @function FechaAccheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de fecha de fin de actualización en el informe dinamico
   */
  FechaAccheck(isChecked: boolean) {
    if (isChecked) {
      this.fechacTB = true;
    } else {
      this.fechacTB = false;
    }
  }
  /**
   * @function FechaAscheck
   * @description Esta función se encarga de mostrar u ocultar la opcion de fecha de fin de asesoria en el informe dinamico
   */
  FechaAscheck(isChecked: boolean) {
    if (isChecked) {
      this.fechasTB = true;
    } else {
      this.fechasTB = false;
    }
  }
  /**
   * @function limpiarForm
   * @description Esta función se encarga de limpiar el contenido del modal de impresión de informes luego del proceso de carga
   */
  limpiarForm() {
    this.form.reset();
  }

   /**
   * @function resetImp
   * @description Esta función se encarga de reestablecer el contenido del modal de impresión de informes luego del proceso de carga
   */
  resetImp(){
    this.current = 0;
    this.uno = true;
    this.dos = false;
    this.tres = false;
    this.marcas = false;

    this.changeContent();
  }
}
