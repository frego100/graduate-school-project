import { ProcesoActividad } from 'src/app/interfaces/procesoactividad';
import { RolProceso } from 'src/app/interfaces/rolproceso';
import { Router, ActivatedRoute } from '@angular/router';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Component, OnInit, Input } from '@angular/core';
import { Rol } from 'src/app/interfaces/rol';
import { Proceso } from 'src/app/interfaces/proceso';

@Component({
  selector: 'app-verrol',
  templateUrl: './verrol.component.html',
  styleUrls: ['./verrol.component.css']
})

export class VerrolComponent implements OnInit {
  @Input() nombreRol: string;
  rolProceso: RolProceso ={
    IdProceso: null,
    NombreProceso: null,
  };
  rolesArray: RolProceso[];
  rolesProceso: RolProceso[];
  procesoActividad: ProcesoActividad[];
  rol: Rol;
  procesos: Proceso[];
  nombre: string;
  actividades = false;
  constructor(private procesoService: ProcesosService, private router: Router,
              private activatedRoute: ActivatedRoute,
               ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    this.getRolLista();
    this.getRolProceso(id);
    this.getNombreRol(id);
  }
/**
   * @function getRolLista
   * @description Función que lista los procesos registrados
   */
  getRolLista(){
    this.procesoService.procesoLista().subscribe(
      (data: Proceso[]) =>{
        this.procesos= data ;
        console.log('procesos',this.procesos);
      }
      );

  }
  /**
   * @function getRolProceso
   * @description Función que lista los procesos permitidos segun el rol deacuerdo al ID de este.
   * @param id El id del rol a consultar.
   */
  getRolProceso(id:number){
    this.procesoService.RolProceso(id).subscribe(
      (data: RolProceso[]) =>{
        this.rolesArray= data ;
        console.log('procesos permitidos segun su rol', this.rolesArray);
      }
      );
     console.log("el id es:",id);
  }
  /**
   * @function getNombreRol
   * @description Función que obtiene el nombre del rol deacuerdo al ID de este.
   * @param id El id del rol a consultar.
   */
   getNombreRol(id: number){
    this.procesoService.verRol(id).subscribe(
      (data: any)=>{
        const datos = data;
        this.nombre = datos.NombreRol;
        console.log(datos.NombreRol);}
      );
     console.log("el id es:",id);
  }
  /**
   * @function enviar
   * @description Función que lista los detalles del proceso seleccionado como el nombre de la actividad y los permisos de este.
   * @param id El id del proceso a consultar.
   */
  enviar(id: number ){
    console.log('Envia ', id);
    this.actividades = true;
    this.procesoService.ProcesoActividad(id).subscribe(
      (data: ProcesoActividad[]) =>{
        this.procesoActividad= data ;
        console.log('ver todas actividades segun proceso', this.procesoActividad);
      }
      );
     console.log("el id es:",id);

      }
    }






