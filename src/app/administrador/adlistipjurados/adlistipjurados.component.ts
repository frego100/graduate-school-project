import { ProcesosService } from 'src/app/servicios/procesos.service';
import { TipoJurado } from './../../interfaces/tipojurado';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { HttpErrorResponse } from '@angular/common/http';
import { identity } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adlistipjurados',
  templateUrl: './adlistipjurados.component.html',
  styleUrls: ['./adlistipjurados.component.css']
})
export class AdlistipjuradosComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
  }
  mensaje: string;
  fileToUpload: File = null;
  archivoxd: any;
  tiposJurado: TipoJurado[];
  tipoJurado: TipoJurado = {
    IdTipoJurado: null,
    TipoJurado: null,
  };
  form: FormGroup;
  result = '';
  carga = false;
  FileUp = false;
  FileValShow = false;

  ngOnInit(): void {
    this.getTiposJurado();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      JurName: ['', [Validators.required,
      Validators.minLength(6),
      Validators.maxLength(40),
      /* Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ- \(\)\,\.\:\; ]+$/)]], */
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]]
    });
  }
  get CampoJur() {
    return this.form.get('JurName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion de un Tipo de jurado.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id del Tipo de jurado a modificar.
   */
  edit(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
   /**
   * @func getTipoDocs
   * @description Funcion que obtiene el listado con todos los Tipos de jurado registrados en la base de datos.
   */
  getTiposJurado() {
    this.procesoService.listaTiposJurado().subscribe(
      (data: TipoJurado[]) => {
        this.tiposJurado = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func eliminar
   * @description Funcion que elimina un Tipo de jurado.
   * @param id parametro que indica el id de un Tipo de jurado para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarTipoJurado(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getTiposJurado();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra un Tipo de jurado en la base de datos.
   */
  registrar() {
    console.log('hizo clic en grabar');
    console.log(this.tipoJurado);
    this.procesoService.crearTipoJurado(this.tipoJurado).subscribe((data) => {
      console.log(data);
      alert('Tipo Jurado Guardado');
      this.getTiposJurado();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data de un Tipo de jurado.
   * @param id parametro que indica el id de un Tipo de jurado para obtener su data y visualizarla
   */
  ver(id: number) {
    this.procesoService.verTipoJurado(id).subscribe(
      (data: TipoJurado) => {
        this.tipoJurado = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func editar
   * @description Funcion que permite editar la data de un Tipo de jurado.
   * @param id parametro que indica el id de un Tipo de jurado para editar su data y actualizarla
   */
  editar(id: number) {
    this.procesoService.editarTipoJurado(id, this.tipoJurado).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        this.getTiposJurado();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
   /**
   * @func getPlantillaTipoDocumento
   * @description Funcion que permite obtener(descargar) el archivo plantilla de Tipo de jurado.
   */
  getPlantillaTipoJurado() {
    this.procesoService.descargarPlantillaTipoJurado().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Tipo Jurado';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite el subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirTipoJurado(this.fileToUpload).subscribe(data => {
      alert('los datos fueron cargados');
      this.getTiposJurado();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  else{
   this.FileValShow = true;
  }
  }
}
