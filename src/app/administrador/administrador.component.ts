import { Component, OnInit } from '@angular/core';
import { Token } from '../interfaces/token';
import { UserserviceService } from '../servicios/userservice.service';
import { Router } from '@angular/router';
import { Roles } from './../interfaces/roles';
import { Estudiante } from 'src/app/interfaces/estudiante';
import {TranslateService} from '@ngx-translate/core'

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {
  constructor( private userService: UserserviceService, private router: Router, private translate: TranslateService) {
    this.translate.setDefaultLang('en');
    this.translate.use('es');
    this.translate.addLangs(['es', 'en']);
    this.langs = this.translate.getLangs();
   }
  isCollapsed = false;
  /** MENU */
  VeOperador = false;
  VeDocente = false;
  VeEstudiante = false;
  Mest = false;
  langs: string[] = [];
  switchValue = false;
  /* /MENU */
  token: Token ={
    token: null,
  }
  est = localStorage.getItem('rolesE');
  doc = localStorage.getItem('rolesD');
  ope = localStorage.getItem('rolesO');
  filterPost: Roles;
  roles: Roles = {
    Estudiante: false,
    Docente: false,
    Operador: false,
  };
  nombre: string;
  estudiante: Estudiante={
      id: null,
      Nombre: null,
      idDNITipo: null,
      DNI: null,
      Correo: null,
      Operador: null,
      Estudiante: null,
      Docente: null,
      Direccion: null,
      Telefono: null,
  }
  ngOnInit(): void {
    this.rolEst();
    this.rolOpe();
    this.rolDoc();
    this.data();
  }

  rolEst(){
    if (this.est === '1'){
      this.roles.Estudiante = true;
      this.VeEstudiante = true;
      this.Mest = true;
    }
  }
  rolOpe(){
    if (this.ope === '1'){
      this.roles.Operador = true;
      this.VeEstudiante = true;
      this.VeDocente = true;
      this.VeOperador = true;
      this.Mest = true;
    }
  }
  rolDoc(){
    if (this.doc === '1'){
      this.roles.Docente = true;
      this.VeEstudiante = true;
      this.VeDocente = true;
    }
  }
  logout(){
    /*  this.userService.getToken().subscribe((data: any) => {
        this.token = data;
      });*/
      this.token.token = localStorage.getItem('token');
      //this.token.token = this.auth.getToken();
      // console.log(localStorage.getItem('token'));
      this.userService.logout(this.token).subscribe((data)=>{
        // console.log(data);
        alert('Sesion finalizada' );
        this.router.navigate( ['/login'] );
      },(error)=>{
          console.log(error);
          alert('Ocurrio un error');
       });
      localStorage.removeItem('token');
      localStorage.removeItem('rol');
    }
    toggleCollapsed(): void {
      this.isCollapsed = !this.isCollapsed;
    }
    data(){
      this.token.token = localStorage.getItem('token');
      this.userService.role(this.token).subscribe(
        (data: any)=>{
          const datos = data;
          this.nombre= datos[0].Nombre;
          ;}
        /*
        (data: Estudiante) => {
          this.estudiante = data;
          console.log(data);
          console.log(data.Nombre);
        } */);
    }
    /**
     * @function Video1
     * @description Funcion que apertura el listado de videotutoriales para Estudiantes
     */
    Video1(){
      window.open('https://youtube.com/playlist?list=PLLc4hCxAZCceO1KELuf7SZnM7TSAxrcQW', '_blank');
    }
    /**
     * @function Video2
     * @description Funcion que apertura el listado de videotutoriales para Docentes
     */
    Video2(){
      window.open('https://youtube.com/playlist?list=PLLc4hCxAZCcceeeehhisdV5UFPqjaSQrC', '_blank');
    }
    /**
     * @function Video3
     * @description Funcion que apertura el listado de videotutoriales para Operadores
     */
    Video3(){
      window.open('https://www.youtube.com/playlist?list=PLLc4hCxAZCcdPzsuXDpr1d5ZkB8fqZDaO', '_blank');
    }
    /**
     * @function Documento1
     * @description Funcion que apertura el manual de usuario para Docentes
     */
    Documento1(){
      window.open('https://drive.google.com/file/d/1XtWgyImmShU4XQ8eXejHMQyHr0sADs3N/view?usp=sharing', '_blank');
    }
    /**
     * @function Documento2
     * @description Funcion que apertura el manual de usuario para Estudiantes
     */
    Documento2(){
      window.open('https://drive.google.com/file/d/1JhmrB6Zr8ahptlcMiP4zTd9MB4vqSRBa/view?usp=sharing', '_blank');
    }
    /**
     * @function Documento3
     * @description Funcion que apertura el manual de usuario para Operadores
     */
    Documento3(){
      window.open('https://drive.google.com/file/d/1q6jEUPT4VjSz7S7-Gps4B6aDJFZqLQe2/view?usp=sharing', '_blank');
    }

    /**
     * @function changeLangEn
     * @description Funcion que realiza la traduccion a español del texto estatico
     */
    changeLangEs(){
      this.translate.use('es');
    }
    /**
     * @function changeLangEn
     * @description Funcion que realiza la traduccion a ingles del texto estatico
     */
    changeLangEn(){
      this.translate.use('en');
    }
}
