import { Roles } from './../../interfaces/roles';
import { Component, OnInit } from '@angular/core';
import { Estudiante } from 'src/app/interfaces/estudiante';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserserviceService } from '../../servicios/userservice.service';
import { Rol } from 'src/app/interfaces/rol';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Estudiantec } from 'src/app/interfaces/estudiantec';
import { asapScheduler } from 'rxjs';
import { EventEmitter } from 'protractor';
import { HttpErrorResponse } from '@angular/common/http';
/// interface para campos repetidos//
interface Respuesta {
  Campo: string;
  Existe: string;
}
////
@Component({
  selector: 'app-adregistro',
  templateUrl: './adregistro.component.html',
  styleUrls: ['./adregistro.component.css']
})
export class AdregistroComponent implements OnInit {

  // tslint:disable-next-line: max-line-length
  constructor(private procesoService: ProcesosService, private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) {
    this.buildForm();
  }
  get CampoTipDoc() {
    return this.form.get('TipDocName');
  }
  get CampoUs() {
    return this.form.get('UserName');
  }
  get CampoCor() {
    return this.form.get('UserCorreo');
  }
  get CampoDNI() {
    return this.form.get('UserDNI');
  }
  get CampoCel() {
    return this.form.get('UserCel');
  }
  get CampoDir() {
    return this.form.get('UserDir');
  }
  websiteList: string[] = ['Operador', 'Docente', 'Estudiante'];
  // optionList: Rol[];
  selectedUser?: string;
  isLoading = false;
  nombreRol: string[] = [];
  idTodos: number[] = [];
  nombreRolFijo: string[] = [];
  localRol = null;
  rolError = false;

  estudiante: Estudiantec = {
    id: null,
    Nombre: null,
    idDNITipo: null,
    DNI: null,
    Correo: null,
    Operador: 0,
    Estudiante: 0,
    Docente: 0,
    Direccion: null,
    Telefono: null,
    ListaRoles: [],
  };
  form: FormGroup;
  roles = [];
  roles1 = [];
  rolesL: Rol[];
  rolcitos: Roles = {
    Estudiante: null,
    Operador: null,
    Docente: null,
  };
  cargando = false;
  est = 0;
  doc = 0;
  op = 0;

  onSearch(value: string): void {
    this.isLoading = true;
  }
  onCreate(uname) {
    if (uname != null) {
      const indice = this.nombreRol.indexOf(uname.value); // obtenemos el indice
      /* const indexUni = this.estudiante.ListaRoles.indexOf(5);
      const indexPro = this.estudiante.ListaRoles.indexOf(6); */
      if ( uname.value === 'Secretaria de Unidad' || uname.value === 'Secretaria de Programa'){
        const indexope = this.estudiante.ListaRoles.indexOf(2);
        this.estudiante.ListaRoles.splice(indexope, 1);
      }
      if (uname.value === 'Estudiante') {
        // tslint:disable-next-line: new-parens
        this.rolcitos.Estudiante = true;
        this.estudiante.Estudiante = 1;
      }
      if (uname.value === 'Operador') {
        this.rolcitos.Operador = true;
        this.estudiante.Operador = 1;
      }
      if (uname.value === 'Docente') {
        this.rolcitos.Docente = true;
        this.estudiante.Docente = 1;
      }
      const indiceFijo = this.nombreRolFijo.indexOf(uname.value);
      console.log('este: ', uname.value);
      this.localRol = uname.value;
      // console.log(this.idTodos[indiceFijo]);
      this.roles.push({
        name: uname.value
      });
      this.estudiante.ListaRoles.push(this.idTodos[indiceFijo]);
      console.log(this.estudiante.ListaRoles);
      console.log('estudiante', this.estudiante);
      delete this.nombreRol[indice];
      this.nombreRol = this.nombreRol.filter((i) => i !== '');
    
    }
  }
  checks(uname: string) {
    if (uname != null) {
      const indice = this.nombreRol.indexOf(uname); // obtenemos el indice
      // tslint:disable-next-line: triple-equals
      /* const indexUni = this.estudiante.ListaRoles.indexOf(5);
      const indexPro = this.estudiante.ListaRoles.indexOf(6);
      if ( indexUni !== -1 || indexPro !== -1){
        this.estudiante.ListaRoles.splice(2, 1);
      } */
      if (uname == 'Estudiante') {
        // tslint:disable-next-line: new-parens
        this.rolcitos.Estudiante = true;
        this.estudiante.Estudiante = 1;
        this.est = 1;
      }
      else {
        this.est = 0;
      }
      if (uname === 'Operador') {
        this.rolcitos.Operador = true;
        this.estudiante.Operador = 1;
        this.op = 1;
      }
      else {
        this.op = 0;
      }
      if (uname === 'Docente') {
        this.rolcitos.Docente = true;
        this.estudiante.Docente = 1;
        this.doc = 1;
      }
      else {
        this.doc = 0;
      }
      const indiceFijo = this.nombreRolFijo.indexOf(uname);
      console.log('este: ', uname);
      this.roles.push({
        name: uname
      });
      this.localRol = uname;
      this.estudiante.ListaRoles.push(this.idTodos[indiceFijo]);
      console.log(this.estudiante.ListaRoles);
      console.log('estudiante', this.estudiante);
      delete this.nombreRol[indice];
      this.nombreRol = this.nombreRol.filter((i) => i !== '');
    }
  }
  delete(uname1) {
    this.nombreRol.push(uname1);
    console.log('asd');
    console.log(uname1);
   /*  const indexUni = this.estudiante.ListaRoles.indexOf(5);
    const indexPro = this.estudiante.ListaRoles.indexOf(6);
    if ( indexUni !== -1 || indexPro !== -1){
      this.estudiante.ListaRoles.splice(2, 1);
    } */
    if ( uname1 === 'Secretaria de Unidad' || uname1 === 'Secretaria de Programa'){
      console.log('efe');
      if (this.estudiante.Operador === 1){
        const num = 2;
        const indexope = this.estudiante.ListaRoles.push(num);
        this.estudiante.ListaRoles.push(num)
        console.log('operador', indexope);
        
      }
      
    }
    if (uname1 === 'Estudiante') {
      this.rolcitos.Estudiante = false;
      this.estudiante.Estudiante = 0;
    }
    if (uname1 === 'Operador') {
      this.rolcitos.Operador = false;
      this.estudiante.Operador = 0;
    }
    if (uname1 === 'Docente') {
      this.rolcitos.Docente = false;
      this.estudiante.Docente = 0;
    }
    let ind;
    for (let index = 0; index < this.roles.length; index++) {
      if (uname1 === this.roles[index].name) {
        ind = index;
      }
    }
    console.log(ind);
    delete this.roles[ind];
    this.estudiante.ListaRoles.splice(ind, 1);
    console.log(this.estudiante.ListaRoles);
    console.log('estudiante', this.estudiante);
    this.roles = this.roles.filter((i) => i !== '');
  }
  uncheck(uname1: string) {
    if ( uname1 === 'Secretaria de Unidad' || uname1 === 'Secretaria de programa'){
      console.log('efe');
      if (this.estudiante.Operador === 1){
        const indexope = 2;
        console.log('operador',indexope);
      }
    }
    this.nombreRol.push(uname1);
    if (uname1 === 'Estudiante') {
      // tslint:disable-next-line: new-parens
      this.rolcitos.Estudiante = false;
      this.estudiante.Estudiante = 0;
    }
    if (uname1 === 'Operador') {
      this.rolcitos.Operador = false;
      this.estudiante.Operador = 0;
    }
    if (uname1 === 'Docente') {
      this.rolcitos.Docente = false;
      this.estudiante.Docente = 0;
    }
    let ind;
    for (let index = 0; index < this.roles.length; index++) {
      if (uname1 === this.roles[index].name) {
        ind = index;
      }
    }
    console.log(ind);
    delete this.roles[ind];
    this.estudiante.ListaRoles.splice(ind, 1);
   
    console.log('estudiante', this.estudiante);
    console.log(this.estudiante.ListaRoles);

    this.roles = this.roles.filter((i) => i !== '');
  }
  onCheckboxChange(e) {
    if (e.target.checked) {
      const index = this.websiteList.indexOf(e.target.value);
      const nombre = this.websiteList[index];
      console.log(nombre);
      this.checks(nombre);
      
      // this.websiteList.push((e.target.value));
    } else if (e.target.checked === false) {
      const index = this.websiteList.indexOf(e.target.value);
      const nombre = this.websiteList[index];
      console.log(nombre);
      this.uncheck(nombre);
      this.delete(nombre);
    }
  }
  ngOnInit(): void {

    this.getRolLista();
  }

  getRolLista() {
    this.procesoService.rolLista().subscribe(
      (data: Rol[]) => {
        this.rolesL = data;
        console.log(this.rolesL);
        for (let i = 0; i < this.rolesL.length; i++) {
          this.nombreRol[i] = this.rolesL[i].NombreRol;
          this.nombreRolFijo[i] = this.rolesL[i].NombreRol;
          this.idTodos[i] = this.rolesL[i].IdRol;
        }
        console.log(this.nombreRol);
      }
    );

  }
  private buildForm() {
    this.form = this.formBuilder.group({
      UserName: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(40), Validators.pattern(/^[a-zA-ZáéíóúáéíóúÁÉÍÓÚñÑäÄëËïÏöÖüÜ ]+$/)]],
      UserCorreo: ['', [Validators.required, Validators.pattern(/([a-z0-9A-Z]+)*@(?:[a-z0-9_](?:[a-z0-9-]*[a-z0-9])?\.)+([a-z0-9](?:[a-z0-9-]*[a-z0-9])?){2}/)]],
      UserDNI: ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{7}/), Validators.maxLength(20), Validators.minLength(8)]],
      UserCel: ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{8}/), Validators.maxLength(9), Validators.minLength(9)]],
      UserDir: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(60), Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ0-9- \#\º\(\)\,\.\:\°\  ]+$/)]],


      TipDocName: ['', [Validators.required]]

    });
  }
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid && (this.est + this.op + this.doc) !== 0) {
      this.registrar();
      this.cargando = true;
      const value = this.form.value;
      console.log('valor: ' + value);
      console.log('nombre rol: ' + this.nombreRol);
      this.rolError = false;
    } else {
      this.form.markAllAsTouched();
      this.cargando = false;
      this.rolError = true;
    }
  }
  registrar() {
    this.estudiante.idDNITipo = 1;
    console.log(this.estudiante);
    console.log('hizo clic en grabar');
    this.cargando = false;
    this.procesoService.crearEstudiante(this.estudiante).subscribe((data: Respuesta) => {
      const datos = data;
      /* this.cargando = false; */
      if (datos.Existe === 'true') {
        alert('No se registro, ya existe un usuario con el mismo: ' + datos.Campo);
      } else {
        alert('Registro exitoso');

        /* this.cargando = false; */
        this.router.navigate(['/adlistalumnos']);
      }
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.cargando = false;
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.' || err.error.message === 'Token has expired') {
        this.router.navigate(['/login']);
      }
    });
  }
}
