import { Router } from '@angular/router';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { TipoDNI } from './../../interfaces/tipodoc';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Mensajes } from 'src/app/interfaces/mensajes';

@Component({
  selector: 'app-adlista-doc',
  templateUrl: './adlista-doc.component.html',
  styleUrls: ['./adlista-doc.component.css']
})
export class AdlistaDocComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
  }
  mensaje: string; //contiene la informacion de la alerta
  fileToUpload: File = null; //manejador de archivos
  archivoxd: any; // variable para descargar archivos
  /**
   * tipoDocs Array que almacena un listado de tipo de documentos
   */
  tipoDocs: TipoDNI[];
  /**
   * variable que contendra la informacion de un Tipo de docuento
   */
  tipoDoc: TipoDNI = {
    IdDNITipo: null,
    TipoDNI: null,
  };
  /**
   * Booleanos que manejan el resultado de la carga de archivos en el sistema
   */
  result = '';
  carga = false;
  form: FormGroup;
  FileUp = false;
  FileValShow = false;

  ngOnInit(): void {
    this.getTipoDocs();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      docName: ['', [Validators.required,
      Validators.minLength(3),
      Validators.maxLength(40),
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]]
    });
  }
  get CampoDoc() {
    return this.form.get('docName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion de un tipo de documento.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id del tipo de documento a modificar.
   */
  edit(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func getTipoDocs
   * @description Funcion que obtiene el listado con todos los tipos de documento registrados en la base de datos.
   */
  getTipoDocs() {
    this.procesoService.listaTipoDoc().subscribe(
      (data: TipoDNI[]) => {
        this.tipoDocs = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func eliminar
   * @description Funcion que elimina un tipo de documento.
   * @param id parametro que indica el id de un tipo de documento para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarTipoDoc(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getTipoDocs();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra un tipo de documento en la base de datos.
   */
  registrar() {
    console.log('hizo clic en grabar');
    console.log(this.tipoDoc);
    this.procesoService.crearTipoDoc(this.tipoDoc).subscribe((data) => {
      console.log(data);
      alert('Tipo de Documento Guardado');
      this.getTipoDocs();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data de un tipo de dictamen.
   * @param id parametro que indica el id de un tipo de documento para obtener su data y visualizarla
   */
  ver(id: number) {
    this.procesoService.verTipoDoc(id).subscribe(
      (data: TipoDNI) => {
        this.tipoDoc = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
      });
  }
  /**
   * @func editar
   * @description Funcion que permite editar la data de un tipo de documento.
   * @param id parametro que indica el id de un tipo de documento para editar su data y actualizarla
   */
  editar(id: number) {
    console.log(this.tipoDoc.IdDNITipo);
    this.procesoService.editarTipoDoc(id, this.tipoDoc).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        this.getTipoDocs();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }

  /**
   * @func getPlantillaTipoDocumento
   * @description Funcion que permite obtener(descargar) el archivo plantilla de tipo de documento.
   */
  getPlantillaTipoDocumento() {
    this.procesoService.descargarPlantillaTipoDocumento().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Tipo Documento';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite el subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirTipoDocumento(this.fileToUpload).subscribe(data => {
      alert('los datos fueron cargados');
      this.getTipoDocs();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  else{
   this.FileValShow = true;
  }
  }
}
