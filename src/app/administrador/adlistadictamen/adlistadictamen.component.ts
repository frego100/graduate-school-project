import { Component, OnInit } from '@angular/core';
import { Dictamen } from 'src/app/interfaces/dictamen';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adlistadictamen',
  templateUrl: './adlistadictamen.component.html',
  styleUrls: ['./adlistadictamen.component.css']
})
export class AdlistadictamenComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
  }
  mensaje: string; //contiene la informacion de la alerta
  fileToUpload: File = null; //maneja documento
  archivoxd: any; // variable para descargar archivos
  /**
   * dictamenes Objeto que almacena un listado de dictamen
   */
  dictamenes: Dictamen[];
  /**
   * Contiene la informacion de un dictamen
   */
  dictamen: Dictamen = {
    IdDictamen: null,
    Dictamen: null,
  };
  /**
   * Booleanos que manejan el resultado de la carga de archivos en el sistema
   */
  form: FormGroup;
  result = '';
  carga = false;
  FileUp = false;
  FileValShow = false;

  ngOnInit(): void {
    this.getDictamenes();
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      dicName: ['', [Validators.required,
      Validators.minLength(6),
      Validators.maxLength(40),
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]]
    });
  }
  get CampoDic() {
    return this.form.get('dicName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion del dictamen.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id del dictamen a modificar.
   */
  edit(event: Event, id: number) {

    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func getDictamenes
   * @description Funcion que obtiene el listado con todos los dictamenes registrados en la base de datos.
   */
   getDictamenes() {
    this.procesoService.listaDictamenes().subscribe(
      (data: Dictamen[]) => {
        this.dictamenes = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func eliminar
   * @description Funcion que elimina un dictamen.
   * @param id parametro que indica el id de un dictamen para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarDictamen(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getDictamenes();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func registrar
   * @description Funcion que registra un dictamen en la base de datos.
   */
  registrar() {
    console.log('hizo clic en grabar');
    console.log(this.dictamen);
    this.procesoService.crearDictamen(this.dictamen).subscribe((data) => {
      console.log(data);
      alert('Dictamen Guardado');
      this.getDictamenes();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data de un dictamen.
   * @param id parametro que indica el id de un dictamen para obtener su data y visualizarla
   */
  ver(id: number) {
    this.procesoService.verDictamen(id).subscribe(
      (data: Dictamen) => {
        this.dictamen = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func editar
   * @description Funcion que permite editar la data de un dictamen.
   * @param id parametro que indica el id de un dictamen para editar su data y actualizarla
   */
  editar(id: number) {
    console.log(this.dictamen.Dictamen);
    this.procesoService.editarDictamen(id, this.dictamen).subscribe(
      (data: Mensajes) => {
        this.mensaje = data.Mensaje;
        alert(this.mensaje);
        this.getDictamenes();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getPlantillaDictamen
   * @description Funcion que permite obtener(descargar) el archivo plantilla de dictamen.
   */
  getPlantillaDictamen() {
    this.procesoService.descargarPlantillaDictamen().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Dictamen';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite el subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirDictamen(this.fileToUpload).subscribe(data => {
      alert('los datos fueron cargados');
      this.getDictamenes();
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    });
  }
  else{
   this.FileValShow = true;
  }
  }
}
