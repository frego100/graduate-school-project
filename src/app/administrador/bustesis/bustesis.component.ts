import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BuscaTesis, ResultadosBusquedaTesis } from 'src/app/interfaces/buscadorTesis';
import { ProcesosService } from 'src/app/servicios/procesos.service';

@Component({
  selector: 'app-bustesis',
  templateUrl: './bustesis.component.html',
  styleUrls: ['./bustesis.component.css']
})
export class BustesisComponent implements OnInit {
  isSpinning = false;
  camposTesis: BuscaTesis = {
    TituloTesis: null,
    Resumen: null,
    PalabrasClave: null,
  };
  resultadosBusqueda: ResultadosBusquedaTesis;
  tam:Number;
  constructor(private procesoService: ProcesosService, private router: Router) { }
  resultado = false;
  ngOnInit(): void {
  }
  ResultShow() {
    this.resultado = true;
  }
  /**
   * @func buscarCamposTesis
   * @description Funcion que realiza el proceso de busqueda de tesis deacuerdo a los datos proporcionados
   */
  buscarCamposTesis(){
    this.isSpinning = true;
    this.procesoService.buscarTesis(this.camposTesis).
    subscribe(
      (data: ResultadosBusquedaTesis) => {
        this.resultadosBusqueda = data;
        console.log(data);
        this.isSpinning = false;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });

  }
}
