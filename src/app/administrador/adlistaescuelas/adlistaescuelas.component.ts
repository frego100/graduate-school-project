import { EscuelaService } from './../../servicios/escuela.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Escuela } from 'src/app/interfaces/escuela';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { Facultad } from 'src/app/interfaces/facultad';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subject, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-adlistaescuelas',
  templateUrl: './adlistaescuelas.component.html',
  styleUrls: ['./adlistaescuelas.component.css']
})
export class AdlistaescuelasComponent implements OnInit {
  mensaje: string;
  /**
   * escuelas Objeto que lista las escuelas
   */
  escuelas: Escuela[];
  /**
   * escuela Objeto de tipo escuela.
   */
  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

model: any;
  escuela: Escuela = {
    idEscuela: null,
    IdFacultad: null,
    Escuela: null,
    Facultad: null,
    Directorio: null,
  };
  archivoxd: any; // variable para descargar archivos
  result = '';
  carga = false;
  /** CONDICIONALES */
  cargando = false;
  subio = true;
  exito = false;
  closed = false;
  FileUp = false;
  FileValShow = false;

  screges = false;
  scregoc = true;

  /**
   * Variables para seleccionar Facultad
   * facultades Objeto que lista las facultades
   * facultad Objeto de una facultad
   */
  seleccion: Facultad;
  facultades: Facultad[];
  facultad: Facultad;
  /**
   * Variables para archivos
   * fileToUpload file para subir data de escuela
   */
  fileToUpload: File = null;

  constructor(private procesoService: ProcesosService, private escuelaService: EscuelaService,
              private formBuilder: FormBuilder, private router: Router) {
    this.getEscuelas();
    this.getFacultades();
    this.buildForm();
  }
  form: FormGroup;
  ngOnInit(): void {
  }
  /**
   * @func buildForm
   * @description Funcion que permite realizar la validación de los campos.
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      escName: ['', [Validators.required,
        Validators.minLength(10),
        Validators.maxLength(90),
        Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ \(\)\,\.\:\;\-\ ]+$/)]],
        facName: [''],
        /* facName: ['', [Validators.required]], */
      escFac: ['', [Validators.required]]
    });
  }
  get CampoEsc() {
    return this.form.get('escName');
  }
  get CampoFac() {
    return this.form.get('facName');
  }
  /**
   * @func save
   * @description Funcion que con un evento realiza el registro.
   * @param event parametro que indica el evento para realizar o no el registro.
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func edit
   * @description Funcion que con un evento realiza la edicion de una escuela.
   * @param event parametro que indica el evento para realizar o no la edicion.
   * @param id parametro que indica el id de la escuela a modificar.
   */
  edit(event: Event, id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.editar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);
    } else {
      this.form.markAllAsTouched();
    }
  }
  /**
   * @func getEscuelas
   * @description Funcion que obtiene el listado con todos las escuelas registrados en la base de datos.
   */
  getEscuelas() {
    this.escuelaService.getAllEscuelas().subscribe(
      (data: Escuela[]) => {
        this.escuelas = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  states: string[]=[];
  /**
   * @func getFacultades
   * @description Funcion que obtiene el listado con todos las facultades registrados en la base de datos.
   */
  getFacultades() {
    this.procesoService.listaFacultades().subscribe(
      (data: Facultad[]) => {
        this.facultades = data;
        for(let i =  0; i < this.facultades.length; i++){

          this.states[i] = this.facultades[i].Facultad;
        }
        console.log('facultades', this.facultades);
        console.log('states', this.states);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func onValueChanged
   * @description Funcion que maneja el evento de seleccion de facultad en un comboBox.
   * @param event parametro que indica el evento con el cual se selecciono la facultad
   */
  onValueChanged(event: Event) {
    this.escuela.IdFacultad = this.seleccion.IdFacultad;
    this.escuela.Facultad = this.seleccion.Facultad;
    console.log(this.seleccion.IdFacultad);
  }
  /**
   * @func registrar
   * @description Funcion que registra una escuela en la base de datos.
   */
  registrar() {
    this.escuela.Directorio = 'asd';
    console.log('hizo clic en grabar');
    console.log(this.escuela);
    this.escuelaService.save(this.escuela).
      subscribe((data) => {
        console.log(data);
        this.screges = true;
        this.scregoc = false;
        this.getEscuelas();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func ver
   * @description Funcion que permite obtener la data de una escuela.
   * @param id parametro que indica el id de una escuela para obtener su data y visualizarla
   */
  ver(id: number){
    this.procesoService.verEscuela(id).subscribe(
      (data: Escuela) => {
        this.escuela = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func editar
   * @description Funcion que permite editar la data de una escuela.
   * @param id parametro que indica el id de una escuelpara editar su data y actualizarla
   */
  editar(id: number){
    console.log(this.escuela);
    this.escuela.IdFacultad = this.seleccion.IdFacultad;
    this.escuela.Facultad = this.seleccion.Facultad;
    this.procesoService.editarEscuela(id, this.escuela).subscribe(
      (data: Mensajes) => {
       this.mensaje = data.Mensaje;
       this.screges = true;
       this.scregoc = false;
       this.getEscuelas();
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func eliminar
   * @description Funcion que elimina una escuela.
   * @param id parametro que indica el id de una escuela para su eliminacion
   */
  eliminar(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.escuelaService.eliminar(id)
        .subscribe((data) => {
          console.log(data);
          this.getEscuelas();
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @func file
   * @description Funcion que permite el manejo de archivos y almacenarlos temporalmente.
   * @param event parametro que indica el evento con el cual se enviara el archivo
   */
  file(event: any) {
    this.fileToUpload = event.target.files[0];
    if (this.fileToUpload.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileUp = true;
      this.FileValShow = false;
    }
    else {
      this.FileValShow = true;
      this.carga = true;
      this.FileUp = false;
    }
  }
  /**
   * @func getPlantillaEscuelas
   * @description Funcion que permite obtener(descargar) el archivo plantilla de escuelas.
   */
  getPlantillaEscuelas() {
    this.procesoService.descargarPlantillaEscuela().
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = 'Plantilla Escuela';
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @func cargar
   * @description Funcion que permite subir el archivo a la base de datos (anteriormente guardado temporalmente).
   */
  cargar() {
    if(this.FileUp === true){
    this.procesoService.subirEscuela(this.fileToUpload).subscribe(
      data => {
        console.log('Subiendo archivo');
        this.subio = false;
        this.cargando = true;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
      },
      () => {
        this.escuelaService.getAllEscuelas().subscribe(
          (data: Escuela[]) => {
            this.escuelas = data;
            console.log(data);
            this.subio = true;
            this.cargando = false;
            this.exito = true;
          });
      });
    }
    else{
     this.FileValShow = true;
    }
  }
  /**
   * @func reset
   * @description Funcion que vuelve las variables de las alertas en vacias
   */
  Reset() {
    this.exito = false;
  }
  /**
   * @func restablecer
   * @description Funcion que vuelve las variables de los formularios en vacias
   */
  reestablecer() {
    this.screges = false;
    this.scregoc = true;
  }
  /* search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.states
        : this.states.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  } */
  searchTerm: any;
  items: any;
  itemsCopy: any;
  filteredItems: any;
  dato: any;
  search(): void {
    let term = this.searchTerm;
    this.items = this.itemsCopy.filter(function(tag) {
        return tag.name.indexOf(term) >= 0;
    });
}
assignCopy(){
  this.filteredItems = Object.assign([], this.items);
}
filterItem(value){
  if(!value){
      this.assignCopy();
  } // when nothing has typed
  this.filteredItems = Object.assign([], this.items).filter(
     item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
  )
}

}
