import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Data, Router } from '@angular/router';
import { DocsInteres } from 'src/app/interfaces/documentosInteres';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { Rolcitos } from 'src/app/interfaces/roles';
import { Token } from 'src/app/interfaces/token';
import { ProcesosService } from 'src/app/servicios/procesos.service';

@Component({
  selector: 'app-ver-documentos',
  templateUrl: './ver-documentos.component.html',
  styleUrls: ['./ver-documentos.component.css']
})
export class VerDocumentosComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private formBuilder: FormBuilder, private router: Router) {
    this.buildForm();
   }
   mensaje1 = '';
  cargando = false;
  subio = true;
  exito = false;
  carga = false;
  fileToUpload: File = null;
  result = '';
  /* CONDICIONAL */
  scregfac = false;
  scregoc = true;
  FileVal = false;
  FileValShow = false;
  form: FormGroup;
  /* CONDICIONALES */
  scregtesti = true;
  scregtes = false;
  scregtesoc = true;
  noCar = true;
  /* CONDICIONALES */
  rolcitos: Rolcitos ={
    Estudiante: 0,
    Operador: 0,
    Docente: 0,
  }
  nombreArchivo: string;
  mensaje: Mensajes;
  data: Data;
  token: Token = {
    token: null,
  }
  docs: DocsInteres[];
  documento: DocsInteres = {
    idDoc: undefined,
    Documento: undefined,
    Operario: undefined,
    Docente: undefined,
    Estudiante: undefined,
    Archivo: undefined,
  }
  id: number;
  ngOnInit(): void {
    this.getData();
  }

  /**
   * @function buildForm
   * @description Esta función define los parametros de validacion de campos del formulario de regisro de documentos de interes
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      DocName: ['', [Validators.required]],
    });
  }
  /**
   * @function CampoDov
   * @description Esta función extrae los valores de campo DocName en el formulario de regisro de documentos de interes
   */
  get CampoDoc() {
    return this.form.get('DocName');
  }
  /**
   * @function save
   * @description Esta función se encarga de validar los datos en el formulario de registro de documentos de interes
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid && this.FileVal === true) {
      this.registrar();
      this.form.reset();
      const value = this.form.value;
      console.log(value);
      this.FileValShow = false;
    } else {
      this.FileValShow = true;
      this.form.markAllAsTouched();
    }
  }
  /**
   * @function modificando
   * @description Función que modifica los datos del documento de interes
   */
  modificando(event: Event,id: number) {
    event.preventDefault();
    if (this.form.valid) {
      this.modificar(id);
      this.form.reset();
      const value = this.form.value;
      console.log(value);

    } else {

      this.form.markAllAsTouched();
    }
  }
  /**
   * @function handleFileInput
   * @description Función de carga de datos del archivo "Documento de interes"
   */
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    if (this.fileToUpload.type === 'application/pdf') {
      this.result = 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.FileVal = true;
      this.FileValShow = false;
    }
    else {
      this.result = 'Seleccione un archivo valido!';
      this.FileVal = false;
    }
  }
   /**
   * @function registrar
   * @description Función que registra documentos de interes
   */
  registrar() {
    if (this.FileVal === true){
      this.subio = false;
      this.cargando = true;
      if (this.rolcitos.Estudiante === 0 && this.rolcitos.Docente === 0 && this.rolcitos.Operador === 0  ){
        alert('El archivo debe tener MINIMO 1 PERMISO');
      }else{
        this.procesoService.registrarDocs(this.fileToUpload, this.rolcitos, this.nombreArchivo).subscribe((data: Mensajes) => {
          /* alert(data.Mensaje); */
          this.listar(this.id);
          this.subio = true;
          this.cargando = false;
          this.scregtes = true;
          this.scregtesoc = false;
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
        });
      }
  }
  else{
   this.FileValShow = true;
  }
  }
  /**
   * @function listar
   * @description Función que lista los datos del documento seleccionado
   * @param id El id del documento de interes
   */
  listar(id: number){
    this.procesoService.getAllDocs(id).subscribe(
      (data: DocsInteres[]) => {
        this.docs = data;
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function onCheckboxChange
   * @description Función que actualiza los permisos del documento seleccionado
   */
  onCheckboxChange(e) {
    if (e.target.checked) {
      const name = e.target.value;
      this.marcado(name.toString());
      console.log(name);
      console.log(this.rolcitos)
      // this.websiteList.push((e.target.value));
    } else if (e.target.checked === false) {
      const name = e.target.value;
      this.nomarcado(name.toString());
      console.log(this.rolcitos);
    }
  }
  /**
   * @function marcado
   * @description Función que actualiza los datos de rol deacuerdo a la seleccion del usuario
   */
  marcado(uname: string){
    if (uname === 'Estudiante') {
      this.rolcitos.Estudiante = 1;
    }
    if (uname === 'Operador') {
      this.rolcitos.Operador = 1;
    }
    if (uname === 'Docente') {
      this.rolcitos.Docente = 1;
    }
  }
  /**
   * @function nomarcado
   * @description Función que actualiza los datos de rol deacuerdo a la seleccion del usuario
   */
  nomarcado(uname: string){
    if (uname === 'Estudiante') {
      this.rolcitos.Estudiante = 0;
    }
    if (uname === 'Operador') {
      this.rolcitos.Operador = 0;
    }
    if (uname === 'Docente') {
      this.rolcitos.Docente = 0;
    }
  }
  // tslint:disable-next-line: member-ordering
  /**
   * @function getData
   * @description Función que retorna los datos de usuario al iniciar sesión
   */
  getData() {
    this.token.token = localStorage.getItem('token');
    this.procesoService.data(this.token).subscribe(
      (data: Data) => {
        this.data = data;
        this.id = this.data[0].id;
        console.log('data', this.data);
        console.log('dataNombre logueado', this.id);
        this.listar(this.id);
      }
    );
  }
  archivoxd: string
  /**
   * @function descargar
   * @description Función que descarga el documento de interes seleccionado
   * @param id El id del documento a descargar
   * @param nombre El nombre del documento a descargar
   */
  descargar(id: number, nombre: string){
    this.procesoService.descargarDocumentoInteres(id).
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = nombre;
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
      });
  }
  /**
   * @function eliminar
   * @description Función que elimina documentos de interes
   * @param id El id del documento a eliminar
   */
  eliminar(id: number){
    this.procesoService.eliminarDocs(id).
    subscribe((data: Mensajes) => {
      alert(data.Mensaje);
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
    });
  }
  /**
   * @function ver
   * @description Función que lista datos de documentos de interes
   * @param id El id del documento a modificar
   */
  ver(id: number){
    this.scregtes = false;
    this.scregtesti = true;
    this.scregtesoc = true;
    this.cargando = false;
    this.procesoService.verDocs(id).subscribe(
      (data: DocsInteres) => {
        this.documento = data;
        console.log(this.documento);
        console.log(this.documento.Documento);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function modificar
   * @description Función que modifica datos de documentos de interes
   * @param id El id del documento a modificar
   */
  modificar(id: number){
    this.subio = false;
    this.cargando = true;
    this.scregtesoc = false;
    this.procesoService.editarDocs(id, this.fileToUpload, this.rolcitos, this.nombreArchivo).subscribe(
      (data: Mensajes) => {
        /* alert(data.Mensaje); */
        this.mensaje1 = data.Mensaje;
        this.listar(this.id);
        this.subio = true;
          this.cargando = false;
          this.scregtes = true;
          this.scregtesoc = false;
          this.scregtesti = false;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
 /**
   * @function reestrablecerDReg
   * @description Función que reestablece el procedimiento de carga del modal de registro de documentos de interes luego de un registro exitoso o fallido
   */
  reestablecer() {
    this.scregfac = false;
    this.scregoc = true;
  }
  /**
   * @function reestrablecerDReg
   * @description Función que reestablece el procedimiento de carga del modal de modificación de documentos de interes luego de un registro exitoso o fallido
   */
  reestrablecerDReg(){
    this.scregtes = false;
    this.scregtesoc = true;
    this.subio = true;
    this.cargando = false;
  }
}
