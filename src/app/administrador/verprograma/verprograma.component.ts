import { DetallesTesis, JuradoD, TesisD } from './../../interfaces/detallesTesis';
import { Grado } from './../../interfaces/grado';
import { TesisListar } from './../../interfaces/tesislistar';
import { EnvioCorr } from './../../interfaces/envioCorr';
import { EnvioObs } from './../../interfaces/envioObs';
import { TesisEstEsc } from './../../interfaces/tesisEstEsc';
import { EstEscuela } from './../../interfaces/estEscuela';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProcesosService } from 'src/app/servicios/procesos.service';
import { Estudiante } from 'src/app/interfaces/estudiante';
import { Escuela } from 'src/app/interfaces/escuela';
import { TipoTesis } from 'src/app/interfaces/tipotesis';
import { Dictamen } from 'src/app/interfaces/dictamen';
import { EstadoTesis } from 'src/app/interfaces/estadotesis';
import { AssertionResult } from 'protractor/built/plugins';
import { Asesor } from 'src/app/interfaces/asesor';
import { Consulta, FinObs, Observacion } from 'src/app/interfaces/observacion';
import { CrearObs } from 'src/app/interfaces/crearObs';
import { Time } from '@angular/common';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EMPTY } from 'rxjs';
import { Data } from 'src/app/interfaces/data';
import { Token } from 'src/app/interfaces/token';
import { EditarPrograma, EstEsc } from 'src/app/interfaces/estudianteescuela';
import { EstEscEditar } from 'src/app/interfaces/estescEditar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Mensajes } from 'src/app/interfaces/mensajes';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


interface TipodeObservacion {
  Id: number;
  TipoObs: string;
}
interface Fecha {
  year: number;
  month: number;
  day: number;

}

@Component({
  selector: 'app-verprograma',
  templateUrl: './verprograma.component.html',
  styleUrls: ['./verprograma.component.css']
})



export class VerprogramaComponent implements OnInit {

  constructor(private procesoService: ProcesosService, private router: Router,
    private activatedRoute: ActivatedRoute, config: NgbAccordionConfig,
    private modalService: NgbModal, private formBuilder: FormBuilder,
    private formBuilder1: FormBuilder, private formBuilder2: FormBuilder,
    private formBuilder3: FormBuilder) {
    config.closeOthers = true;
    this.buildForm();
    this.buildForm1();
    this.buildForm2();
    this.buildForm3();
  }
  color: 'primary';
  /* NI MODO */
  ID: number;
  /* NI MODO */
  di: string;
  mo: string;
  an: string;
  ho: string;
  mi: string;
  se: string;
  tmhora: string;
  fechagrad: Date;
  fecha: string;
  time: string;
  fechaGraduacion: Date;
  /*======Formato condicionado========*/
  est = localStorage.getItem('rolesE');
  doc = localStorage.getItem('rolesD');
  ope = localStorage.getItem('rolesO');
  TabOp = false;
  TabNop = false;
  TabDoc = false;
  TapNoA = false;
  ModNoA = false;
  tesisSeccion = false;
  obsSeccion = false;

  cargando = false;
  subio = true;
  exito = false;
  aprobar = false;
  corregir = false;
  consulta = false;
  ObsMos = false;
  TabOpOp = false;
  /* ALERTAS */
  prop = false;
  intento = true;
  scregtes = false;
  scregtesoc = true;
  scregOb = false;
  scregObOc = true;
  scregMOb = false;
  scregMObOc = true;
  scEscEstReg = false;
  scEscEstRegOc = true;
  scEdRegEs = false;
  scEdRegEsOc = true;
  scModTe = false;
  scModTeOc = true;
  scModTeTiOc = true;
  scregCon = false;
  scregConOc = true;
  FileVal = false;
  FileVal1 = false;
  FileValShow = false;
  FileValShow1 = false;

   /* AUTOCOMPLETABLE */
   AutShow = false;
   AseAuto = 'Nombre';
   PreAuto = 'Nombre';
   SecAuto = 'Nombre';
   AssAuto = 'Nombre';
   SupAuto = 'Nombre';
   /* AUTOCOMPLETABLE */

  ObsDoc = true;
  ObsJur = false;
  esop = false;
  /* ALERTAS */

  /* TesMos = false; */
  /* FECHA */
  fechactual: String;
  dia: string;
  mes: string;
  año: string;
  horaGraduacion: Date;
  anlist: Number[];
  TablaFake: String[] = ['fake1'];

  /* FECH */
  /*Alerta*/
  mensajeAlertaRegEscuela: string;
  mensajeObservacion: string;
  /*==================================*/
  escuelas: Escuela[];
  tipotesis: TipoTesis[];
  dictamenes: Dictamen[];
  estudiantes: Estudiante[];
  estadoTesis: EstadoTesis[];
  asesores: Asesor[];
  grados: Grado[];
  seleccionGrado: Grado = {
    IdGrado: null,
    IdEscuela: null,
    Grado: null,
    Escuela: null,
  };
  seleccionPromocion: number = null;
  seleccionTipoTesis: TipoTesis;
  tipoTesisE: TipoTesis;
  seleccion: Escuela = {
    idEscuela: null,
    IdFacultad: null,
    Escuela: null,
    Facultad: null,
    Directorio: null,
  };
  escuela: Escuela;
  seleccionDictamenes: Dictamen;
  seleccionEstadoTesis: EstadoTesis;
  data: Data;
  seleccionAsesor: Asesor = {
    id: null,
    Nombre: null,
  };
  seleccionSecretario: Asesor = {
    id: null,
    Nombre: null,
  };

  seleccionSuplente: Asesor = {
    id: null,
    Nombre: null,
  };
  seleccionPresidente: Asesor = {
    id: null,
    Nombre: null,
  };
  estudianteEsc: EstEsc = {
    IdEscuela: null,
    IdEstudianteEscuela: null,
    IdEstudiante: null,
    IdGrado: null,
    Promocion: null,
  }
  estescEditar: EstEscEditar = {
    IdEscuela: null,
    Escuela: null,
    IdEstudianteEscuela: null,
    IdEstudiante: null,
    IdGrado: null,
    Grado: null,
    Promocion: null,
  }
  tesisObservacion: Observacion[];
  tesisObservacion1: Observacion[];

  nombre: string;

  estudiante: Estudiante;
  tesisEstEscuelas: TesisListar[];
  tesisEstEsc: TesisEstEsc = {
    IdEstudianteEscuela: null,
    IdTipoTesis: null,
    IdEstado: null,
    IdDictamen: null,
    IdEstudiante: null,
    TituloTesis: null,
  };
  ///EDITAR PROGRAMA//
  editarProgra: EditarPrograma = {
    IdEscuela: null,
    IdEstudiante: null,
    IdGrado: null,
    Promocion: null,
  }
  ///

  seleccionTipoObs: TipodeObservacion;
  tesisOberva: CrearObs =
    {
      IdObservacion: null,
      IdTesis: null,
      IdJurado: null,
      IdTipoObservacion: null,
      Descripcion: null,
      Fecha: null,
      TipoObservacion: null,
    }
  ////ENviar obersvaciones/////////
  enviandoObs: EnvioObs = {
    IdTesis: null,
    IdJurado: null,
  }
  consultita: Consulta = {
    Consulta: null,
  }

  ////////////////////////////////////
  result: string = '';
  resultT: string = '';
  resultA: string = '';
  resultJ: string = '';
  resultplan: string = '';
  carga: boolean = false;

  token: Token = {
    token: null,
  }
  estudianteEscuela: EstEscuela[];
  observacionTesis: Observacion[];

  obs: TipodeObservacion[] = [
    { Id: 2, TipoObs: 'Especifico' },
    { Id: 1, TipoObs: 'General' }
  ];
  estudianteID: number;
  idPrograma: number;
  id: number;

  fileToUpload: File = null;
  fileToUploadTesis: File = null;
  fileToUploadJurado: File = null;
  fileToUploadAsesor: File = null;
  fileToUploadDictamen: File = null;
  fileToUploadPlanTesis: File = null;
  archivoxd: any;
  idTesis: number;
  idEstEsc: number;
  idTesObs: number;
  idJurado: number;
  //////////PARA CORRECCIONES
  auxListaObsCorregido: number[] = [];
  auxListaObsAprobado: number[] = [];
  auxListaIDObservaciones: number[] = [];
  idObsCorr: EnvioCorr = {
    idsObservaciones: [],
  }

  IdObs: number;

  estado1 = false;
  ////////////////////////
  /* FIN OBSERVACIONES */
  finObs: FinObs = {
    id: null,
    IdTesis: null,
  }
  /*  */
  expandSet = new Set<number>();

  ///////////////DETALLES TESIS////////

  vacio: JuradoD = {
    IdJurado: null,
    IdTipoJurados: null,
    Nombre: null,
    TipoJurado: null,
    id: null,
  }
  editarTesis: DetallesTesis = {
    ArchivoBase64_Dictamen: null,
    ArchivoBase64_ResolucionAsesor: null,
    ArchivoBase64_ResolucionJurado: null,
    ArchivoBase64_PlanTesis: null,
    ArchivoBase64_Tesis: null,
    Jurados: null,
    Tesis: null,
  }
  idsJurados: number[] = [null, null, null];
  detallesTesis: DetallesTesis;
  form: FormGroup;
  form1: FormGroup;
  form2: FormGroup;
  form3: FormGroup;

  nombreUserLogin: string;
/**
 * variables para user logueado -> this.nombreUserLogin: string;
 * para acceder listado de Observaciones -> this.tesisObservacion
 * para acceder a nombre del asesor -> this.tesisObservacion[i].Nombre
 */
  /**
   * @function buildForm3
   * @description Esta función define los parametros de validacion de campos del formulario de registro de tesis
   */
  private buildForm() {
    this.form = this.formBuilder.group({
      titName: ['', [Validators.required,
      Validators.minLength(5),
      Validators.maxLength(250)]],
      // tslint:disable-next-line: max-line-length
      tipName: ['', [Validators.required]],
      palName: ['', [Validators.maxLength(300),
      Validators.minLength(2)/* ,
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ0-9- \"\#\$\%\&\?\¿\*\+\/\'\=\º\(\)\,\.\:\;\’\°\ ]+$/) */]],
      textAName: ['', [Validators.maxLength(2000),
      Validators.minLength(25)/* ,
      Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ0-9-\s \"\#\$\%\&\?\¿\*\+\/\'\=\º\(\)\,\.\:\;\’\°\”\“ ]+$/) */]],
    });

  }
  /**
   * @function buildForm3
   * @description Esta función define los parametros de validacion de campos del formulario de regisro de escuelas
   */
  private buildForm1() {
    this.form1 = this.formBuilder1.group({
      EscName: ['', [Validators.required]],
      graName: ['', [Validators.required]],
      proName: ['', [Validators.required]]
    });
  }
  /**
   * @function buildForm3
   * @description Esta función define los parametros de validacion de campos del formulario de regisro de escuelas
   */
  private buildForm2() {
    this.form2 = this.formBuilder2.group({
      tipObName: ['', [Validators.required]],
      desObName: ['', [Validators.required,
      Validators.minLength(5),
      Validators.maxLength(499),
      // tslint:disable-next-line: max-line-length
      /* Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ0-9- \"\#\$\%\&\?\¿\*\+\/\'\=\º\(\)\,\.\:\;\’\°\”\“  ]+$/) */]],
    });
  }
   /**
   * @function buildForm3
   * @description Esta función define los parametros de validacion de campos del formulario de registro de consultas
   */
  private buildForm3() {
    this.form3 = this.formBuilder3.group({
      consultaName: ['', [Validators.required,
      Validators.minLength(5),
      Validators.maxLength(300),
      // tslint:disable-next-line: max-line-length
      /* Validators.pattern(/^[a-zA-ZäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙÑñ0-9- \"\#\$\%\&\?\¿\*\+\/\'\=\º\(\)\,\.\:\;\’\°\”\“  ]+$/) */]],
    });
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo consultaName en el formulario de registro de consultas
   */
  get CampoConsu() {
    return this.form3.get('consultaName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo tipObName en el formulario de modificación de observaciones
   */
  get CampoOb() {
    return this.form2.get('tipObName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo desObName en el formulario de modificación de observaciones
   */
  get CampoDes() {
    return this.form2.get('desObName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo palName en el formulario de registro de tesis
   */
  get CampoPal() {
    return this.form.get('palName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo textAName en el formulario de registro de tesis
   */
  get CampoText() {
    return this.form.get('textAName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo EscName en el formulario de regisro de escuelas
   */
  get CampoEsc() {
    return this.form1.get('EscName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo proName en el formulario de regisro de escuelas
   */
  get CampoPro() {
    return this.form1.get('proName');
  }
  /**
   * @function CampoGra
   * @description Esta función extrae los valores de campo graName en el formulario de regisro de escuelas
   */
  get CampoGra() {
    return this.form1.get('graName');
  }
  /**
   * @function saveConsulta
   * @description Esta función se encarga de validar los datos en el formulario de registro de consultas
   */
  saveConsulta(event: Event) {
    event.preventDefault();
    if (this.form3.valid) {
      this.consultas();
      this.form3.reset();
    } else {
      this.form3.markAllAsTouched();
    }
  }
  /**
   * @function saveMOb
   * @description Esta función se encarga de validar los datos en el formulario de modificación de observaciones
   */
  saveMOb(event: Event, variable) {
    event.preventDefault();
    if (this.form2.valid) {
      /* this.regitrarTesisObservacion(); */
      this.editarObs(variable);
      this.form2.reset();
    } else {
      this.form2.markAllAsTouched();
    }
  }
  /**
   * @function saveOb
   * @description Esta función se encarga de validar los datos en el formulario de regisro de observaciones
   */
  saveOb(event: Event) {
    event.preventDefault();
    if (this.form2.valid) {
      this.regitrarTesisObservacion();
      this.form2.reset();
    } else {
      this.form2.markAllAsTouched();
    }
  }
  /**
   * @function saveEsc
   * @description Esta función se encarga de validar los datos en el formulario de regisro de escuelas
   */
  saveEsc(event: Event) {
    console.log("SAVEESC Entro")
    event.preventDefault();
    /* if (this.form1.valid) { */
    this.registrarEstEsc();
    console.log("Esvalido")
    this.form1.reset();
    const value = this.form1.value;
    console.log(value);
    /* } else {
      this.form1.markAllAsTouched();
      console.log("NOvalido")
    } */
  }
/**
   * @function CampoTi
   * @description Esta función extrae los valores de campo titName en el formulario de registro de tesis
   */
  get CampoTi() {
    return this.form.get('titName');
  }
  /**
   * @function CampoTip
   * @description Esta función extrae los valores de campo tipName en el formulario de registro de tesis
   */
  get CampoTip() {
    return this.form.get('tipName');
  }
  /**
   * @function CampoAs
   * @description Esta función extrae los valores de campo asName en el formulario de registro de tesis
   */
  get CampoAs() {
    return this.form.get('asName');
  }
  /**
   * @function  save
   * @description Esta función se encarga de validar los datos en el formulario de regisro de tesis
   */
  save(event: Event) {
    event.preventDefault();
    if (this.form.valid && this.FileVal === true && this.FileVal1 === true) {
      this.regitrarTesisEstudiante();
      this.form.reset();
      const value = this.form.value;
      this.FileValShow = false;
      this.FileValShow1 = false;
      this.AutShow = false;
    } else {
      if (this.FileVal === false){
        this.FileValShow = true;
      }
      if (this.FileVal1 === false){
        this.FileValShow1 = true;
      }
      if(this.seleccionAsesor.Nombre === null){
        this.AutShow = true;
      }
      this.form.markAllAsTouched();

    }
  }
  /**
   * @function rolEst
   * @description Esta función se encarga identificar si el usuario que inicia sesión es un estudiante
   */
  rolEst() {
    if (this.est === '1') {
      this.TabNop = true;
      this.TapNoA = true;
      this.ModNoA = true;
      this.aprobar = true;
    }
  }
  /**
   * @function rolOpe
   * @description Esta función se encarga identificar si el usuario que inicia sesión es un operador
   */
  rolOpe() {
    if (this.ope === '1') {
      console.log(this.ope)
      this.TabOp = true;
      this.TabDoc = true;
      this.TapNoA = true;
      this.ModNoA = true;
      this.esop = true;
    }
  }
  /**
   * @function rolDoc
   * @description Esta función se encarga identificar si el usuario que inicia sesión es un docente
   */
  rolDoc() {
    if (this.doc === '1') {
      this.TabDoc = true;
      this.TabNop = true;
      this.corregir = true;
    }
  }
  /**
   * @function fileEscuela
   * @description Función que muestra los datos del archivo "Escuela" precargado
   */
  fileEscuela(event: any): void {
    var selectFile = event.target.files[0];
    if (selectFile.type == 'application/pdf') {
      this.result = 'Nombre: ' + selectFile.name;
      this.result += '<br>Tamaño: ' + selectFile.size;
      this.result += '<br>Tipo: ' + selectFile.type;

      this.carga = false;
    }
    else {
      this.result = 'Seleccione un archivo valido!';
      this.carga = true;
    }

  }
  /**
   * @function fileTesis
   * @description Función que muestra los datos del archivo "Tesis" precargado
   */
  fileTesis(event: any): void {
    var selectFile = event.target.files[0];
    if (selectFile.type == 'application/pdf') {
      this.result = 'Nombre: ' + selectFile.name;
      this.result += '<br>Tamaño: ' + selectFile.size;
      this.result += '<br>Tipo: ' + selectFile.type;

      this.carga = false;
    }
    else {
      this.result = 'Seleccione un archivo valido!';
      this.carga = true;
    }

  }
  /**
   * @function getEscuelas
   * @description Función que devuelve el listado de escuelas registradas en la base de datos
   */
  getEscuelas() {
    this.procesoService.getAllEscuelas().subscribe(
      (data: Escuela[]) => {
        this.escuelas = data;
      });
  }
  /**
   * @function onValueChanged
   * @description Función que devuelve la seleccion de grado para su actualización
   */
  onValueChanged(event: Event) {
    //this.seleccion= target.value;

    // this.idFacultad(this.seleccion);
    console.log(this.seleccion);
    this.escuela = this.seleccion;
    console.log(this.escuela.idEscuela);
    this.getGradoxEscuela(this.seleccion.idEscuela);
  }
  /**
   * @function registrarEstEsc
   * @description Función que registra un estudiante en una determinada escuela deacuerdo a la seleccion
   */
  registrarEstEsc() {
    this.estudianteEsc.IdEscuela = this.seleccion.idEscuela;
    this.estudianteEsc.IdEstudiante = this.estudianteID;
    console.log('idescuelapa grado', this.escuela.idEscuela);
    this.estudianteEsc.IdGrado = this.seleccionGrado.IdGrado;
    this.estudianteEsc.Promocion = this.seleccionPromocion;
    console.log('q paso', this.estudianteEsc);
    this.procesoService.estescCrear(this.estudianteEsc).
      subscribe((data: Mensajes) => {
        this.mensajeAlertaRegEscuela = data.Mensaje;
        console.log(data);
        this.scEscEstReg = true;
        this.scEscEstRegOc = false;
        this.getEstEsc(this.estudianteID);
      }, (error) => {
        console.log(error);
        alert('Ocurrio un error');
      });
  }
  ngOnInit(): void {
    this.estudianteID = this.activatedRoute.snapshot.params['id'];
    console.log('el idestudiante en ngoninit', this.estudianteID);
    this.rolEst();
    this.rolOpe();
    this.rolDoc();
    this.getEstEsc(this.estudianteID);
    this.getNombrecito(this.estudianteID);
    this.getEscuelas();
    this.getTipoTesis();
    this.getDictamen();
    this.getEstadoTesisLista();
    this.getDocente();
    this.getData();
    this.getTimeActual();
    console.log('fecha' + this.fechactual);
    this.promocionListado();
    this.getEstudiantes();
    this.AsJuCambiar();
    /* this. comprobar(); */
  }
  /**
   * @function promocionListado
   * @description Función que el listado de promociones menores a la fecha actual
   */
  promocionListado() {

    /* this.anlist  */
    this.calculo();
    const division = this.tmhora.split(' ', 2);
    const fecha = division[0];
    const divisionf = fecha.split('/', 3);
    const an = divisionf[2];
    console.log('añooo' + an);
    let diferencia: Number;
    diferencia = Number(an) - 1900;
    console.log('dife' + diferencia)
    this.anlist = [diferencia];
    /* this.anlist */
    let i;
    for (i = 0; i < diferencia; i++) {
      this.anlist[i] = Number(an) - i;
      /* console.log('numero:' + this.anlist[i]); */
    }

  }
  /**
   * @function getTimeActual
   * @description Función que retorna el tiempo deacuerdo al servidor actual, este es procesado y convertido a un formato compatible con el definido en la base de datos
   */
  getTimeActual() {
    this.calculo();
    const division = this.tmhora.split(' ', 2);
    const fecha = division[0];
    const divisionf = fecha.split('/', 3);
    /* console.log(divisionh); */
    let di = divisionf[0];
    let me = divisionf[1];
    const an = divisionf[2];

    if (Number(me) < 10) {
      me = '0' + me;
    }
    if (Number(di) < 10) {
      di = '0' + di;
    }
    this.fechactual = an + '-' + me + '-' + di;
  }
  /**
   * @function getData
   * @description Función que retorna los datos de usuario al iniciar sesión
   */
  getData() {
    this.token.token = localStorage.getItem('token');
    this.procesoService.data(this.token).subscribe(
      (data: Data) => {
        this.data = data;
        this.finObs.id = this.data[0].id;
        console.log('data', this.data);
        this.nombreUserLogin = data[0].Nombre;
        console.log('dataNombre logueado', this.nombreUserLogin);
      }
    );
  }
  /**
   * @function getEstEsc
   * @description Función que lista las escuelas registradas segun el programa del usuario seleccionado
   * @param id El id del programa
   */
  getEstEsc(id: number) {
    this.idPrograma = id;
    this.procesoService.estescLista(id).subscribe(
      (data: EstEscuela[]) => {
        this.estudianteEscuela = data;
        console.log('escuelas', this.estudianteEscuela);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
      });
  }
  idestescEditar: number;
  /**
   * @function verPrograma
   * @description Función que lista los programas registrados segun el usuario seleccionado
   * @param id El id del usuario
   */
  verPrograma(id: number) {
    /*
        this.scEdRegEs = false;
        this.scEdRegEsOc = true; */
    this.idestescEditar = id;
    this.procesoService.estescVer(id)
      .subscribe((data: EstEscEditar) => {
        this.estescEditar = data;
        console.log('verprograma', this.estescEditar)
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function onValueChangedEscuela
   * @description Función que actualiza la Escuela segun la escuela deacuerdo a la seleccion de datos en HTML
   */
  onValueChangedEscuela(event: Event) {
    this.estescEditar.Escuela = this.seleccion.Escuela;
    this.estescEditar.IdEscuela = this.seleccion.idEscuela;
    /* console.log(this.seleccion);
    this.escuela = this.seleccion;
    console.log(this.escuela.idEscuela); */
    this.getGradoxEscuela(this.seleccion.idEscuela);
  }
   /**
   * @function onValueChangedGrado
   * @description Función que actualiza el Grado segun la escuela deacuerdo a la seleccion de datos en HTML
   */
  onValueChangedGrado(event: Event) {
    this.estescEditar.Grado = this.seleccionGrado.Grado;
    this.estescEditar.IdGrado = this.seleccionGrado.IdGrado;
  }
   /**
   * @function onValueChangedPromocion
   * @description Función que actualiza la promoción segun la escuela deacuerdo a la seleccion de datos en HTML
   */
  onValueChangedPromocion(event: Event) {
    this.estescEditar.Promocion = this.seleccionPromocion.toString();
  }
  /**
   * @function editarPrograma
   * @description Función que modifica los datos del programa(escuela) seleccionado
   */
  editarPrograma() {
    this.estescEditar.IdEstudiante = this.estudianteID;
    console.log(this.editarProgra);
    this.procesoService.estescEditar(this.idestescEditar, this.estescEditar)
      .subscribe((data) => {
        console.log(data);
        this.scEdRegEs = true;
        this.scEdRegEsOc = false;
        this.getEstEsc(this.idPrograma);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function eliminarPrograma
   * @description Función que elimina el programa(escuela) seleccionado
   * @param id El id del programa registrado
   */
  eliminarPrograma(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.estescEliminar(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.getEstEsc(this.idPrograma);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @function getTipoTesis
   * @description Función que muestra los Tipos de Tesis registrados en la base de datos
   */
  getTipoTesis() {
    this.procesoService.tesisLista().subscribe(
      (data: TipoTesis[]) => {
        this.tipotesis = data;
        console.log(this.tipotesis);
      }
    );
  }
  /**
   * @function getDictamen
   * @description Función que muestra los Dictamenes registrados en la base de datos
   */
  getDictamen() {
    this.procesoService.listaDictamenes().subscribe(
      (data: Dictamen[]) => {
        this.dictamenes = data;
        console.log(this.dictamenes);
      }
    );
  }
   /**
   * @function getEstadoTesisLista
   * @description Función que muestra los estados de tesis registrados en la base de datos
   */
  getEstadoTesisLista() {
    this.procesoService.tesisEstadoLista().subscribe(
      (data: EstadoTesis[]) => {
        this.estadoTesis = data;
        console.log(this.estadoTesis);
      }
    );
  }
  /**
   * @function getDocente
   * @description Función que muestra los docentes registrados en la base de datos
   */
  getDocente() {
    this.procesoService.docenteFiltro().subscribe(
      (data: Asesor[]) => {
        this.asesores = data;
        console.log(this.asesores);
      }
      /* (data: Estudiante) => {
       this.estudiante = data ;
       console.log(this.estudiante.Nombre);
     } */
    );
  }
  /**
   * @function getGradoxEscuela
   * @description Función que muestra los grados registrados deacuerdo a la escuela
   * @param id El id del grado segun la escuela
   */
  getGradoxEscuela(id: number) {
    this.procesoService.listarGradoxEscuela(id).subscribe(
      (data: Grado[]) => {
        this.grados = data;
        console.log(this.grados);
      }
      /* (data: Estudiante) => {
       this.estudiante = data ;
       console.log(this.estudiante.Nombre);
     } */
    );
  }
  /**
   * @function getEstudiantes
   * @description Función que muestra los estudiantes registrados en la base de datos
   */
  getEstudiantes() {

    this.procesoService.getAllEstudiantes().subscribe(
      (data: Estudiante[]) => {
        this.estudiantes = data;
        console.log('asdadasd', this.estudiantes[0]);

        // tslint:disable-next-line: prefer-for-of
        for (let index = 0; index < this.estudiantes.length; index++) {
          /* ºººººººººººººººººººº */
          if (this.estudiantes[index].id == this.ID) {
            // tslint:disable-next-line: max-line-length
            if (this.estudiantes[index].Operador == 1 && this.estudiantes[index].Docente == 0 && this.estudiantes[index].Estudiante == 0) {
              this.TabOpOp = false;
            }
            else {
              this.TabOpOp = true;
            }
          }
        }
        /* IMPRESION */
      }
    );
  }
  /**
   * @function getNombrecito
   * @description Función que muestra el nombre del usuario que inicia sesión
   * @param id El id del usuario registrado
   */
  getNombrecito(id: number) {
    /* console.log("Es estudiante?: ", this.estudiantes[id]); */
    this.ID = id;
    this.procesoService.getNombre(id).subscribe(

      (data: any) => {
        const datos = data;
        console.log(datos)
        this.nombre = datos.Nombre;
        this.id = datos.id;
        console.log('servicio de getalumno por id', datos.Nombre)
        console.log(datos.id);
      }
      /* (data: Estudiante) => {
        this.estudiante = data ;
        console.log(this.estudiante.Nombre);
      } */
    );
    console.log('el id es:', id);
  }
   /**
   * @function handleFileInput
   * @description Función de carga de datos del archivo "Tesis"
   */
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log("ENTRO")
    this.FileVal = true;
    if (this.fileToUpload.type == 'application/pdf') {
      this.result += 'Nombre: ' + this.fileToUpload.name;
      this.result += '<br>Tamaño: ' + this.fileToUpload.size;
      this.result += '<br>Tipo: ' + this.fileToUpload.type;
      this.carga = false;
      this.FileValShow = false;
      console.log(this.FileVal)
    }
    else {
      this.result = 'Seleccione un archivo valido!';
      this.carga = true;
    }
  }
  /**
   * @function handleFileInputTesis
   * @description Función de carga de datos del archivo "Tesis"
   */
  handleFileInputTesis(files: FileList) {
    this.fileToUploadTesis = files.item(0);

    if (this.fileToUploadTesis.type == 'application/pdf') {
      this.resultT = 'Nombre: ' + this.fileToUploadTesis.name;
      this.resultT += '<br>Tamaño: ' + this.fileToUploadTesis.size;
      this.resultT += '<br>Tipo: ' + this.fileToUploadTesis.type;
      this.carga = false;
    }
    else {
      this.resultT = 'Seleccione un archivo valido!';
      this.carga = true;
    }
  }
  /**
   * @function handleFileInputAsesor
   * @description Función de carga de datos del archivo "Asesor"
   */
  handleFileInputAsesor(files: FileList) {

    this.fileToUploadAsesor = files.item(0);

    if (this.fileToUploadAsesor.type == 'application/pdf') {
      this.resultA = 'Nombre: ' + this.fileToUploadAsesor.name;
      this.resultA += '<br>Tamaño: ' + this.fileToUploadAsesor.size;
      this.resultA += '<br>Tipo: ' + this.fileToUploadAsesor.type;
      this.carga = false;
    }
    else {
      this.resultA = 'Seleccione un archivo valido!';
      this.carga = true;
    }
  }
  /**
   * @function handleFileInputPlanJurado
   * @description Función de carga de datos del archivo "Jurado"
   */
  handleFileInputJurado(files: FileList) {
    this.fileToUploadJurado = files.item(0);

    if (this.fileToUploadJurado.type == 'application/pdf') {
      this.resultJ = 'Nombre: ' + this.fileToUploadJurado.name;
      this.resultJ += '<br>Tamaño: ' + this.fileToUploadJurado.size;
      this.resultJ += '<br>Tipo: ' + this.fileToUploadJurado.type;
      this.carga = false;
    }
    else {
      this.resultJ = 'Seleccione un archivo valido!';
      this.carga = true;
    }
  }
  /**
   * @function handleFileInputPlanDictamen
   * @description Función de carga de datos del archivo "Dictamen"
   */
  handleFileInputDictamen(files: FileList) {
    this.fileToUploadDictamen = files.item(0);
    if (this.fileToUploadDictamen.type == 'application/pdf') {
      this.resultJ = 'Nombre: ' + this.fileToUploadDictamen.name;
      this.resultJ += '<br>Tamaño: ' + this.fileToUploadDictamen.size;
      this.resultJ += '<br>Tipo: ' + this.fileToUploadDictamen.type;
      this.carga = false;
    }
    else {
      this.resultJ = 'Seleccione un archivo valido!';
      this.carga = true;
    }
  }
  /**
   * @function handleFileInputPlanTesis
   * @description Función de carga de datos del archivo "Plan de tesis"
   */
  handleFileInputPlanTesis(files: FileList) {
    this.fileToUploadPlanTesis = files.item(0);
    this.FileVal1 = true;
    if (this.fileToUploadPlanTesis.type == 'application/pdf') {
      this.resultplan = 'Nombre: ' + this.fileToUploadPlanTesis.name;
      this.resultplan += '<br>Tamaño: ' + this.fileToUploadPlanTesis.size;
      this.resultplan += '<br>Tipo: ' + this.fileToUploadPlanTesis.type;
      this.carga = false;
      this.FileValShow1 = false;
    }
    else {
      this.resultplan = 'Seleccione un archivo valido!';
      this.carga = true;
    }
  }
  /**
   * @function cargar
   * @description Función de carga del archivo "Tesis"
   */
  cargar() {
    this.procesoService.postFileTesisMod(this.fileToUpload, this.idTesis).subscribe(data => {
      console.log('Subiendo archivo');
      this.subio = false;
      this.cargando = true;
      this.ModNoA = false;
      /* this.listarTesisEstudianteEscuela(this.idEstEsc); */
    }, (err: HttpErrorResponse) => {
      alert(err.error.message);
      if (err.error.message === 'Por favor, inicia sesion para continuar.') {
        this.router.navigate(['/login']);
      }
    },
      () => {
        this.procesoService.listarTesisEstudianteEscuela(this.idEstEsc).
          subscribe(
            (data: TesisListar[]) => {
              console.log(data);
              this.tesisEstEscuelas = data;
              console.log('interface tesislistar', this.tesisEstEscuelas);
              /* this.TesMos = true; */
              this.subio = true;
              this.cargando = false;
              this.exito = true;
              this.ModNoA = true;
              this.prop = true;

              this.intento = false;
            },
            () => {
              alert('Se subio el archivo con exito');
            }
          );
      }
    );
  }
  /**
   * @function mandarID
   * @description Función que actualiza el valor de id de la tesis registrada y reinicia el modal de carga del proceso de registro
   */
  mandarID(id: number) {
    this.idTesis = id;
    this.exito = false;
    this.prop = false;
    this.intento = true;

  }
  /**
   * @function getthefile
   * @description Función que retorna el archivo pdf del documento "Tesis"
   */
  getthefile() {
    console.log(this.idTesis)
    this.procesoService.descargarPDF(this.idTesis).
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = this.nombre + '_Tesis.pdf'
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function getthefileTesis
   * @description Función que retorna el archivo pdf del documento "Tesis"
   */
  getthefileTesis() {
    console.log(this.idTesis)
    this.archivoxd = this.detallesTesis.ArchivoBase64_Tesis;

    if (this.archivoxd != null) {
      // link para descargar
      const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
      const downloadLink = document.createElement('a');
      const fileName = this.detallesTesis.Tesis.RutaArchivo + '.pdf'

      // HREF para descargar
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
      console.log('se descargo');
    } else {
      alert('no existe el archivo');
    }

  }
  /**
   * @function getthefileAsesor
   * @description Función que retorna el archivo pdf del documento "Asesor"
   */
  getthefileAsesor() {
    console.log(this.idTesis)
    this.procesoService.descargarAsesor(this.idTesis).
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = this.detallesTesis.Tesis.ResolucionAsesor + '.pdf'
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function getthefileDictamen
   * @description Función que retorna el archivo pdf del documento "Dictamen"
   */
  getthefileDictamen() {
    console.log(this.idTesis)
    this.procesoService.descargarDictamen(this.idTesis).
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = this.detallesTesis.Tesis.ActaDictamen + '.pdf'
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function getthefilePlanTesis
   * @description Función que retorna el archivo pdf del documento "Plan de tesis"
   */
  getthefilePlanTesis() {
    console.log(this.idTesis)
    this.procesoService.descargarPlanTesis(this.idTesis).
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = this.nombre + '_PlanTesis.pdf';
        /* const fileName = this.detallesTesis.Tesis.ActaDictamen + '.pdf' */
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function getthefileJurado
   * @description Función que retorna el archivo pdf del documento de designación de jurados
   */
  getthefileJurado() {
    console.log(this.idTesis)
    this.procesoService.descargarJurado(this.idTesis).
      subscribe((data: any) => {
        this.archivoxd = data;
        // link para descargar
        const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
        const downloadLink = document.createElement('a');
        const fileName = this.detallesTesis.Tesis.ResolucionJurado + '.pdf'
        // HREF para descargar
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        console.log('se descargo');
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
    /* console.log(this.idTesis)
    this.archivoxd = this.detallesTesis.ArchivoBase64_ResolucionJurado;

    if (this.archivoxd != null) {
      // link para descargar
      const linkSource = 'data:application/pdf;base64,' + this.archivoxd;
      const downloadLink = document.createElement('a');
      const fileName = this.detallesTesis.Tesis[0].ResolucionJurado + '.pdf'
      // HREF para descargar
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
      console.log('se descargo');
    } else {
      alert('no existe el archivo');
    } */

  }
  mensajeTesisGrado: string;
  /**
   * @function regitrarTesisEstudiante
   * @description Función que registra los datos de la tesis del estudiante
   */
  regitrarTesisEstudiante() {
    console.log("entro")
    this.subio = false;
    this.cargando = true;
    this.tesisEstEsc.IdDictamen = 6;
    this.tesisEstEsc.IdEstado = 2;
    this.tesisEstEsc.IdTipoTesis = this.seleccionTipoTesis.IdTipoTesis;
    this.tesisEstEsc.IdEstudianteEscuela = this.idEstEsc;
    this.tesisEstEsc.IdEstudiante = this.seleccionAsesor.id;
    // this.tesisEstEsc.Tamano = this.fileToUpload.size;
    // this.tesisEstEsc.RutaArchivo ='';
    console.log('nombre de la tesis de formulario', this.tesisEstEsc.TituloTesis);
    console.log('enviando objeto de tesis:', this.tesisEstEsc);
    this.procesoService.crearTesisEstudianteEscuela(this.tesisEstEsc, this.fileToUpload, this.fileToUploadPlanTesis).
      subscribe((data: Mensajes) => {
        this.mensajeTesisGrado = data.Mensaje;
        /* alert('Tesis añadida'); */
        /* this.listarTesisEstudianteEscuela(this.idEstEsc); */
        // this.router.navigate( ['/adlistaescuelas'] );
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      },
        () => {
          this.procesoService.listarTesisEstudianteEscuela(this.idEstEsc).
            subscribe(
              (data: TesisListar[]) => {
                console.log(data);
                this.tesisEstEscuelas = data;
                this.subio = true;
                this.cargando = false;
                this.scregtes = true;
                this.scregtesoc = false;
              });
        });
  }
  // tslint:disable-next-line: member-ordering
  todoslosJurados: JuradoD[] = [];
  /**
   * @function verTesisEstEsc
   * @description Función que muestra los datos del archivo de tesis segun la escuela en la que el estudiante esta registrado
   * @param id El id de la tesis registrada
   */
  verTesisEstEsc(id: number) {
    this.idTesis = id;
    this.procesoService.verTesisEstudianteEscuela(id)
      .subscribe((data: DetallesTesis) => {
        this.detallesTesis = data;
        if (this.detallesTesis.Tesis.Resumen === 'undefined') {
          this.detallesTesis.Tesis.Resumen = '';
        }
        if (this.detallesTesis.Tesis.PalabrasClave === 'undefined') {
          this.detallesTesis.Tesis.PalabrasClave = '';
        }
        console.log('detalles tesis', this.detallesTesis);
        const tam = this.detallesTesis.Jurados.length;
        console.log('nombre de la tesis', this.detallesTesis.Tesis.TituloTesis);
        console.log('estado de la tesis', this.detallesTesis.Tesis.Estado);
        console.log(this.todoslosJurados);
        if (tam > 1) {
          this.seleccionPresidente.Nombre = this.detallesTesis.Jurados[1].Nombre;
          this.seleccionPresidente.id = this.detallesTesis.Jurados[1].id;
          this.seleccionSecretario.Nombre = this.detallesTesis.Jurados[2].Nombre;
          this.seleccionSecretario.id = this.detallesTesis.Jurados[2].id;
          this.seleccionSuplente.Nombre = this.detallesTesis.Jurados[3].Nombre;
          this.seleccionSuplente.id = this.detallesTesis.Jurados[3].id;
          console.log(this.todoslosJurados);
        }
        console.log(data);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function onValueChangedEstadoTesis
   * @description Función de captura el valor seleccionado en HTML y actualiza el EstadoTesis designado al archivo de tesis
   */
  onValueChangedEstadoTesis(event: Event) {
    console.log(this.seleccionTipoTesis);
    this.detallesTesis.Tesis.Estado = this.seleccionEstadoTesis.Estado;
    this.detallesTesis.Tesis.IdEstado = this.seleccionEstadoTesis.IdEstado;
  }
  /**
   * @function onValueChangedTipoTesis
   * @description Función de captura el valor seleccionado en HTML y actualiza el TipoTesis designado al archivo de tesis
   */
  onValueChangedTipoTesis(event: Event) {
    console.log(this.seleccionTipoTesis);
    this.detallesTesis.Tesis.TipoTesis = this.seleccionTipoTesis.TipoTesis;
    this.detallesTesis.Tesis.IdTipoTesis = this.seleccionTipoTesis.IdTipoTesis;
  }
  /**
   * @function onValueChangedAsesor
   * @description Función de captura el valor seleccionado en HTML y actualiza el Asesor designado en la tabla de asignación de jurados para el proceso de sustentación
   */
  onValueChangedAsesor() {
    this.detallesTesis.Jurados[0].Nombre = this.seleccionAsesor.Nombre;
    this.detallesTesis.Jurados[0].id = this.seleccionAsesor.id;
    console.log('id asesor', this.seleccionAsesor.id);
  }
  /**
   * @function onValueChangedPresidente
   * @description Función de captura el valor seleccionado en HTML y actualiza el Presidente designado en la tabla de asignación de jurados para el proceso de sustentación
   */
  onValueChangedPresidente() {
    this.idsJurados[0] = this.seleccionPresidente.id;
    console.log(this.idsJurados);
  }
  /**
   * @function onValueChangedSecretario
   * @description Función de captura el valor seleccionado en HTML y actualiza el Secretario designado en la tabla de asignación de jurados para el proceso de sustentación
   */
  onValueChangedSecretario() {
    this.idsJurados[1] = this.seleccionSecretario.id;
    console.log(this.idsJurados);
  }
  /**
   * @function onValueChangedSuplente
   * @description Función de captura el valor seleccionado en HTML y actualiza el Suplente designado en la tabla de asignación de jurados para el proceso de sustentación
   */
  onValueChangedSuplente() {
    this.idsJurados[2] = this.seleccionSuplente.id;
    console.log(this.idsJurados);
  }
   /**
   * @function onValueChangedDictamen
   * @description Función de captura el valor seleccionado en HTML y actualiza el dictamen del archivo de tesis
   */
  onValueChangedDictamen(event: Event) {
    this.detallesTesis.Tesis.Dictamen = this.seleccionDictamenes.Dictamen;
    this.detallesTesis.Tesis.IdDictamen = this.seleccionDictamenes.IdDictamen;
    console.log('dictamenon', this.detallesTesis.Tesis.Dictamen);
    console.log('dictameidnon', this.detallesTesis.Tesis.IdDictamen);
  }
  idTesisModificar: number;
  /**
   * @function editarTesisEstudiante
   * @description Función que actualiza el id de la tesis de la cual se visualizaran los datos antes de hacer la actualización de estos
   */
  editarTesisEstudiante(id: number) {
    this.idTesisModificar = id;
    this.verTesisEstEsc(id)
  }
  /**
   * @function eliminarEstEscTesis
   * @description Función que modifica los datos de las tesis registradas segun la escuela seleccionada
   */
  botonModificarTesisEstudiante() {
    /* if (this.detallesTesis.ArchivoBase64_ResolucionJurado == null) {
    } */
    this.scModTe =false;
    this.cargando = true;
    this.scModTeOc = false;
    this.scModTeTiOc = true;
    console.log('horita', this.horaGraduacion);
    /*  this.time = ho + ':' + mi + ':' + se;
     this.fecha = an + '-' + me + '-' + di; */
    /* this.detallesTesis.Tesis[0].FechaGraduacion = this.detallesTesis.Tesis[0].FechaGraduacion + ' 09:00:00'; */
    console.log('grad', this.detallesTesis.Tesis.HoraGraduacion);
    this.procesoService.modificarTesisEstudianteEscuela(this.idTesisModificar, this.idEstEsc, this.detallesTesis, this.idsJurados,
      this.fileToUploadTesis, this.fileToUploadJurado, this.fileToUploadAsesor, this.fileToUploadDictamen, this.fileToUploadPlanTesis).subscribe((data) => {
        console.log(data);
        this.listarTesisEstudianteEscuela(this.idEstEsc);
        /* alert('tesis modificada'); */
        this.scModTe = true;
        this.cargando = false;
        this.scModTeOc = false;
        this.scModTeTiOc = false;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }

  /**
   * @function eliminarEstEscTesis
   * @description Función que lista las tesis registradas segun la escuela seleccionada
   * @param id El id de la escuela a la que el estudiante pertenece
   */
  listarTesisEstudianteEscuela(id: number) {
    this.idEstEsc = id;
    this.tesisSeccion = true;
    this.procesoService.listarTesisEstudianteEscuela(id).
      subscribe(
        (data: TesisListar[]) => {
          console.log(data);
          this.tesisEstEscuelas = data;
          console.log('interface tesislistar', this.tesisEstEscuelas);
          /* this.TesMos = true; */
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });

  }
  datosJurados: JuradoD[];
  /**
   * @function eliminarEstEscTesis
   * @description Función que elimina la tesis segun el programa en el que esta se registro
   * @param id El id de la escuela a la que el estudiante pertenece
   */
  eliminarEstEscTesis(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarEstEsc(id)
        .subscribe((data) => {
          alert('Eliminado con exito');
          console.log(data);
          this.listarTesisEstudianteEscuela(this.idEstEsc);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @function listaObservacionTesis
   * @description Función que lista las observaciones deacuerdo a las tesis seleccionada
   * @param id El de la observación segun la tesis
   * @param idjurado El id del jurado segun la escuela
   */
  listaObservacionTesis(id: number, idjurado: number) {
    /* PARA LA TABLA */
    this.ObsMos = true;
    this.obsSeccion = true;
    /* PARA LA TABLA */
    this.idTesObs = id;
    this.idJurado = idjurado;
    this.procesoService.listaObservacion(id).
      subscribe(
        (data: Observacion[]) => {
          this.tesisObservacion = data;
          this.tesisObservacion1 = data;
          const tam = this.tesisObservacion.length;
          for (let i = 0; i < tam; i++) {
            this.auxListaObsAprobado[i] = this.tesisObservacion[i].Aprobado;
            this.auxListaObsCorregido[i] = this.tesisObservacion[i].Correccion;
            this.auxListaIDObservaciones[i] = this.tesisObservacion[i].IdObservacion;
          }
          console.log('array de los aprobados', this.auxListaObsAprobado);
          console.log('array de los corregidos', this.auxListaObsCorregido);
          console.log('las observaciones de una tesis', this.tesisObservacion);
        }
      );

  }
  /**
   * @function eliminarObs
   * @description Función que elimina las observaciones deacuerdo a la tesis registrada
   * @param id El id de la observación a eliminar
   */
  eliminarObs(id: number) {
    if (confirm('Seguro que deseas eliminar')) {
      this.procesoService.eliminarObservacion(id)
        .subscribe((data) => {

          console.log(data);
          this.listaObservacionTesis(this.idTesObs, this.idJurado);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @function regitrarTesisObservacion
   * @description Función que registra observaciones deacuerdo a la tesis registrada
   */
  regitrarTesisObservacion() {
    /* this.tiempo(); */
    this.calculo();
    const now = new Date();
    const division = this.tmhora.split(' ', 2);
    const fecha = division[0];
    const hora = division[1];
    const divisionf = fecha.split('/', 3);
    const divisionh = hora.split(':', 3);
    /* console.log(divisionh); */
    const di = divisionf[0];
    const me = divisionf[1];
    const an = divisionf[2];
    let ho = divisionh[0];
    const mi = divisionh[1];
    const se = divisionh[2];
    var hor = Number(ho);
    if (Number(ho) > 12) {
      ho = (hor - 12).toString();
    }

    this.tesisOberva.IdJurado = this.idJurado;

    this.tesisOberva.IdTesis = this.idTesObs;
    this.tesisOberva.IdTipoObservacion = this.seleccionTipoObs.Id;

    this.time = ho + ':' + mi + ':' + se;
    this.fecha = an + '-' + me + '-' + di;
    this.tesisOberva.Fecha = this.fecha + ' ' + this.time;
    console.log('model2', this.tesisOberva.Fecha);

    console.log('hizo clic en grabar');
    console.log(this.tesisOberva.IdTipoObservacion);
    this.procesoService.crearObservacion(this.tesisOberva).
      subscribe((data: Mensajes) => {
        console.log(data);
        /* alert('Observacion añadida'); */
        this.mensajeObservacion = data.Mensaje;
        this.scregOb = true;
        this.scregObOc = false;
        /*  */
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
        // this.router.navigate( ['/adlistaescuelas'] );
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function formatoFecha
   * @description Función que procesa la fecha actual devolviendo un formato compatible con el definido en la base de datos
   */
  formatoFecha() {
    this.calculo();
    const now = new Date();
    const division = this.tmhora.split(' ', 2);
    const fecha = division[0];
    const hora = division[1];
    const divisionf = fecha.split('/', 3);
    const divisionh = hora.split(':', 3);
    /* console.log(divisionh); */
    const di = divisionf[0];
    const me = divisionf[1];
    const an = divisionf[2];
    let ho = divisionh[0];
    const mi = divisionh[1];
    const se = divisionh[2];
    var hor = Number(ho);
    if (Number(ho) > 12) {
      ho = (hor - 12).toString();
    }
    this.tesisOberva.IdJurado = this.idJurado;
    this.tesisOberva.IdTesis = this.idTesObs;
    this.tesisOberva.IdTipoObservacion = this.seleccionTipoObs.Id;
    this.time = ho + ':' + mi + ':' + se;
    this.fecha = an + '-' + me + '-' + di;
    this.tesisOberva.Fecha = this.fecha + ' ' + this.time;
  }
   /**
   * @function verObs
   * @description Función que lista los datos de las observaciones registradas
   */
  verObs(id: number) {
    this.procesoService.verObservacion(id).subscribe(
      (data: CrearObs) => {
        this.tesisOberva = data;
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function onValueChangedTipoObs
   * @description Función que actualiza los datos del tipo de observaciones y el ID del mismo deacuerdo a la tesis registrada
   */
  onValueChangedTipoObs(event: Event) {
    this.tesisOberva.TipoObservacion = this.seleccionTipoObs.TipoObs;
    this.tesisOberva.IdTipoObservacion = this.seleccionTipoObs.Id;
  }
  /**
   * @function editarObs
   * @description Función que modifica los datos de las observaciones realizadas
   */
  editarObs(id: number) {
    this.formatoFecha();

    this.procesoService.editarObservacion(id, this.tesisOberva)
      .subscribe((data) => {
        this.scregMOb = true;
        this.scregMObOc = false;
        console.log(data);
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
      }, (error) => {
        console.log(error);
      });
  }
  /**
   * @function enviarObservaciones
   * @description Función que actualiza las observaciones hechas en un archivo de tesis
   */
  enviarObservaciones() {
    this.enviandoObs.IdTesis = this.idTesObs;
    this.enviandoObs.IdJurado = this.idJurado;
    if (confirm('Seguro que deseas enviar esta Observacion?')) {
      this.procesoService.envioObservacion(this.enviandoObs)
        .subscribe((data) => {
          alert('Se modifico la observacion');
          console.log(data);
          this.listaObservacionTesis(this.idTesObs, this.idJurado);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
  /**
   * @function modalConsultas
   * @description Función que actualiza al id de la observación deacuerdo a la opción seleccionada
   * @param id El id de la observación
   *
   */
  modalConsultas(id: number) {
    this.IdObs = id;
  }
  /**
   * @function consultas
   * @description Función que registra las consultas realizadas por el estudiante en cada observación listada
   */
  consultas() {
    this.procesoService.agregarConsultas(this.IdObs, this.consultita)
      .subscribe((data) => {
        /* alert('Se registro la consulta'); */
        this.scregCon = true;
        this.scregConOc = false;
        console.log(this.consultita);
        console.log(data);
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
  }
  /**
   * @function enviarCorrecciones
   * @description Función que envia las correcciones realizadas sobre una tesis permitiendo se levante la observación a la que corresponde
   */
  enviarCorrecciones() {
    const tam = this.tesisObservacion.length;
    console.log(tam);
    for (let i = 0; i < tam; i++) {
      if (this.auxListaObsAprobado[i] == 0 && this.auxListaObsCorregido[i] == 1) {
        this.idObsCorr.idsObservaciones.push(this.auxListaIDObservaciones[i]);
      }
    }
    console.log(this.idObsCorr.idsObservaciones);
    if (confirm('Seguro que deseas enviar las correciones?')) {
      this.procesoService.envioCorreccion(this.idObsCorr)
        .subscribe((data) => {
          console.log(data);
          this.listaObservacionTesis(this.idTesObs, this.idJurado);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
      console.log('los ids corr', this.idObsCorr);
    }
  }
  /**
   * @function corrigiendoObservacion
   * @description Función que cambia el estado de una observacion a corregida
   * @param idd El id de la observación aprobada
   */
  corrigiendoObservacion(id: number) {
    this.procesoService.corregirObservacion(id)
      .subscribe((data) => {
        console.log(data);
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
      }, (error) => {
        console.log(error);
      });
  }
  /**
   * @function NocorrigiendoObservacion
   * @description Función que cambia el estado de una observacion a no corregida
   * @param idd El id de la observación.
   */
  NocorrigiendoObservacion(id: number) {
    /* if (confirm('La observacion No se corrigio ?')) { */
    this.procesoService.corregirObservacion(id)
      .subscribe((data) => {
        console.log(data);
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
    /* } else {
      this.listaObservacionTesis(this.idTesObs, this.idJurado);
    } */
  }
  /**
   * @function aprobandoObservacion
   * @description Función que aprueba las observaciones hechas al documento de tesis
   * @param idd El id de la observación aprobada
   */
  aprobandoObservacion(id: number) {
    /* if (confirm('Seguro que desea aprobar esta observacion?')) {
     */  this.procesoService.aprobarObservacion(id)
      .subscribe((data) => {
        alert('Se aprobo la observación con exito');
        console.log(data);
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
    /*  } else {
       this.listaObservacionTesis(this.idTesObs, this.idJurado);
     } */
  }
  /**
   * @function desaprobandoObservacion
   * @description Función que desaprueba las observaciones hechas al documento de tesis
   * @param idd El id de la observación desaprobada
   */
  desaprobandoObservacion(id: number) {
    /* if (confirm('Seguro que desea desaprobar esta observacion?')) {
     */  this.procesoService.aprobarObservacion(id)
      .subscribe((data) => {
        console.log(data);
        this.listaObservacionTesis(this.idTesObs, this.idJurado);
      }, (err: HttpErrorResponse) => {
        alert(err.error.message);
        if (err.error.message === 'Por favor, inicia sesion para continuar.') {
          this.router.navigate(['/login']);
        }
      });
    /*}
   else {
     this.listaObservacionTesis(this.idTesObs, this.idJurado);
   } */
  }
  /**
   * @function onCheckboxChangeCorregido
   * @description Función que envia las correcciones hechas al documento de tesis mediante checks
   * @param e 'e' es el evento que indica si el check esta marcado o desmarcado
   * @param idd El id de la corrección hecha.
   */
  onCheckboxChangeCorregido(e, idd: number) {
    this.IdObs = idd;
    console.log('id corregio obs', this.IdObs);
    if (e.target.checked) {
      this.corrigiendoObservacion(this.IdObs);
    } else if (e.target.checked == false) {
      this.NocorrigiendoObservacion(this.IdObs);
    }
  }
  /**
   * @function onCheckboxChange
   * @description Función que aprueba las observaciones hechas al documento de tesis mediante checks
   * @param e 'e' es el evento que indica si el check esta marcado o desmarcado
   * @param idd El id de la observación hecha.
   */
  onCheckboxChange(e, idd: number) {
    this.IdObs = idd;
    console.log('id aprobando obs', this.IdObs)
    if (e.target.checked) {
      this.aprobandoObservacion(this.IdObs);
      // this.websiteList.push((e.target.value));
    } else if (e.target.checked == false) {
      this.desaprobandoObservacion(this.IdObs);
      /*  const index = website.controls.findIndex(x => x.value === e.target.value);
       website.removeAt(index); */
    }
  }
/**
   * @function finalizarObservacion
   * @description Función que actualiza y envia todas las observaciones hechas por el asesor permitiendo el cambio de estado de tesis.
   */
  finalizarObservacion() {
    if (confirm('Usted termino todas sus observaciones?')) {
      this.finObs.IdTesis = this.idTesObs;
      console.log('finobs', this.finObs);
      this.procesoService.finObservacion(this.finObs)
        .subscribe((data: Mensajes) => {
          alert(data.Mensaje);
          this.listaObservacionTesis(this.idTesObs, this.idJurado);
        }, (err: HttpErrorResponse) => {
          alert(err.error.message);
          if (err.error.message === 'Por favor, inicia sesion para continuar.') {
            this.router.navigate(['/login']);
          }
        });
    }
  }
   /**
   * @function calculo
   * @description Función que actualiza la variable tmhora con la fecha y hora actuales deacuerdo al servidor local.
   */
  calculo() {
    this.tmhora = new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();
  }
  onvaluechangeFecha(event) {
    //this.detallesTesis.Tesis[0].FechaGraduacion =  this.detallesTesis.Tesis[0].FechaGraduacion + ' ' + this.time;
  }
  onvaluechangeHora(event) {
    //this.detallesTesis.Tesis[0].FechaGraduacion =  this.detallesTesis.Tesis[0].FechaGraduacion + ' ' + this.time;
  }
  /* ---------MODAL---------- */
  /**
   * @function openXl
   * @description Función que establece el tamaño de modal en "Xl-Extra largo" segun documentación de NGBOOTSTRAP, y reestablece el procedimiento de actualización del modal de modificación de tesis exitoso o fallido
   */
  openXl(content) {
    this.modalService.open(content, { size: 'xl' });
    this.calculo();
    this.scModTe = false;
    this.scModTeOc = true;
    this.scModTeTiOc = true;
  }
  /**
   * @function openLg
   * @description Función que establece el tamaño de modal en "lg-Largo" segun documentación de NGBOOTSTRAP
   */
  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }
  /**
   * @function openMd
   * @description Función que establece el tamaño de modal en "md-Medio" segun documentación de NGBOOTSTRAP
   */
  openMd(content) {
    this.modalService.open(content, { size: 'md' });
  }
  /* ---------MODAL---------- */

/**
   * @function reestablecerConsulta
   * @description Función que reestablece el procedimiento de carga del modal de registro de tesis luego de un registro exitoso o fallido
   */
  reestrablecerTReg() {
    this.scregtes = false;
    this.scregtesoc = true;
    this.subio = true;
    this.cargando = false;
  }
  /**
   * @function reestablecerConsulta
   * @description Función que reestablece el procedimiento de carga del modal de registro de observación luego de un registro exitoso o fallido
   */
  reestablecerObs() {
    this.scregOb = false;
    this.scregObOc = true;
  }
  /**
   * @function reestablecerConsulta
   * @description Función que reestablece el procedimiento de carga del modal de registro de programa luego de un registro exitoso o fallido
   */
  reestablecerEsc() {
    this.scEscEstReg = false;
    this.scEscEstRegOc = true;
  }
  /**
   * @function reestablecerConsulta
   * @description Función que reestablece el procedimiento de carga del modal de editar programa luego de un registro exitoso o fallido
   */
  reestablecerEditarEsc() {
    this.scEdRegEs = false;
    this.scEdRegEsOc = true;
  }
  /**
   * @function reestablecereditObs
   * @description Función que reestablece el procedimiento de carga del modal de editar observación luego de un registro exitoso o fallido
   */
  reestablecereditObs() {
    this.scregMOb = false;
    this.scregMObOc = true;
  }
/**
   * @function AsJuCambiar
   * @description Función que consulta si el usuario que inicia sesión es jurado
   */
  AsJuCambiar() {
    if (this.id === this.idJurado) {
      console.log('Son iguales: ' + Number(this.id) + ' jurado: ' + Number(this.idJurado));
    }
  }
/**
   * @function reestablecerConsulta
   * @description Función que reestablece el procedimiento de carga del modal de consultas luego de un registro exitoso o fallido
   */
  reestablecerConsulta() {
    this.scregCon = false;
    this.scregConOc = true;
  }

  /* HORA */
/**
   * @function mostrar
   * @description Función que muestra el valor de la variable timeformat
   */
  timeformat: String;
  mostrar() {
    console.log(this.timeformat);
  }

  /* MIS OBSERVACIONES */
  /**
   * @function onChange
   * @description Función que filtra las observaciones en la tabla de observaciones de tesis deacuerdo al usuario que ingresa al sistema
   * @param isChecked El isChecked indica si el checkbox que dispara esta función fue marcado o no (true o false).
   */
  onChange(isChecked: boolean) {
    if (isChecked) {

      console.log("entro")
      this.estado1 = true;
      this.tesisObservacion = Object.values(this.tesisObservacion).filter((i) => (i.Nombre === this.nombreUserLogin));
      console.log(this.tesisObservacion);
    } else {
      this.estado1 = false;
      this.tesisObservacion = this.tesisObservacion1;
      console.log(this.tesisObservacion);
    }

  }
  /* MIS OBSERVACIONES */
  /**
   * @function selectEvent
   * @description Función que procesa la variable item y actualiza el valor de la variable seleccionAsesor.id y seleccionAsesor.Nombre
   * @param item El item es el objeto seleccionado en el autocompletable del campo Asesor en el formulario de registro de tesis.
   */
  selectEvent(item) {
    let variable: String;
    variable = (Object.values(item)).toString();

    console.log('VARIABLE: ' + variable);
    const division = variable.split(',', 2);
    const d1 = division[0];
    console.log('d1' + d1);
    const d2 = division[1];
    console.log('d2' + d2);
    this.seleccionAsesor.id = Number(d1);
    this.seleccionAsesor.Nombre = d2;
    this.AutShow = false;
  }
  /**
   * @function selectPreTab
   * @description Función que procesa la variable item y actualiza el valor de la variable seleccionPresidente.id y seleccionPresidente.Nombre
   * @param item El item es el objeto seleccionado en el autocompletable de la tabla de selección de jurados campo Presidente
   */
  selectPreTab(item) {
    let variable: String;
    variable = (Object.values(item)).toString();

    console.log('VARIABLE: ' + variable);
    const division = variable.split(',', 2);
    const d1 = division[0];
    console.log('d1' + d1);
    const d2 = division[1];
    console.log('d2' + d2);
    this.seleccionPresidente.id = Number(d1);
    this.seleccionPresidente.Nombre = d2;
    this.AutShow = false;
    this.onValueChangedPresidente();
  }
  /**
   * @function selectSecTab
   * @description Función que procesa la variable item y actualiza el valor de la variable seleccionSecretario.id y seleccionSecretario.Nombre
   * @param item El item es el objeto seleccionado en el autocompletable de la tabla de selección de jurados campo Secretario
   */
  selectSecTab(item) {
    let variable: String;
    variable = (Object.values(item)).toString();

    console.log('VARIABLE: ' + variable);
    const division1 = variable.split(',', 2);
    const d11 = division1[0];
    console.log('d1' + d11);
    const d21 = division1[1];
    console.log('d2' + d21);
    this.seleccionSecretario.id = Number(d11);
    this.seleccionSecretario.Nombre = d21;
    this.AutShow = false;
    this.onValueChangedSecretario();
  }
  /**
   * @function selectAsTab
   * @description Función que procesa la variable item y actualiza el valor de la variable seleccionAsesor.id y seleccionAsesor.Nombre
   * @param item El item es el objeto seleccionado en el autocompletable de la tabla de selección de jurados campo Asesor
   */
  selectAsTab(item) {
    let variable: String;
    variable = (Object.values(item)).toString();

    console.log('VARIABLE: ' + variable);
    const division1 = variable.split(',', 2);
    const d11 = division1[0];
    console.log('d1' + d11);
    const d21 = division1[1];
    console.log('d2' + d21);
    this.seleccionAsesor.id = Number(d11);
    this.seleccionAsesor.Nombre = d21;
    this.AutShow = false;
    this.onValueChangedAsesor();
  }
  /**
   * @function selectSupTab
   * @description Función que procesa la variable item y actualiza el valor de la variable seleccionSuplente.id y seleccionSuplente.Nombre
   * @param item El item es el objeto seleccionado en el autocompletable de la tabla de selección de jurados campo suplente
   */
  selectSupTab(item) {
    let variable: String;
    variable = (Object.values(item)).toString();

    console.log('VARIABLE: ' + variable);
    const division1 = variable.split(',', 2);
    const d11 = division1[0];
    console.log('d1' + d11);
    const d21 = division1[1];
    console.log('d2' + d21);
    this.seleccionSuplente.id = Number(d11);
    this.seleccionSuplente.Nombre = d21;
    this.AutShow = false;
    this.onValueChangedSuplente();
  }
}
