import { ProcesosService } from 'src/app/servicios/procesos.service';
import { UserserviceService } from './../../servicios/userservice.service';
import { Estudiante } from './../../interfaces/estudiante';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TipoDNI } from 'src/app/interfaces/tipodoc';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

estudiante: Estudiante = {
  id: null,
  Nombre: null,
  idDNITipo: null,
  DNI: null,
  Correo: null,
  Operador: 0,
  Estudiante: 1,
  Docente: 0,
  Direccion: null,
  Telefono: null,
}
  seleccion: TipoDNI;
  tipodocumentos: TipoDNI[];
  tipodoc: TipoDNI;
  // tslint:disable-next-line: max-line-length
  constructor(  private procesoService: ProcesosService,
               private userService: UserserviceService, private activatedRoute: ActivatedRoute , private router: Router, private formBuilder: FormBuilder) {
    this.buildForm();
    }
    form: FormGroup;
ngOnInit(): void {
  //this.getTipoDocumentos();

}
  // FORMULARIO REACTIVO

  private buildForm(){
    this.form = this.formBuilder.group({
      UserName : ['', [Validators.required, Validators.minLength(10), Validators.pattern(/^[a-zA-Z ]+$/)]],
      UserCorreo : ['', [Validators.required, Validators.email]],
      UserDNI : ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{7}/)]],
      UserCel : ['', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]{8}/)]],
      UserDir : ['', [Validators.required, Validators.pattern(/^[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?(( |\-)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\.?)* (((#|[nN][oO]\.?) ?)?\d{1,4}(( ?[a-zA-Z0-9\-]+)+)?)/)]],
      FacSec : [ ]
});

  }

  save(event: Event){
    event.preventDefault();
    if (this.form.valid){
      this.registrar();
      const value = this.form.value;
      console.log(value);
    } else{
      this.form.markAllAsTouched();
    }
  }

  get CampoUs(){
    return this.form.get('UserName');
  }
  get CampoCor(){
    return this.form.get('UserCorreo');
  }
  get CampoDNI(){
    return this.form.get('UserDNI');
  }
  get CampoCel(){
    return this.form.get('UserCel');
  }
  get CampoDir(){
    return this.form.get('UserDir');
  }
  /* getTipoDocumentos(){
    this.procesoService.listaTipoDoc().subscribe(
      (data: TipoDNI[]) => {
        this.tipodocumentos = data;
      });
    } */
    onValueChanged(event: Event){
      //this.seleccion= target.value;

     // this.idFacultad(this.seleccion);
      console.log(this.seleccion);
      this.tipodoc = this.seleccion;
      console.log(this.tipodoc.IdDNITipo);
    }
  registrar(){
   this.estudiante.idDNITipo = 1 ;
   console.log(this.estudiante);
   console.log('hizo clic en grabar');
  	this.userService.register(this.estudiante).subscribe((data)=>{
    console.log(data);
    const datos= data ;
    const datito = JSON.stringify(data);
    var n = datito.search("true");
    console.log('este es con json', datito);
    console.log('este es con json', n);
    alert('Registro Exitoso');
    this.router.navigate( ['/login'] );
  	},(error)=>{
  		  console.log(error);
  	  	alert('Ocurrio un error');
  	 });
  }
}

