import { Recuperar } from './../../interfaces/recuperar';
import { UserserviceService } from 'src/app/servicios/userservice.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reestablecer',
  templateUrl: './reestablecer.component.html',
  styleUrls: ['./reestablecer.component.css']
})
export class ReestablecerComponent implements OnInit {


  DNI: number;
  Correo: string;
  //valores: Object;
  // FORMULARIO REACTIVO
  valores: Recuperar = {
    DNI: null,
    Correo: null,
  };
  form: FormGroup;
  // FORMULARIO REACTIVO

  constructor( private userService: UserserviceService, private router: Router, private formBuilder: FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {

  }

   // FORMULARIO REACTIVO

    private buildForm(){
      this.form = this.formBuilder.group({
        /* UserName : ['', [Validators.required, Validators.pattern(/posgrado|admin-posgrado/), Validators.maxLength(14)]], */
        UserName : ['', [Validators.required]],
        UserCorreo : ['', [Validators.required, Validators.email]],
      });

    }
    save(event: Event){
      event.preventDefault();
      if (this.form.valid){
        // Aqui podemos hacer una peticion al servidor.
        this.recuperarContraseña();
        const value = this.form.value;
        console.log(value);
      } else{
        this.form.markAllAsTouched();
      }
    }
    registro(event: Event){
      this.router.navigate(['/registro']);
    }
    get CampoUs(){
      return this.form.get('UserName');
    }

    get CampoCor(){
      return this.form.get('UserCorreo');
    }
//////////variables para error/////
error: boolean = false;
 mensajeError: string='';
 //alert('Se envio la contraseña');
//////////////////
   // FORMULARIO REACTIVO
   recuperarContraseña(){
    console.log('hizo clic en enviar contraseña');
    this.valores.DNI = this.DNI;
    this.valores.Correo = this.Correo;
    console.log(this.valores);
    console.log();
    this.userService.recuperarContraseña(this.valores).subscribe((data)=>{
    const datos= data ;
    const datito = JSON.stringify(data);
    var n = datito.search("true");
    console.log('este es con json', datito);
    console.log('este es con json', n);
    if(n > 0){
      alert('Se envio un mensaje a su correo electronico');
      this.router.navigate( ['/login'] );
    }
    else{
      alert('Correo o DNI incorrecto');
    }
    },(error)=>{
         console.log(error);
         alert('Ocurrio un error');
      });
   }

}
