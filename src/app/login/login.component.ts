import { UserserviceService } from './../servicios/userservice.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { Token } from '../interfaces/token';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from '../Toast/toasts-container/toast-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  // FORMULARIO REACTIVO
  form: FormGroup;
  fieldTextType: boolean;

  cargando: boolean;
  // FORMULARIO REACTIVO

  // tslint:disable-next-line: max-line-length
  constructor( private userService: UserserviceService, private router: Router, private formBuilder: FormBuilder, public ToastService: ToastService) {
    this.buildForm();

  }
  user: User = {
    DNI: null,
    password: null,
  };
  token: Token = {
    token: null,
  };
  ngOnInit(): void {
    this.cargando = false;
  }

   // FORMULARIO REACTIVO

    private buildForm(){
      this.form = this.formBuilder.group({
        /* UserName : ['', [Validators.required, Validators.pattern(/posgrado|admin-posgrado/), Validators.maxLength(14)]], */
        // tslint:disable-next-line: max-line-length
        UserName : ['', [Validators.required, Validators.maxLength(8), Validators.pattern(/^[0-9]/), Validators.minLength(8) ]],
        UserContra : ['', [Validators.required, Validators.maxLength(20), Validators.minLength(6), Validators.pattern(/^[0-9A-Za-z]/)]]
      });

    }
    save(event: Event){
      event.preventDefault();
      if (this.form.valid){
        // Aqui podemos hacer una peticion al servidor.
        this.userAuthentification();
        const value = this.form.value;
        console.log(value);
      } else{
        this.form.markAllAsTouched();
      }
    }
    registro(event: Event){
      this.router.navigate(['/registro']);
    }
    get CampoUs(){
      return this.form.get('UserName');
    }

    get CampoCon(){
      return this.form.get('UserContra');
    }

    /// variables para errores/
    error: boolean = false;
    mensajeError: string='';
    //////////////
   // FORMULARIO REACTIVO
   userAuthentification(){
    console.log(this.user);
    this.cargando = true;
    this.userService.userAuthentification(this.user).subscribe((data: any) => {
      localStorage.setItem('token', data.token);
      /* this.router.navigate(['/adlistalumnos']); */
      console.log(localStorage.getItem('token'));
      this.token.token = localStorage.getItem('token');
      this.userService.permisos(this.token).subscribe((data:any) =>{
        const datos = data;
        console.log(datos);
      });
      this.userService.role(this.token).subscribe((data: any) => {
        const datos = data;
        console.log(datos);
        localStorage.setItem('rolesO', datos[0].Operador );
        localStorage.setItem('rolesD', datos[0].Docente );
        localStorage.setItem('rolesE', datos[0].Estudiante);
        this.router.navigate(['/adlistalumnos']);
        });
      },
      (err: HttpErrorResponse) => {
        this.error = true;
        this.cargando = false;
         this.mensajeError = err.error.message;
         /* alert(this.mensajeError); */
         this.showDanger();
        console.log(err.error.message);
        /* this.showStandard(); */
      });
  }

  /* OJITO */
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
    console.log("entro");
  }
  /* OJITO */
  showDanger() {
    this.ToastService.show(this.mensajeError+'!', { classname: 'bg-danger text-light', delay: 3000 });
  }
 /*  showStandard() {
    this.ToastService.show(this.mensajeError);
  } */

  /* FORMULARIO ESTATICO */

  /* FORMULARIO ESTATICO */
}
