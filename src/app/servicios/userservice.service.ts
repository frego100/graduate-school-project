import { Recuperar } from './../interfaces/recuperar';
import { Estudiante } from './../interfaces/estudiante';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interfaces/user';
import { Token } from '../interfaces/token';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  API_ENDPOINT = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/login';
  constructor( private http: HttpClient) { }
  userAuthentification(user: User){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(this.API_ENDPOINT+'?token='+localStorage.getItem('token'), user, { headers : headers });
  }
  logout(token: Token){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/logout'+'?token='+localStorage.getItem('token'), token, { headers : headers });
  }
  role(token: Token){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/data'+'?token='+localStorage.getItem('token'), token, { headers : headers });
  }
  register(estudiante: Estudiante ){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/register'+'?token='+localStorage.getItem('token'), estudiante , {headers :headers} );
  }
  permisos(token: Token ){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/permission'+'?token='+localStorage.getItem('token'), token , {headers :headers} );
  }
  recuperarContraseña(valores: Recuperar){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/olvidoClave'+'?token='+localStorage.getItem('token'), valores, {headers :headers} );

  }

}
