import { TipoDNI } from 'src/app/interfaces/tipodoc';
import { CrearObs } from './../interfaces/crearObs';
import { EstEscEditar } from 'src/app/interfaces/estescEditar';
import { DetallesTesis } from './../interfaces/detallesTesis';
import { Grado } from './../interfaces/grado';
import { EnvioObs } from './../interfaces/envioObs';
import { EstadoTesis } from './../interfaces/estadotesis';
import { TipoTesis } from './../interfaces/tipotesis';
import { asapScheduler, Observable } from 'rxjs';
import { TipoJurado } from './../interfaces/tipojurado';
import { Rol } from './../interfaces/rol';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Estudiante } from '../interfaces/estudiante';
import { Proceso } from '../interfaces/proceso';
import { Facultad } from '../interfaces/facultad';
import { Estudiantec } from '../interfaces/estudiantec';
import { EditarPrograma, EstEsc } from '../interfaces/estudianteescuela';
import { Escuela } from '../interfaces/escuela';
import { Dictamen } from '../interfaces/dictamen';
import { TesisEstEsc } from '../interfaces/tesisEstEsc';
import { FinObs, Observacion, Consulta } from '../interfaces/observacion';
import { RolProceso } from '../interfaces/rolproceso';
import { EnvioCorr } from '../interfaces/envioCorr';
import { Informes, InformesCampos } from '../interfaces/informes';
import { Token } from '../interfaces/token';
import { NumberSymbol } from '@angular/common';
import { BuscaTesis } from '../interfaces/buscadorTesis';
import { Rolcitos } from '../interfaces/roles';

@Injectable({
  providedIn: 'root'
})
export class ProcesosService {
  constructor(private http: HttpClient) { }
  getNombre(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    // tslint:disable-next-line: max-line-length
    return (this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estudiante/obtenerNombreEstudiante/' + id+'?token='+localStorage.getItem('token'), { headers }));
  }
  data(token: Token) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/data'+'?token='+localStorage.getItem('token'), token, { headers });
  }
  /**
   * @ignore Inicio de métodos de servicios de estudiante
   */
  /**
   * Enlace para obtener los datos referentes a Estudiante.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Estudiante}
   */

  // tslint:disable-next-line: member-ordering
  API_ENDPOINT = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estudiante';
  /**
   * @func crearEstudiante
   * @description Funcion que envia los datos para registrar a un Usuario en el sistema.
   * @param estudiante Objeto que contiene la data del estudiante a registrar.
   * @returns Mensaje indicando si se creo correctamente o no
   */
  crearEstudiante(estudiante: Estudiantec) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_ENDPOINT + '/crearEstudiante/'+'?token='+localStorage.getItem('token'), estudiante, { headers });
  }
  /**
   * @func getAllEstudiantes
   * @description Funcion que muestra los usuarios registrados en el sistema.
   * @returns Data de los usuarios del sistema
   */
  getAllEstudiantes() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ENDPOINT + '/listar/'+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func getAlumno
   * @description Funcion que muestra los datos de un usuario.
   * @param id El id del estudiante a mostrar sus datos.
   * @returns Data del usuario seleccionado.
   */
  getAlumno(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return (this.http.get(this.API_ENDPOINT + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers }));
  }
  /**
   * @func eliminar
   * @description Funcion que elimina la data de un usuario en el sistema.
   * @param id El id del estudiante a eliminar.
   * @returns Mensaje indicando si se elimino correctamente o no
   */
  eliminar(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_ENDPOINT + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func update
   * @description Funcion que actualiza los datos de un Usuario en el sistema.
   * @param id El id del estudiante a actualizar.
   * @param alumno El objeto que contiene la data del estudiante.
   * @returns Mensaje indicando si se actualizó correctamente o no
   */
  update(id: number, alumno: Estudiantec) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_ENDPOINT + '/editar/' + id+'?token='+localStorage.getItem('token'), alumno, { headers });
  }
  /**
   * @func buscar
   * @description Funcion que busca a los usuarios según parámetro en el sistema.
   * @param valor El valor, dato de busqueda.
   * @returns Data de usuarios que coinciden con la búsqueda.
   */
  buscar(valor: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const token = localStorage.getItem('token');
    return (this.http.get(this.API_ENDPOINT + '/listar/?token=' + token + '&' + valor, { headers }));
  }
  /**
   * @func getDetallesAlumno
   * @description Funcion que muestra los datos de un usuario a diferencia del metodo getAlumno,
   *              este servicio tiene el acceso de roles distinto.
   * @param id El id del estudiante a mostrar sus datos.
   * @returns Data del usuario seleccionado.
   */
  getDetallesAlumno(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return (this.http.get(this.API_ENDPOINT + '/verDetalleEstudiante/' + id+'?token='+localStorage.getItem('token'), { headers }));
  }
  /**
   * @ignore Fin de métodos de servicios de estudiante escuela
   */
  /**
   * @ignore Inicio de métodos de servicios de estudiante escuela
   */
  /**
   * Enlace para obtener los datos referentes a Estudiante Escuela.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/EstudianteEscuela}
   */

  // tslint:disable-next-line: member-ordering
  API_ESTESC = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/EstudianteEscuela';
  /**
   * @func estescLista
   * @description Funcion que lista las escuelas a las que pertenece un estudiante.
   * @param id El id del estudiante.
   * @returns Data con las escuelas que un estudiante tiene.
   */
  estescLista(id: number) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ESTESC + '/listar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func estescCrear
   * @description Funcion que lista las escuelas a las que pertenece un estudiante.
   * @param estesc Objeto que contiene la data para registrar la escuela de un estudiante.
   * @returns Mensaje indicando si se creo correctamente o no .
   */
  estescCrear(estesc: EstEsc) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_ESTESC + '/crear/'+'?token='+localStorage.getItem('token'), estesc, { headers });
  }
  /**
   * @func estescVer
   * @description Funcion que muestra las escuelas a las que pertenece un estudiante.
   * @param id El id del estudiante.
   * @returns Data con las escuelas que un estudiante tiene.
   */
  estescVer(id: number) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ESTESC + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func estescEditar
   * @description Funcion que lista las escuelas a las que pertenece un estudiante.
   * @param estesc Objeto que contiene la data para registrar la escuela de un estudiante.
   * @returns Data con las escuelas que un estudiante tiene.
   */
  estescEditar(id: number, estesc: EstEscEditar) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_ESTESC + '/editar/' + id+'?token='+localStorage.getItem('token'), estesc, { headers });
  }
  /**
   * @func estescEliminar
   * @description Funcion que elimina una escuela a la que pertenece un estudiante.
   * @param id El id de la relacion a eliminar (idEstudianteEscuela).
   * @returns Mensaje indicando si se elimino correctamente o no.
   */
  estescEliminar(id: number) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_ESTESC + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @ignore Fin de métodos de servicios de estudiante escuela
   */
  /**
   * @ignore Inicio de métodos de servicios de Rol
   */
  /**
   * Enlace para obtener los datos referentes a ROL.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Rol}
   */

  // tslint:disable-next-line: member-ordering
  API_ROL = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Rol';
  /**
   * @func rolLista
   * @description Funcion que lista los roles.
   * @returns Data con los roles en el sistema.
   */
  rolLista() {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ROL + '/listar/'+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func rolCrear
   * @description Funcion que crea un rol en el sistema.
   * @param rol Objeto que contiene la data del rol a crear
   * @returns Mensaje indicando se se creo el rol correctamente o no.
   */
  rolCrear(rol: Rol) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_ROL + '/crear/'+'?token='+localStorage.getItem('token'), rol, { headers });
  }
  /**
   * @func rolEliminar
   * @description Funcion que elimina un rol en el sistema.
   * @param id El id del rol a eliminar.
   * @returns Mensaje indicando si se elimino el rol correctamente o no.
   */
  rolEliminar(id: number) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_ROL + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func verRol
   * @description Funcion que muestra los datos un rol.
   * @param id El id del rol a mostrar.
   * @returns Data del rol.
   */
  verRol(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ROL + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @ignore Fin de métodos de servicios de estudiante escuela
   */
  //ROL-PROCESO
  RolProceso(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Proceso/verRolProceso/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  //ROL-ACTIVIDAD el id es el id rol proceso

  darpermisos(id: number, rolactividad: RolProceso) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    const token = localStorage.getItem('token');
    return this.http.put('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Proceso/editarProcesoActividad/' + id+'?token='+localStorage.getItem('token'), rolactividad, { headers: headers });
  }

  // metodo servicio para filtrado de usuarios que son solo docente
  docenteFiltro() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estudiante/listarDocentes/'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  //metodos servicios proceso
  API_PROCESO = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Proceso';

  ProcesoActividad(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Proceso/verProcesoActividad/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  procesoLista() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_PROCESO + '/listarProceso/'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  procesoCrear(proceso: Proceso) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.API_PROCESO + '/crearProceso?token=' + token, proceso, { headers: headers });
  }
  //metodos servicios actividad
  API_ACTIVIDAD = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Actividad';

  /* ProcesoActividad(id: number){
    const headers = new HttpHeaders({'Authorization': 'Bearer' +  localStorage.getItem('token')});
    return this.http.get('https://gestion-tesis-backend.herokuapp.com/api/Proceso/verProcesoActividad/'+ id, {headers:headers});
  } */
  actividadLista(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ACTIVIDAD + '/listarActividad/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  actividadCrear(proceso: Proceso) {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.API_ACTIVIDAD + '/crearActividad?token=' + token, proceso, { headers: headers });
  }

  //metodos servicios de tipotesis
  API_TESIS = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/TipoTesis';

  tesisLista() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TESIS + '/listar'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  crearTipoTesis(tipoTesis: TipoTesis) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const token = localStorage.getItem('token');
    // tslint:disable-next-line: indent
    return this.http.post(this.API_TESIS + '/crear?token=' + token, tipoTesis, { headers: headers });
  }
  eliminarTipoTesis(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_TESIS + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }

  verTipoTesis(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TESIS + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  editarTipoTesis(id: number, tipoTesis: TipoTesis) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_TESIS + '/editar/' + id+'?token='+localStorage.getItem('token'), tipoTesis, { headers });
  }

  //metodo servicio estado de tesis
  API_ESTADO = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estado'
  tesisEstadoLista() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ESTADO + '/listar'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  crearEstadoTesis(estadoTesis: EstadoTesis) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });

    // tslint:disable-next-line: indent
    return this.http.post(this.API_ESTADO + '/crear'+'?token='+localStorage.getItem('token') , estadoTesis, { headers: headers });
  }
  eliminarEstadoTesis(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_ESTADO + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }

  verEstadoTesis(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ESTADO + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  editarEstadoTesis(id: number, estadoTesis: EstadoTesis) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_ESTADO + '/editar/' + id+'?token='+localStorage.getItem('token'), estadoTesis, { headers });
  }
  //metodo servicio de tesisestudiante escuela
  API_TESISESTESC = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis'
  listarTesisEstudianteEscuela(id: number) {

    /* const headers = new HttpHeaders({'Content-Type': 'application/json'});
    const token = localStorage.getItem('token'); */
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TESISESTESC + '/listarTesisEstudianteEscuela/' + id+'?token='+localStorage.getItem('token'), { headers: headers });

  }
  verTesisEstudianteEscuela(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TESISESTESC + '/verTesis/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  crearTesisEstudianteEscuela(tesisEstEsc: TesisEstEsc, fileToUpload: File, fileToUploadPlanT: File) {
    console.log('servicio recibe objeto de tesis:', tesisEstEsc)
    var tesis = JSON.stringify(tesisEstEsc)
    const file: FormData = new FormData();
    var Idestesc = tesisEstEsc.IdEstudianteEscuela.toString();
    console.log(Idestesc);
    var idtiptesis = tesisEstEsc.IdTipoTesis.toString();
    var idestado = tesisEstEsc.IdEstado.toString();
    var iddictamen = tesisEstEsc.IdDictamen.toString();
    var idestudiante = tesisEstEsc.IdEstudiante.toString();
    file.append('IdEstudianteEscuela', Idestesc);
    file.append('IdTipoTesis', idtiptesis);
    file.append('IdEstado', idestado);
    file.append('IdDictamen', iddictamen);
    file.append('IdEstudiante', idestudiante);
    file.append('Resumen', tesisEstEsc.Resumen);
    file.append('PalabrasClave', tesisEstEsc.PalabrasClave);
    file.append('TituloTesis', tesisEstEsc.TituloTesis);
    console.log('titulo tesis desde servicio', tesisEstEsc.TituloTesis)
    file.append('file', fileToUpload);
    if (fileToUploadPlanT == null) {
      console.log('no hay archivo de plan de tesis');
    } else {
      file.append('filePlanTesis', fileToUploadPlanT);
    }
    console.log(file);
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_TESISESTESC + '/crear'+'?token='+localStorage.getItem('token'), file, { headers });
  }

  tamaño: string;
  idPresi: string;
  idSecre: string;
  idSuple: string;
  graduacion: string;
  hora: string;

  modificarTesisEstudianteEscuela(idTesis: number, idESTESC: number, tesisEstEsc: DetallesTesis, jurados: number[],
                                  fileToUploadT: File, fileToUploadJ: File, fileToUploadA: File, fileToUploadD: File,
                                  fileToUploadPlanT: File) {
    const file: FormData = new FormData();
    var Idestesc = idESTESC.toString();
    var idtiptesis = tesisEstEsc.Tesis.IdTipoTesis.toString();
    var idestado = tesisEstEsc.Tesis.IdEstado.toString();
    var iddictamen = tesisEstEsc.Tesis.IdDictamen.toString();
    var idJurados = tesisEstEsc.Jurados[0].id.toString();
    /* if (tesisEstEsc.Tesis.FechaGraduacion == null) {
      this.graduacion = null;
    } else if (tesisEstEsc.Tesis.FechaGraduacion.length == 10) {
      this.graduacion = tesisEstEsc.Tesis.FechaGraduacion + ' 09:00:00';
    } else {
      this.graduacion = tesisEstEsc.Tesis.FechaGraduacion;
    } */
    this.graduacion = tesisEstEsc.Tesis.FechaGraduacion;
    this.hora = tesisEstEsc.Tesis.HoraGraduacion;
    if (fileToUploadT == null) {
      this.tamaño = '';
    } else {
      this.tamaño = fileToUploadT.size.toString();
    }
    this.idPresi = jurados[0] == null ? (this.idPresi = '') : (this.idPresi = jurados[0].toString());
    this.idSecre = jurados[1] == null ? (this.idSecre = '') : (this.idSecre = jurados[1].toString());
    this.idSuple = jurados[2] == null ? (this.idSuple = '') : (this.idSuple = jurados[2].toString());
    file.append('IdEstudianteEscuela', Idestesc);
    file.append('TituloTesis', tesisEstEsc.Tesis.TituloTesis);
    file.append('IdTipoTesis', idtiptesis);
    file.append('IdEstado', idestado);
    file.append('IdDictamen', iddictamen);
    file.append('PalabrasClave', tesisEstEsc.Tesis.PalabrasClave);
    file.append('Resumen', tesisEstEsc.Tesis.Resumen);
    file.append('idsJurados[0]', idJurados);
    file.append('idsJurados[1]', this.idPresi);
    file.append('idsJurados[2]', this.idSecre);
    file.append('idsJurados[3]', this.idSuple);
    if (this.graduacion == null) {
      console.log('no hay registro de fecha');
    } else {
      file.append('FechaGraduacion', this.graduacion);
    }
    if (this.hora == null) {
      console.log('no hay registro de fecha');
    } else {
      file.append('HoraGraduacion', this.hora);
    }
    if (fileToUploadA == null) {
      console.log('no hay archivo de Asesor');
    } else {
      file.append('fileAsesor', fileToUploadA);
    }
    if (fileToUploadT == null) {
      console.log('no hay archivo de Tesis');
    } else {
      file.append('fileTesis', fileToUploadT);
    }
    if (fileToUploadJ == null) {
      console.log('no hay archivo de Jurado');
    } else {
      file.append('fileJurado', fileToUploadJ);
    }
    if (fileToUploadD == null) {
      console.log('no hay archivo de Dictamen');
    } else {
      file.append('fileDictamen', fileToUploadD);
    }
    if (fileToUploadPlanT == null) {
      console.log('no hay archivo de plan de tesis');
    } else {
      file.append('filePlanTesis', fileToUploadPlanT);
    }

    file.append('Tamano', this.tamaño);
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_TESISESTESC + '/modificarTesis/' + idTesis+'?token='+localStorage.getItem('token'), file, { headers });
  }
  eliminarEstEsc(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_TESISESTESC + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  descargarPDF(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis/obtenerarchivoTesis/' + id+'?token='+localStorage.getItem('token'), { headers: headers });

  }
  descargarAsesor(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis/obtenerarchivoResolucionAsesor/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  descargarDictamen(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis/obtenerarchivoDictamen/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  descargarJurado(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis/obtenerarchivoResolucionJurado/' + id+'?token='+localStorage.getItem('token'), { headers: headers });

  }
  descargarPlanTesis(id: number) {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis/obtenerarchivoPlanTesis/' + id+'?token='+localStorage.getItem('token'), { headers });

  }
  postFileTesisMod(fileToUpload: File, id: number) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Tesis/ActualizarArchivo/';
    const file: FormData = new FormData();

    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    var tamano = fileToUpload.size.toString();
    file.append('Tamano', tamano);
    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint + id+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  /**
   * @ignore Inicio de métodos de servicios de facultad
   */
  /**
   * Enlace para obtener los datos referentes a Facultad.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Facultad}
   */

  // tslint:disable-next-line: member-ordering
  API_FACULTAD = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Facultad';
  /**
   * @func listaFacultades
   * @description Funcion que obtiene el listado de las facultades existentes en la base de datos.
   * @returns Data de las facultades existentes en la base de datos
   */
  listaFacultades() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_FACULTAD + '/listar'+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func crearFacultad
   * @description Funcion que permite la creación de una facultad en la base de datos.
   * @param facultad parametro que recibe un objeto de tipo facultad.
   * @returns Un mensaje de confirmación indicando si se creo la facultad o no.
   */
  crearFacultad(facultad: Facultad) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_FACULTAD + '/crear'+'?token='+localStorage.getItem('token'), facultad, { headers });
  }
  /**
   * @func eliminarFacultad
   * @description Funcion que permite la eliminación de una facultad en la base de datos.
   * @param id parametro que indica el id de la facultad a eliminar.
   * @returns Mensaje de confirmación indicando si se elimino la facultad o no.
   */
  eliminarFacultad(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_FACULTAD + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func verFacultad
   * @description Funcion que permite visualizar los datos de una facultad de la base de datos.
   * @param id parametro que indica el id de la facultad a visualizar.
   * @returns Data de la facultad consultada.
   */
  verFacultad(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_FACULTAD + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func editarFacultad
   * @description Funcion que permite editar los datos de una facultad de la base de datos.
   * @param id parametro que indica el id de la facultad que se desea editar.
   * @param facultad parametro que recibe un objeto de tipo facultad.
   * @returns Mensaje de confirmación indicando si se edito la facultad o no.
   */
  editarFacultad(id: number, facultad: Facultad) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_FACULTAD + '/editar/' + id+'?token='+localStorage.getItem('token'), facultad, { headers });
  }
  /**
   * @func listarEscuelaxFacultad
   * @description Funcion que permite obtener una lista de las escuelas pertenecientes a una facultad de la base de datos.
   * @param id parametro que indica el id de la facultad, para obtener las escuelas relacionadas.
   */
  listarEscuelaxFacultad(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_FACULTAD + '/ListarEscuelasFacultad/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @ignore Fin de métodos de servicios de facultad
   */
  //metodos servicio para DICTAMENES

  API_DICTAMEN = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Dictamen';
  listaDictamenes() {
    //console.log(localStorage.getItem('token'));
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_DICTAMEN + '/listar'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  crearDictamen(dictamen: Dictamen) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const token = localStorage.getItem('token');
    // tslint:disable-next-line: indent
    return this.http.post(this.API_DICTAMEN + '/crear?token=' + token, dictamen, { headers: headers });
  }
  eliminarDictamen(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_DICTAMEN + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }

  verDictamen(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_DICTAMEN + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  editarDictamen(id: number, dictamen: Dictamen) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_DICTAMEN + '/editar/' + id+'?token='+localStorage.getItem('token'), dictamen, { headers });
  }

  //metodos para Tipo de documentos
  API_TIPODOC = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/TipoDocumento';
  listaTipoDoc() {
    const headers = new HttpHeaders({ Authorization : 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TIPODOC + '/listar'+'?token='+localStorage.getItem('token'), { headers });
  }
  crearTipoDoc(tipoDNI: TipoDNI) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const token = localStorage.getItem('token');
    // tslint:disable-next-line: indent
    return this.http.post(this.API_TIPODOC + '/crear?token=' + token, tipoDNI, { headers: headers });
  }
  eliminarTipoDoc(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_TIPODOC + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }

  verTipoDoc(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TIPODOC + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  editarTipoDoc(id: number, tipodoc: TipoDNI) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_TIPODOC + '/editar/' + id+'?token='+localStorage.getItem('token'), tipodoc, { headers });
  }
  /**
   * @ignore Inicio de métodos de servicios de escuela
   */
  /**
   * Enlace para obtener los datos referentes a escuela.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Escuela}
   */
  // tslint:disable-next-line: member-ordering
  API_ESCUELA = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Escuela';
  /**
   * @func getAllEscuelas
   * @description Funcion que obtiene el listado de las escuelas existentes en la base de datos.
   * @returns Data de las escuelas existentes en la base de datos
   */
  getAllEscuelas() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ESCUELA + '/listar'+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func save
   * @description Funcion que permite la creación de una escuela en la base de datos.
   * @param escuela parametro que recibe un objeto de tipo escuela.
   * @returns Un mensaje de confirmación indicando si se creo la escuela o no.
   */
  save(escuela: Escuela) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.API_ESCUELA + '/crear'+'?token='+localStorage.getItem('token'), escuela, { headers });
  }
  /**
   * @func eliminarEscuela
   * @description Funcion que permite la eliminación de una escuela en la base de datos.
   * @param {number} id parametro que indica el id de la escuela a eliminar.
   * @returns Mensaje de confirmación indicando si se elimino la escuela o no.
   */
  eliminarEscuela(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_ESCUELA + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func verEscuela
   * @description Funcion que permite visualizar los datos de una escuela de la base de datos.
   * @param id parametro que indica el id de la escuela a visualizar.
   * @returns Data de la escuela consultada.
   */
  verEscuela(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ESCUELA + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func editarEscuela
   * @description Funcion que permite editar los datos de una escuela de la base de datos.
   * @param id parametro que indica el id de la escuela que se desea editar.
   * @param escuela parametro que recibe un objeto de tipo escuela.
   * @returns Mensaje de confirmación indicando si se edito la escuela o no.
   */
  editarEscuela(id: number, escuela: Escuela) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_ESCUELA + '/editar/' + id+'?token='+localStorage.getItem('token'), escuela, { headers });
  }
  /**
   * @ignore Fin de métodos de servicios de escuela
   */
  //metodos servicio para TIPO DE JURADO
  API_TIPOJURADO = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/TipoJurado';
  listaTiposJurado() {
    //console.log(localStorage.getItem('token'));
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TIPOJURADO + '/listar'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  crearTipoJurado(tipoJurado: TipoJurado) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const token = localStorage.getItem('token');
    // tslint:disable-next-line: indent
    return this.http.post(this.API_TIPOJURADO + '/crear?token=' + token, tipoJurado, { headers: headers });
  }
  eliminarTipoJurado(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_TIPOJURADO + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }

  verTipoJurado(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_TIPOJURADO + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  editarTipoJurado(id: number, tipoJurado: TipoJurado) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.API_TIPOJURADO + '/editar/' + id+'?token='+localStorage.getItem('token'), tipoJurado, { headers });
  }

  /* metodos para observaciones */
  OBSERVACIONES = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Observacion';
  listaObservacion(id: number) {
    //console.log(localStorage.getItem('token'));
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.OBSERVACIONES + '/listar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  crearObservacion(observacion: CrearObs) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.OBSERVACIONES + '/crear'+'?token='+localStorage.getItem('token'), observacion, { headers: headers });
  }
  eliminarObservacion(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.OBSERVACIONES + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  aprobarObservacion(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.OBSERVACIONES + '/aprobado/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  corregirObservacion(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.OBSERVACIONES + '/corregido/' + id+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  envioObservacion(envioObs: EnvioObs) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.OBSERVACIONES + '/EnviarObservacion'+'?token='+localStorage.getItem('token'), envioObs, { headers: headers });
  }
  envioCorreccion(idsObservaciones: EnvioCorr) {
    console.log('ids array', idsObservaciones);
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.OBSERVACIONES + '/enviarCorrecciones'+'?token='+localStorage.getItem('token'), idsObservaciones, { headers: headers });
  }
  agregarConsultas(id: number, Consulta: Consulta) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.OBSERVACIONES + '/agregarConsultar/' + id+'?token='+localStorage.getItem('token'), Consulta, { headers: headers });
  }
  finObservacion(finObs: FinObs) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Jurado/FinalizarObservacion'+'?token='+localStorage.getItem('token'), finObs, { headers: headers });
  }
  verObservacion(id: number) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.OBSERVACIONES + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers: headers });

  }
  editarObservacion(id: number, editarObs: CrearObs) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.OBSERVACIONES + '/editar/' + id+'?token='+localStorage.getItem('token'), editarObs, { headers: headers });
  }
  /* FIN observaciones */
  /**
   * @ignore Inicio de métodos de servicios de grado
   */
  /**
   * Enlace para obtener los datos referentes a grado.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Grado}
   */

  // tslint:disable-next-line: member-ordering
  GRADO = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Grado';
  /**
   * @func listaGrado
   * @description Funcion que obtiene el listado de los grados existentes en la base de datos.
   * @returns Data de los grados existentes en la base de datos
   */
  listaGrado() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.GRADO + '/listar'+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func crearGrado
   * @description Funcion que permite la creación de un grado en la base de datos.
   * @param {Grado} grado parametro que recibe un objeto de tipo grado.
   * @returns Un mensaje de confirmación indicando si se creo la grado o no.
   */
  crearGrado(grado: Grado) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.GRADO + '/crear'+'?token='+localStorage.getItem('token'), grado, { headers });
  }
  /**
   * @func eliminarGrado
   * @description Funcion que permite la eliminación de un grado en la base de datos.
   * @param {number} id parametro que indica el id del grado a eliminar.
   * @returns Mensaje de confirmación indicando si se elimino el grado o no.
   */
  eliminarGrado(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.GRADO + '/eliminar/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func verGrado
   * @description Funcion que permite visualizar los datos de un grado de la base de datos.
   * @param {number} id parametro que indica el id del grado a visualizar.
   * @returns Data del grado consultada.
   */
  verGrado(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.GRADO + '/ver/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func editarGrado
   * @description Funcion que permite editar los datos de un grado de la base de datos.
   * @param {number} id parametro que indica el id del grado que se desea editar.
   * @param {Escuela} escuela parametro que recibe un objeto de tipo grado.
   * @returns Mensaje de confirmación indicando si se edito el grado o no.
   */
  editarGrado(id: number, grado: Grado) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.put(this.GRADO + '/editar/' + id+'?token='+localStorage.getItem('token'), grado, { headers });
  }
  /**
   * @func listarGradoxEscuela
   * @description Funcion que permite obtener una lista de los grados pertenecientes a una escuela de la base de datos.
   * @param {number} id parametro que indica el id de la escuela, para obtener los grados relacionadas.
   */
  listarGradoxEscuela(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get('http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/grado/listarPorEscuela/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @ignore Fin de métodos de servicios de escuela
   */


  /////////////////////////////////////////////////////////////
  ////////////////////////////////SUBIR ARCHIVOS//////////////////////////

  postFile(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Escuela/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  postFileEstudiante(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estudiante/subirExcelEstudiantes';
    const files: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    files.append('files', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), files, { headers: headers });
  }
  postFileDocente(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estudiante/subirExcelDocentes';
    const files: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    files.append('files', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), files, { headers: headers });
  }
  postFileFacultad(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Facultad/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  postFileTesis(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Facultad/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  postFileGrado(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Grado/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirTipoDocumento(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/TipoDocumento/subirExcelTipoDocumento';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirDictamen(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Dictamen/subirExcelDictamen';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirTipoJurado(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/TipoJurado/subirExcelTipoJurado';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirEstadoTesis(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Estado/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirTipoTesis(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/TipoTesis/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirEscuela(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Escuela/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  subirGrado(fileToUpload: File) {
    const endpoint = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Grado/subirExcel';
    const file: FormData = new FormData();
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });

    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint+'?token='+localStorage.getItem('token'), file, { headers: headers });
  }
  /* DESCARGA DE ARCHIVOS */
  PLANTILLA = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/'
  PLANTILLAS = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Plantilla/'
  descargarEstudiante() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLA + 'PlantillaEstudianteExcel'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  descargarDocente() {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLA + 'PlantillaDocenteExcel'+'?token='+localStorage.getItem('token'), { headers: headers });
  }
  descargarPlantillaFacultad() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'getPlantillaFacultad'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaTipoJurado() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'getPlantillaTipoJurado'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaDictamen() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'getPlantillaDictamen'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaTipoDocumento() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'getPlantillaTipoDocumento'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaEstadoTesis() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'PlantillaEstadoExcel'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaTipoTesis() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'PlantillaTipoTesisExcel'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaGrado() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'PlantillaGradoExcel'+'?token='+localStorage.getItem('token'), { headers });
  }
  descargarPlantillaEscuela() {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.PLANTILLAS + 'PlantillaEscuelaExcel'+'?token='+localStorage.getItem('token'), { headers });
  }

  /* FIN DESCARGA DE ARCHIVOS */

  //////////////REPORTES////////////
  REPORTE = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/generarReporte';
  datosReporte(datosReporte: InformesCampos) {
    const headers = new HttpHeaders({ 'Authorization': 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.REPORTE+'?token='+localStorage.getItem('token'), datosReporte, { headers: headers });
  }
  ///////////FIN REPORTES/////////
  /**
   * @ignore Inicio de métodos de servicios de buscador de tesis
   */
  /**
   * Enlace para obtener los datos referentes a grado.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Grado}
   */

  // tslint:disable-next-line: member-ordering
  BUSCATESIS = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/BuscadorTesis/buscar';
  /**
   * @func buscarTesis
   * @description Funcion que obtiene el listado de datos según la tesis buscada.
   * @returns Data de las coincidencias con la tesis buscada.
   */
  buscarTesis(buscadorTesis: BuscaTesis) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.post(this.BUSCATESIS+'?token='+localStorage.getItem('token'), buscadorTesis, { headers });
  }
/**
   * @ignore Inicio de métodos de servicios de documentos de inteeres
   */
  /**
   * Enlace para obtener los datos referentes a escuela.
   *
   * {@link https://gestion-tesis-backend.herokuapp.com/api/Escuela}
   */
  // tslint:disable-next-line: member-ordering
  API_DOC_INTERES = 'http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/DocumentoInteres';
  /**
   * @func getAllDocs
   * @description Funcion que obtiene el listado de los documentos de interes existentes en la base de datos.
   * @returns Data de las escuelas existentes en la base de datos.
   */
  getAllDocs(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_ENDPOINT + '/VerDocumentoInteres/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func registrarDocs
   * @description Funcion que permite la creación de una documentos de interes en la base de datos.
   * @param fileToUpload parametro que recibe un objeto de tipo documentos de interes.
   * @returns Un mensaje de confirmación indicando si se creo la documentos de interes o no.
   */
  registrarDocs(fileToUpload: File, rolcitos: Rolcitos, nombre: string) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    const file: FormData = new FormData();
    file.append('Estudiante', rolcitos.Estudiante.toString());
    file.append('Operario', rolcitos.Operador.toString());
    file.append('Docente', rolcitos.Docente.toString());
    file.append('Documento', nombre);
    file.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(this.API_DOC_INTERES + '/RegistrarDocumento'+'?token='+localStorage.getItem('token'), file, { headers });
  }
  /**
   * @func eliminarDocs
   * @description Funcion que permite la eliminación de una documentos de interes en la base de datos.
   * @param id parametro que indica el id de la documentos de interes a eliminar.
   * @returns Mensaje de confirmación indicando si se elimino la documentos de interes o no.
   */
  eliminarDocs(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.delete(this.API_DOC_INTERES + '/EliminarDocumento/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func verDocs
   * @description Funcion que permite visualizar los datos de un documentos de interes de la base de datos.
   * @param id parametro que indica el id de los documentos de interes a visualizar.
   * @returns Data de documentos de interes consultada.
   */
  verDocs(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_DOC_INTERES + '/VerDocumento/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @func editarDocs
   * @description Funcion que permite editar los datos de un documentos de interes de la base de datos.
   * @param id parametro que indica el id de documentos de interes que se desea editar.
   * @param escuela parametro que recibe un objeto de tipo documentos de interes.
   * @returns Mensaje de confirmación indicando si se edito documentos de interes o no.
   */
  editarDocs(id: number, fileToUpload: File, rolcitos: Rolcitos, nombre: string) {
      const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
      const file: FormData = new FormData();
      file.append('Estudiante', rolcitos.Estudiante.toString());
      file.append('Operario', rolcitos.Operador.toString());
      file.append('Docente', rolcitos.Docente.toString());
      file.append('Documento', nombre);
      file.append('file', fileToUpload, fileToUpload.name);
      return this.http.post(this.API_DOC_INTERES + '/ModificarDocumento/' + id+'?token='+localStorage.getItem('token'), file, { headers });
  }
  descargarDocumentoInteres(id: number) {
    const headers = new HttpHeaders({ Authorization: 'Bearer' + localStorage.getItem('token') });
    return this.http.get(this.API_DOC_INTERES + '/DescargarDocumento/' + id+'?token='+localStorage.getItem('token'), { headers });
  }
  /**
   * @ignore Fin de métodos de servicios de documentos de interes
   */
}
