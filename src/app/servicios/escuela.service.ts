import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Escuela } from '../interfaces/escuela';

@Injectable({
  providedIn: 'root'
})
export class EscuelaService {

  API_ENDPOINT='http://190.119.145.150:8032/gestion-tesis-backend/public/index.php/api/Escuela';
  constructor(private httpClient: HttpClient) { }
  getAllEscuelas(){
    //console.log(localStorage.getItem('token'));
    const headers = new HttpHeaders({'Authorization': 'Bearer' +  localStorage.getItem('token')});
    return this.httpClient.get(this.API_ENDPOINT + '/listar/'+'?token='+localStorage.getItem('token'), { headers : headers});
    }
  save(escuela: Escuela ){
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    const token = localStorage.getItem('token');
  	 // tslint:disable-next-line: indent
  	 return this.httpClient.post(this.API_ENDPOINT + '/crear?token='+ token, escuela , {headers :headers} );
  }
/*   update(id: number, escuela: Escuela){
    const headers = new HttpHeaders({'Authorization': 'Bearer' +  localStorage.getItem('token')});
    const token = localStorage.getItem('token');
  	 return this.httpClient.put(this.API_ENDPOINT + '/updateEscuela/'+ id , escuela, {headers:headers});
  }
  getEscuela(id: number){
    const headers = new HttpHeaders({'Authorization': 'Bearer' +  localStorage.getItem('token')});
    const url = this.API_ENDPOINT + '/getEscuela/' + id ;
    return (this.httpClient.get( url , { headers : headers}));
  } */
  eliminar(id: number){
    const headers = new HttpHeaders({'Authorization': 'Bearer' +  localStorage.getItem('token')});
    return this.httpClient.delete(this.API_ENDPOINT + '/eliminar/'+ id+'?token='+localStorage.getItem('token'), { headers : headers});
  }
}
