import { AuthGuard } from './auth/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routes } from './routes';
import { RouterModule } from '@angular/router';
import { RegistroComponent } from './login/registro/registro.component';
import { ReestablecerComponent } from './login/reestablecer/reestablecer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserserviceService } from './servicios/userservice.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ScrollingModule} from '@angular/cdk/scrolling';

import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { es_ES } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { AdministradorComponent } from './administrador/administrador.component';
import { AdlistalumnosComponent } from './administrador/adlistalumnos/adlistalumnos.component';
import { MatMenuModule, MatMenu} from '@angular/material/menu';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { AdmodalumnoComponent } from './administrador/admodalumno/admodalumno.component';
import { AdrolesComponent } from './administrador/adroles/adroles.component';
import { VerrolComponent } from './administrador/verrol/verrol.component';
import { VerprogramaComponent } from './administrador/verprograma/verprograma.component';
import { AddtesisComponent } from './administrador/addtesis/addtesis.component';
import { AdlistaprocesosComponent } from './administrador/adlistaprocesos/adlistaprocesos.component';
import { TiptesisComponent } from './administrador/tiptesis/tiptesis.component';
import { EsttesisComponent } from './administrador/esttesis/esttesis.component';
import { AdlistaescuelasComponent } from './administrador/adlistaescuelas/adlistaescuelas.component';
import { AdlistipjuradosComponent } from './administrador/adlistipjurados/adlistipjurados.component';
import { AdregistroComponent } from './administrador/adregistro/adregistro.component';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { AdlistafacultadComponent } from './administrador/adlistafacultad/adlistafacultad.component';
import { AdlistaDocComponent } from './administrador/adlista-doc/adlista-doc.component';
import { AdlistadictamenComponent } from './administrador/adlistadictamen/adlistadictamen.component';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AdlistagradoComponent } from './administrador/adlistagrado/adlistagrado.component';
import { ToastsContainer } from './Toast/toasts-container/toasts-container.component';
import { InformesComponent } from './administrador/informes/informes.component';
import { BustesisComponent } from './administrador/bustesis/bustesis.component';
import { MatCardModule} from '@angular/material/card';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { ObservacionesPipe } from './administrador/verprograma/observaciones.pipe';
import { VerDocumentosComponent } from './administrador/ver-documentos/ver-documentos.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { NzSpinModule } from 'ng-zorro-antd/spin';

import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

export function createTranslateLoader(http: HttpClient){
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    ReestablecerComponent,
    AdministradorComponent,
    AdlistalumnosComponent,
    AdmodalumnoComponent,
    AdrolesComponent,
    VerrolComponent,
    VerprogramaComponent,
    AddtesisComponent,
    AdlistaprocesosComponent,
    TiptesisComponent,
    EsttesisComponent,
    AdlistaescuelasComponent,
    AdlistipjuradosComponent,
    AdregistroComponent,
    AdlistafacultadComponent,
    AdlistaDocComponent,
    AdlistadictamenComponent,
    AdlistagradoComponent,
    ToastsContainer,
    InformesComponent,
    BustesisComponent,
    ObservacionesPipe,
    VerDocumentosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    MatMenuModule,
    NzTableModule,
    NzDividerModule,
    NzInputModule,
    NzSelectModule,
    NzFormModule,
    NzAvatarModule,
    NzDropDownModule,
    NzButtonModule,
    NzDatePickerModule,
    NzTagModule,
    NzCollapseModule,
    NzBadgeModule,
    NzListModule,
    ScrollingModule,
    NzSkeletonModule,
    NzPopoverModule,
    NzResultModule,
    NzStepsModule,
    NzCardModule,
    NzTabsModule,
    NzPageHeaderModule,
    NzDescriptionsModule,
    NzStatisticModule,
    MatCardModule,
    NzTypographyModule,
    NzModalModule,
    NzBreadCrumbModule,
    NzToolTipModule,
    NzCheckboxModule,
    MatCheckboxModule,
    NzTimelineModule,
    NzCommentModule,
    NzSwitchModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    AutocompleteLibModule,
    NzSpinModule
  ],
  providers: [UserserviceService, AuthGuard, { provide: NZ_I18N, useValue: es_ES },{provide: LocationStrategy, useClass: HashLocationStrategy}],

  bootstrap: [AppComponent]
})
export class AppModule { }
