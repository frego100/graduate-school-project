
import { AdlistipjuradosComponent } from './administrador/adlistipjurados/adlistipjurados.component';
import { AdmodalumnoComponent } from './administrador/admodalumno/admodalumno.component';
import { LoginComponent } from './login/login.component';
import { Routes } from '@angular/router';
import { RegistroComponent } from './login/registro/registro.component';
import { ReestablecerComponent } from './login/reestablecer/reestablecer.component';
import { AuthGuard } from './auth/auth.guard';
import { AdministradorComponent } from './administrador/administrador.component';
import { AdlistalumnosComponent } from './administrador/adlistalumnos/adlistalumnos.component';
import { AdrolesComponent } from './administrador/adroles/adroles.component';
import { VerrolComponent } from './administrador/verrol/verrol.component';
import { VerprogramaComponent } from './administrador/verprograma/verprograma.component';
import { AddtesisComponent } from './administrador/addtesis/addtesis.component';
import { AdlistaprocesosComponent } from './administrador/adlistaprocesos/adlistaprocesos.component';
import { TiptesisComponent } from './administrador/tiptesis/tiptesis.component';
import { EsttesisComponent } from './administrador/esttesis/esttesis.component';
import { AdlistaescuelasComponent } from './administrador/adlistaescuelas/adlistaescuelas.component';
import { AdregistroComponent } from './administrador/adregistro/adregistro.component';
import { AdlistafacultadComponent } from './administrador/adlistafacultad/adlistafacultad.component';
import { AdlistadictamenComponent } from './administrador/adlistadictamen/adlistadictamen.component';
import { AdlistaDocComponent } from './administrador/adlista-doc/adlista-doc.component';
import { AdlistagradoComponent } from './administrador/adlistagrado/adlistagrado.component';
import { InformesComponent } from './administrador/informes/informes.component';
import { BustesisComponent } from './administrador/bustesis/bustesis.component';
import { VerDocumentosComponent } from './administrador/ver-documentos/ver-documentos.component';

export const routes: Routes = [
  { path : 'login', component: LoginComponent },
  { path : '', redirectTo: '/login', pathMatch : 'full'},
  { path : 'registro', component: RegistroComponent },
  { path : 'reestablecer', component: ReestablecerComponent },
  { path: 'adlistalumnos', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistalumnosComponent}]
  },

  { path: 'admodalumno/:id', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdmodalumnoComponent}]
  },
  { path: 'adroles', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdrolesComponent}]
  },
  { path: 'verrol/:id', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: VerrolComponent}]
  },
  { path: 'verprograma/:id', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: VerprogramaComponent}]
  },
  { path: 'addtesis', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AddtesisComponent}]
  },
  { path: 'adlistaprocesos', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistaprocesosComponent}]
  },
  { path: 'tiptesis', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: TiptesisComponent}]
  },
  { path: 'esttesis', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: EsttesisComponent}]
  },
  { path: 'adlistaescuelas', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistaescuelasComponent}]
  },
  { path: 'adlistipjurados', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistipjuradosComponent}]
  },
  { path: 'adregistro', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdregistroComponent}]
  },
  { path: 'adlistafacultad', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistafacultadComponent}]
  },
  { path: 'adlistadictamen', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistadictamenComponent}]
  },
  { path: 'adlistaDoc', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistaDocComponent}]
  },
  { path: 'adlistagrado', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: AdlistagradoComponent}]
  },
  { path: 'informes', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: InformesComponent}]
  },
  { path: 'bustesis', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: BustesisComponent}]
  },
  { path: 'verdocumentos', component: AdministradorComponent, canActivate: [AuthGuard],
  children : [{path: '', component: VerDocumentosComponent}]
  },
  /* { path : '', redirectTo: '/registro', pathMatch : 'full'} */
];
