export interface Data {
  Correo: string,
DNI: string,
Direccion: string,
Docente: number,
Estudiante: number,
Nombre: string,
Operador: number,
Telefono: string,
id: number,
idDNITipo: number,
}

