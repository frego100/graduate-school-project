export interface TesisEstEsc{
  IdEstudianteEscuela?: number;
  IdTipoTesis?: number;
  IdEstado?: number;
  IdDictamen?: number;
  IdEstudiante?: number;
  TituloTesis?: string;
  Resumen?: string;
  PalabrasClave?: string;
}
