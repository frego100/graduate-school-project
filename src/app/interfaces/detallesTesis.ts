export interface DetallesTesis {
  ArchivoBase64_Dictamen: string;
  ArchivoBase64_ResolucionAsesor: string;
  ArchivoBase64_ResolucionJurado: string;
  ArchivoBase64_PlanTesis: string;
  ArchivoBase64_Tesis: string;
  Jurados: JuradoD[];
  Tesis: TesisD;
  }
export interface JuradoD {
  IdJurado: number;
  IdTipoJurados: number;
  Nombre: string;
  TipoJurado: string;
  id: number;
}
export interface TesisD{
  ActaDictamen: string;
  Dictamen: string;
  PlanTesis: string;
  Estado: string;
  FechaActualizacion: string;
  FechaDictamen: string;
  FechaFinAsesoria: string;
  FechaFinRevision: string;
  HoraGraduacion: string;
  FechaGraduacion: string;
  FechaRegistro: string;
  IdDictamen: number;
  IdEstado: number;
  IdTipoTesis: number;
  ResolucionAsesor: string;
  ResolucionJurado: string;
  RutaArchivo: string;
  TipoTesis: string;
  TituloTesis: string;
  Resumen?: string;
  PalabrasClave?: string;
}
