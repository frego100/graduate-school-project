export interface DocsInteres{
    idDoc ?: number;
    Documento ?: string;
    Operario ?: number;
    Docente ?: number;
    Estudiante ?: number;
    Archivo ?: string;
}