export interface Roles {
  Estudiante: boolean;
  Operador: boolean;
  Docente: boolean;
}

export interface Rolcitos{
  Estudiante?: number;
  Operador?: number;
  Docente?: number;
}