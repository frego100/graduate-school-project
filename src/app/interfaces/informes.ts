
export interface Informes {
  id:number;
  DNI: string;
  Escuela: string;
  Estado: string;
  Facultad: string;
  TipoTesis: string;
  FechaActualizacion: string;
  FechaFinAsesoria: string;
  FechaFinRevision: string;
  FechaGraduacion: string;
  FechaRegistro: string;
  Grado: string;
  Nombre: string;
  TituloTesis: string;
  jurados: JuradoInformes[];
  disabled: boolean;
}

export interface JuradoInformes {
  FechaFin: string;
  FinObservaciones: string;
  IdEstudiante: number;
  IdJurado: number;
  IdTesis: number;
  IdTipoJurado: number;
  Nombre: string;
}


export interface InformesCampos {
  DNI: string;
  Nombre: string;
  idFacultad: number;
  idEscuela: number;
  IdGrado: number;
  IdTipoTesis: number;
  FechaDesde: string;
  FechaFin: string;
  FechaInicioActualizacion: string;
  FechaFinActualizacion: string;
  FechaInicioAsesoria: string;
  FechaFinAsesoria: string;
  FechaInicioGraduacion: string;
  FechaFinGraduacion: string;
  Asesor: string;
  Jurado: string;
}
