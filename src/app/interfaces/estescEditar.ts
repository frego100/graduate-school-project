export interface EstEscEditar{
  IdEstudianteEscuela: number;
  IdEscuela: number;
  Escuela: string;
  IdEstudiante: number;
  IdGrado: number;
  Grado: string;
  Promocion: string;
}
