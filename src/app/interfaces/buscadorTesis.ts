export interface BuscaTesis {
  TituloTesis: string;
  Resumen: string;
  PalabrasClave: string;
}

export interface ResultadosBusquedaTesis{
  id: string;
  IdTesis: string;
  Nombre: string;
  DNI: string;
  Facultad: string;
  Escuela: string;
  Grado: string;
  FechaRegistro: string;
  FechaActualizacion: string;
  TituloTesis: string;
  FechaFinAsesoria: string;
  FechaFinRevision: string;
  Resumen: string;
  PalabrasClave: string;
  FechaGraduacion: string;
  Estado: string;
  TipoTesis: string;
}
