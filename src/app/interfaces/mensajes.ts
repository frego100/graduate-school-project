/**
 * Interface para capturar los mensajes de respuesta del JSON.
 *
 * @interface Mensaje
 */
export interface Mensajes {
  Mensaje: string;
}
