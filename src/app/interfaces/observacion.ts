export interface Observacion {
  IdObservacion: number;
  IdTesis: number;
  IdJurado: number;
  IdTipoObservacion: number;
  TipoObservacion: string;
  Descripcion: string;
  Fecha: string;
  Correccion: number;
  Aprobado: number;
  Consulta: string;
  Nombre: string;
}

export interface FinObs{
  id: number;
  IdTesis: number;
}
export interface Consulta{
  Consulta: string;
}
