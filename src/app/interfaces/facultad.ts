/**
 * Interface para las clases de facultades.
 *
 * @interface Facultad
 */
export interface Facultad {
  IdFacultad: number;
  Facultad: string;
}
