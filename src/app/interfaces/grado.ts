/**
 * Interface para las clases de grados de una escuela.
 *
 * @interface Grado
 */
export interface Grado {
  IdGrado: number;
  IdEscuela: number;
  Grado: string;
  Escuela: string;
}
