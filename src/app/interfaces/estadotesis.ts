export interface EstadoTesis{
  IdEstado: number;
  Estado: string;
  Modificable: string;
}
