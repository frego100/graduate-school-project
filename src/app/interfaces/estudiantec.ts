export interface Estudiantec {
  id: number;
  Nombre: string;
  idDNITipo: number;
  DNI: number;
  Correo: string;
  Operador: number;
  Estudiante: number;
  Docente: number;
  Direccion: string;
  Telefono: string;
  ListaRoles: number[];
}

export interface DetallesAlumno {
  Detalles?: Detalles;
}
export interface Detalles{
  Correo ?: string;
  DNI ?: string;
  Direccion ?: string;
  Docente?: number;
  Estudiante?: number;
  Nombre?: string;
  Operador?: number;
}