export interface EstEsc{
  IdEstudianteEscuela: number;
  IdEscuela: number;
  IdEstudiante: number;
  IdGrado: number;
  Promocion: number;

}

export interface EditarPrograma{
  IdEscuela: number,
  IdEstudiante: number,
  IdGrado: number,
  Promocion: number
}
