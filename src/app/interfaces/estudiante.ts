export interface Estudiante {
  id: number;
  Nombre: string;
  idDNITipo: number;
  DNI: number;
  Correo: string;
  Operador: number;
  Estudiante: number;
  Docente: number;
  Direccion: string;
  Telefono: string;
}
