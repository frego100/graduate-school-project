export interface Tesis {
  IdTesis: number;
  IdEstudianteEscuela: number;
  IdTipoTesis: number;
  IdEstado: number;
  TituloTesis: string;
  IdDictamen: number;
  RutaArchivo: string;
  Tamano: number;
  }
