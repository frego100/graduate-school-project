export interface TesisListar{
  IdTesis: number;
  IdJurado: number;
  TipoTesis: string;
  TituloTesis: string;
  Nombre: string;
  Dictamen: string;
  Estado: string;
  Fecha: string;
  RutaArchivo: string;
}
