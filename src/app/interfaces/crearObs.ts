export interface CrearObs {
  IdObservacion: number;
  IdTesis: number;
  IdJurado: number;
  IdTipoObservacion: number;
  Descripcion: string;
  Fecha: string;
  TipoObservacion: string;
  }
