/**
 * Interface para las clases de las escuelas de una facultad.
 *
 * @interface Escuela
 */
export interface Escuela {
  idEscuela: number;
  IdFacultad: number;
  Escuela: string;
  Facultad: string;
  Directorio: string;
}
