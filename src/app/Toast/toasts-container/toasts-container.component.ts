import {Component, TemplateRef} from '@angular/core';

import {ToastService} from './toast-service';


@Component({
  selector: 'app-toasts',
  template: `
    <ngb-toast
      *ngFor="let toast of toastService.toasts"
      [class]="toast.classname"
      [autohide]="true"
      [delay]="toast.delay || 5000"
      (hide)="toastService.remove(toast)"
    >
      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
        <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
      </ng-template>

      <ng-template #text>
        <div class="row">
          <div class="col-2">
            <i class="fas fa-shield-alt" style="font-size:2.5em; margin-top:.1em;"></i>
          </div>
          <div class="col-10">
            {{ toast.textOrTpl }} <br>

            <p style="font-size:.9em; font-weight: 200;">Intente nuevamente</p>
          </div>
        </div>
    </ng-template>
    </ngb-toast>
  `,
  host: {'[class.ngb-toasts]': 'true'}
})
export class ToastsContainer {
  constructor(public toastService: ToastService) {}

  isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }
}
